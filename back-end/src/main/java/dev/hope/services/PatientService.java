package dev.hope.services;

import dev.hope.models.Patient;
import dev.hope.models.UserAccount;
import dev.hope.repositories.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service // registers this class to IOC container as a Spring bean
public class PatientService {

    @Autowired
    PatientRepository patientRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;

    @Autowired
    EmailNotification emailNotification;

    public List<Patient> getAll() {
        return patientRepository.findAll();
    }

    public Patient getById(int id) {
        return patientRepository.getOne(id);
    }

    public ResponseEntity<Patient> create(Patient patient, int userID) {
        patient = patientRepository.save(patient);
        UserAccount user = userService.getUserById(userID);
        user.setPatient(patient);
        userService.updateUserAccount(user);
        String token = authService.generateToken(user);
        HttpHeaders headers = new HttpHeaders();
        List<String> exposeHeaders = new ArrayList<>();
        exposeHeaders.add("Authorization");
        headers.add("Authorization",token);
        if(user.getProvider()!=null){
            headers.add("Provider",String.valueOf(user.getProvider().getId()));
            exposeHeaders.add("Provider");
        }
        if(user.getPatient()!=null){
            headers.add("Patient",String.valueOf(user.getPatient().getId()));
            exposeHeaders.add("Patient");
        }
        headers.setAccessControlExposeHeaders(exposeHeaders);
        return ResponseEntity.status(HttpStatus.OK).headers(headers).body(patient);

    }

    public List<Patient> getByName(String name) {
       // return patientRepository.getPatientByName(name);
        return null;
    }
}
