package dev.hope.controllers;

import dev.hope.models.Patient;
import dev.hope.models.UserAccount;
import dev.hope.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.xml.ws.Response;
import java.util.List;
import java.util.Map;

@RestController
//@CrossOrigin(origins = "http://karlkanitschrevature.z13.web.core.windows.net/")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    private static final Logger logger = LogManager.getLogger(UserService.class);

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody UserAccount user){
        String email = user.getEmail();
        String password = user.getPassword();
        return userService.login(email, password);
    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody UserAccount user){
        String email = user.getEmail();
        String password = user.getPassword();
        return userService.register(email, password);
    }
    @GetMapping(value = "/userAccounts")
    public List<UserAccount> getAllUserAccounts(){
        return userService.getAllUsers();
    }

    @GetMapping(value ="/userAccounts/{id}", produces = "application/json")
    public ResponseEntity<UserAccount> getUserAccountById(@PathVariable("id") int id){
        logger.info("Getting User Account with id", id);
        return ResponseEntity.ok().body(userService.getUserById(id));
    }
    @PutMapping("/userAccounts/{id}")
    public ResponseEntity<?> updateUserAccount(@PathVariable("id") Integer accountId, @RequestBody UserAccount account){
        account.setId(accountId);
        UserAccount updatedAccount = userService.updateUserAccount(account);
        return new ResponseEntity<Object>(updatedAccount, HttpStatus.OK);
    }

    @PatchMapping("/userAccounts/{id}")
    public ResponseEntity<UserAccount> addPatientToUser(@PathVariable("id") int id, @RequestBody Patient patient){
        logger.info("Getting User Account with id", id);
        UserAccount updatedUser = userService.SetPatientId(id, patient);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);

    }
}
