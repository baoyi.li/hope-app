package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;



@Entity
@Table(name="location")
@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
@Scope("prototype")
public class Location {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="location_id")
    private int id;
    private boolean onsite;
    private String siteName;
    private String address;
    private String city;
    private String state;
    private String postalCode;
    private String contactPerson;
    private String contactPersonPhoneNumber;

    @ManyToOne(fetch = FetchType.EAGER)
    private Provider provider;


    public Location() {
    }

    public Location(int id) {
        this.id = id;
    }

    public Location(boolean onsite, String siteName, String address, String city, String state, String postalCode, String contactPerson, String contactPersonPhoneNumber) {
        this.onsite = onsite;
        this.siteName = siteName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.contactPerson = contactPerson;
        this.contactPersonPhoneNumber = contactPersonPhoneNumber;
    }

    public Location(boolean onsite, String siteName, String address, String city, String state, String postalCode, String contactPerson, String contactPersonPhoneNumber, Provider provider) {
        this.onsite = onsite;
        this.siteName = siteName;
        this.address = address;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.contactPerson = contactPerson;
        this.contactPersonPhoneNumber = contactPersonPhoneNumber;
        this.provider = provider;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isOnsite() {
        return onsite;
    }

    public void setOnsite(boolean onsite) {
        this.onsite = onsite;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getContactPersonPhoneNumber() {
        return contactPersonPhoneNumber;
    }

    public void setContactPersonPhoneNumber(String contactPersonPhoneNumber) {
        this.contactPersonPhoneNumber = contactPersonPhoneNumber;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return id == location.id && onsite == location.onsite && Objects.equals(siteName, location.siteName) && Objects.equals(address, location.address) && Objects.equals(city, location.city) && Objects.equals(state, location.state) && Objects.equals(postalCode, location.postalCode) && Objects.equals(contactPerson, location.contactPerson) && Objects.equals(contactPersonPhoneNumber, location.contactPersonPhoneNumber) && Objects.equals(provider, location.provider);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, onsite, siteName, address, city, state, postalCode, contactPerson, contactPersonPhoneNumber, provider);
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", onsite=" + onsite +
                ", siteName='" + siteName + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                ", contactPersonPhoneNumber='" + contactPersonPhoneNumber + '\'' +
                ", provider=" + provider +
                '}';
    }

    public String emailString() {
        return "Street Address: " + address + '\n' +
                "City: " + city + '\n' +
                "State: " + state + '\n' +
                "Postal Code: " + postalCode + '\n' +
                "Contact Person: " + contactPerson + '\n' +
                "Contact Number: " + contactPersonPhoneNumber;
    }
}
