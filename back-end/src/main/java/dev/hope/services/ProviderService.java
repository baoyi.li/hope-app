package dev.hope.services;

import dev.hope.models.Provider;
import dev.hope.repositories.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProviderService {

    @Autowired
    ProviderRepository providerRepo;

    public List<Provider> getAll() {
        return providerRepo.findAll();
    }

    public Provider getProviderById(int id) {
        return providerRepo.getOne(id);
    }

    public Provider addProvider(Provider provider) {
        return providerRepo.save(provider);
    }

    public void deleteProvider(int id) {
        providerRepo.deleteById(id);
    }

    /**
     * @param id the id of the location
     * @param
     */
    public Provider updateProvider(int id, Provider newProvider) {
        return providerRepo.findById(id)
                .map(provider -> {
                    provider.setProviderName(newProvider.getProviderName());
                    provider.setMainAddress(newProvider.getMainAddress());
                    provider.setCity(newProvider.getCity());
                    provider.setState(newProvider.getState());
                    provider.setPostalCode(newProvider.getPostalCode());
                    provider.setMainPhoneNumber(newProvider.getMainPhoneNumber());
                    provider.setMainEmail(newProvider.getMainEmail());
                    provider.setPassword(newProvider.getPassword());
                    return providerRepo.save(provider);
                })
                .orElseGet(() -> {
                    newProvider.setId(id);
                    return providerRepo.save(newProvider);
                });
    }

    public List<Provider> getProvidersByName(String name) {
        return providerRepo.findProviderByProviderNameContaining(name);
    }

}
