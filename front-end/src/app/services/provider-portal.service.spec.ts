import { TestBed } from '@angular/core/testing';

import { ProviderPortalService } from './provider-portal.service';

describe('ProviderPortalService', () => {
  let service: ProviderPortalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProviderPortalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
