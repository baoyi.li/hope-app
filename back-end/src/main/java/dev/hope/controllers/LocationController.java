package dev.hope.controllers;

import dev.hope.models.Location;
import dev.hope.models.Provider;
import dev.hope.services.AuthService;
import dev.hope.services.LocationService;
import dev.hope.services.ProviderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
/**
 * registering this class as a spring bean
 * returns a response body from each method
 * @RestController is equivalent to both @Controller and @ResponseBody for each handler method
 */

@RequestMapping("/locations")   // prepends this to each request URI
@CrossOrigin
public class LocationController {
    private static Logger logger = LogManager.getLogger(LocationController.class);

    @Autowired
    private LocationService locationService;
    @Autowired
    private AuthService authService;

    @GetMapping
    public ResponseEntity<List<Location>> getAllLocations(@RequestHeader("Authorization")String token) {
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        System.out.println(claimsMap);
        int providerID;
        if(claimsMap.get("providerId")==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        try {
            providerID = Integer.parseInt((String) claimsMap.get("providerId"));
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return ResponseEntity.ok().body(locationService.getAll(providerID));
    }

    @GetMapping(value = "/{id}")
    public Location getLocationById(@PathVariable("id") int id) {
        return locationService.getLocationById(id);
    }

    @PostMapping
    public ResponseEntity<Location> addLocation(@RequestHeader("Authorization")String token, @RequestBody Location location) {
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        int providerID;
        if(claimsMap.get("providerId")==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        try {
            providerID = Integer.parseInt((String) claimsMap.get("providerId"));
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if(providerID!=location.getProvider().getId())
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        return ResponseEntity.ok().body(locationService.addLocation(location));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Location> updateLocation(@RequestHeader("Authorization")String token, @RequestBody Location location, @PathVariable("id") int id) {
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        int providerID;
        if(claimsMap.get("providerId")==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        try {
            providerID = Integer.parseInt((String) claimsMap.get("providerId"));
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        if(providerID!=location.getProvider().getId())
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        return ResponseEntity.ok().body(locationService.updateLocation(id,location));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Location> deleteLocation(@RequestHeader("Authorization")String token, @PathVariable("id") int id) {
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        int providerID;
        if(claimsMap.get("providerId")==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        try {
            providerID = Integer.parseInt((String) claimsMap.get("providerId"));
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        Location location = locationService.getLocationById(id);
        if(providerID!=location.getProvider().getId())
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        return ResponseEntity.ok().body(locationService.updateLocation(id,location));
    }

}
