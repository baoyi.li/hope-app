package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;


@Entity
@JsonIgnoreProperties("hibernateLazyInitializer")

@Component
@Scope("prototype")
@Table(name="user_account")
public class UserAccount {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;
    private String email;
    private String password;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="provider_id")
    private Provider provider;
    private boolean admin;
    @OneToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    public UserAccount() {}

    public UserAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public UserAccount(int id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return id == that.id && admin == that.admin && Objects.equals(email, that.email) && Objects.equals(password, that.password) && Objects.equals(provider, that.provider);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, password, provider, admin);
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", provider=" + provider +
                ", admin=" + admin +
                ", patient=" + patient +
                '}';
    }
}
