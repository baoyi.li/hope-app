import { Component, OnInit } from '@angular/core';
import { Appointment } from 'src/app/models/appointment.model';
import { PatientPortalService } from 'src/app/services/patient-portal.service';
import { ActivatedRoute } from '@angular/router';
// import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-appointment-confirmation',
  templateUrl: './appointment-confirmation.component.html',
  styleUrls: ['./appointment-confirmation.component.css']
})
export class AppointmentConfirmationComponent implements OnInit {

  token: string = "";
  apptStatus: string = "";
  apptDateTime: string = "";
  apptProvider: string = "";
  apptLocation: string = "";
  apptType: string = ""
  appointmentDetails = this.ppService.getAppointmentDetails();

  appointment: Appointment = new Appointment;

  getAppointmentDetails() {
    this.appointmentDetails.subscribe((response)=>{
      this.appointment = response.body;
      console.log(this.appointment);
      if(this.appointment!=null){
        this.apptStatus="Confirmed";
        this.apptDateTime=String(this.appointment.date)+" at "+String(this.appointment.time);
        this.apptProvider=this.appointment.provider.providerName;
        let apptAddress: string = this.appointment.location.address+"<br>"+this.appointment.location.city+", "+this.appointment.location.state+" "+this.appointment.location.postalCode;
        this.apptLocation=apptAddress;
        this.apptType=this.appointment.vaccineMfr;
      } else {
        this.apptStatus="Pending";
      }
    });
  }

  constructor(private ppService: PatientPortalService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    // this.route.queryParams
    //   .filter(params => params.id)
    //   .subscribe(params => {
    //     console.log(params); // { order: "popular" }

    //     this.token = params.id;
    //     console.log(this.token); // popular
    //   }
    // );
    this.getAppointmentDetails();
  }

}


