package dev.hope.repositories;

import dev.hope.models.Location;

import java.util.ArrayList;
import java.util.List;


public abstract class LocationData implements LocationRepository {

    public final List<Location> locations = new ArrayList<>();

    /**
    @Override
    public List<Location> getAllLocations() {
        return new ArrayList<>(locations);
    }

    @Override
    public Location getLocationById(int id) {
        return locations.stream().filter(location -> location.getId()==id).findAny().orElse(null);
    }

    @Override
    public Location getLocationBySiteName(String name) {
        return locations.stream().filter(location -> location.getSiteName().equals(name)).findFirst().get();
    }

    @Override
    public Location addLocation(Location location) {
        locations.add(location);
        return location;
    }

    @Override
    public void updateLocation(int id, Location location) {
        for (int i = 0; i < locations.size(); i++) {
            Location l = locations.get(i);
            if(l.getId() == id) {
                locations.set(i, location);
                return;
            }


        }
    }


    @Override
    public void deleteLocation(int id) {
        locations.removeIf(location -> location.getId()==id);
    }

    @Override
    public void deleteLocationBySiteName(String name) {
        locations.removeIf(location -> location.getSiteName().equals(name));
    }


     @Override
     public Location updateLocation(int id, Location location) {
     for (int i = 0; i < locations.size(); i++) {
     Location l = locations.get(i);
     if(l.getId() == id) {
     locations.set(i, location);
     return location;
     }
     }
     return null;
     }


    */



}
