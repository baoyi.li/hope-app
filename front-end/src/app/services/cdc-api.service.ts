import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';


import { CovidData } from '../models/covid-data';

import { VaccinationData } from  '../models/vaccination-data';
import { environment } from 'src/environments/environment';

// import { MessageService } from '../services/message.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}


@Injectable({
  providedIn: 'root'
})

export class CdcAPIService {


  //version 0:
  //private cdcCasesUrl = 'https://data.cdc.gov/resource/9mfq-cb36.json';  // URL to web api
  
  //version 1: 
  private cdcCasesUrl = environment.baseUrl + "/cdc-api/covid-data"
  
  
  //version 0:
  //private cdcVaccinationsUrl = 'https://covid.cdc.gov/covid-data-tracker/COVIDData/getAjaxData?id=vaccination_data';  // URL to web api

  //version 1:
  private cdcVaccinationsUrl = environment.baseUrl + "/cdc-api/vaccination-data"


  // Angular injector creates an instance of HttpClient for us, and provides it to CdcAPIService
  constructor(private http: HttpClient) { } 


    
    
    /** GET CovidData from the CDC server via the backend */
    getAllCovidData(): Observable<CovidData[]> {
      return this.http.get<CovidData[]>(this.cdcCasesUrl);
    }

        /** GET VaccinationData from the CDC server via the backend */
    getAllVaccinationData(): Observable<VaccinationData[]> {
      return this.http.get<VaccinationData[]>(this.cdcVaccinationsUrl);
    }

    



    /** GET CovidData by state. Will 404 if id not found */
    getCovidData(state: string): Observable<CovidData> {
      const url = `${this.cdcCasesUrl}/${state}`;
      return this.http.get<CovidData>(url).pipe(
        tap(_ => this.log(`fetched CDCCovidData for state=${state}`)),
        catchError(this.handleError<CovidData>(`getCovidData state=${state}`))
      );
    }

    /** GET VaccinationData by state. Will 404 if id not found */
   // getVaccinationData(state: string): Observable<VaccinationData> {
   // const url = `${this.cdcVaccinationsUrl}/${state}`;
   // return this.http.get<VaccinationData>(url).pipe(
    //  tap(_ => this.log(`fetched VaccinationData for state=${state}`)),
     // catchError(this.handleError<VaccinationData>(`getVaccinationData state=${state}`))
     // );
   // }




    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   * 
   * Copyright Google LLC. All Rights Reserved.
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      //console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }


  /** Log a CdcAPIService message with the MessageService */
  private log(message: string) {
    console.log(`CdcAPIService: ${message}`);
  }

}
