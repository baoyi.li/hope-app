# HopeApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Project Description

This is the front end portion of HopeApp project. We come up of with this idea of signing up for Covid-19 vaccination process because people were not sure when the vaccination will be available for them as there were not enough vaccines available for all at once. So, we decided to build this service where people can sign up and get into waitlist for vaccination. We work with various providers who will post their available vaccination appointments as soon as they are available to them into our app. Our main job is to match the available appointments to the patient in waitlist. We match the patient with appointment based on their willingness on how far they are willing to drive, and obviously in first come first serve basis. If we find a suitable match, then we will notify the patient through email. The notified patient user will need to confirm the available appointment by clicking the link provided in the email within speicified time period. If the patient confirms the appoinment, we will notify provider about the appointment confirmation, if not, we will notify another patient from the waitlist if the notified patient failed to confirm the appointment. 

## Technologies Used

* Spring Framework
* Spring Boot
* Spring Web
* Spring AOP
* Spring Data
* Azure VM
* Azure DevOps
* Jenkins
* Azure SQL database
* REST
* Java
* HTML
* CSS
* AWS Simple Email service


## Features

* One can sign up as a vaccination provider or a patient seeking vaccination and use the created credentials to login to this app
* User will be able to different navigation bar based on whether they signed in as a patient or a provider
* Patient user will be able to see current CDC covid data in their portal. They are also able to sign up to get vaccinated and check their vaccination appointment status.
* Provider user will be able to add and remove appointments. They are also able to add and edit locations if they have various locations for vaccination.

To-do list:
* Make the application more secure by using Spring Security.
* Display pie charts based on CDC covid data.

## Getting Started
 
Downlaod the project from GitLab
> git clone https://gitlab.com/baoyi.li/hope-app.git

Build the dependencies in the Back-end project
> gradle build

Run the Driver class

Build the dependencies in the front-end project
> ng build

Run the project
> ng serve --open

## Usage

- Sign up by clicking 'Sign Up' in the navigation bar.
- Sign in to the service by using the created credentials.
- If you are a patient user, click 'GET VACCINATED' to get into waitlist. After you sign up for wait list, you will be able to check your appointment status by clicking the same button with different label 'GET APPOINTMENT STATUS'.
- If you are provider, you can go to the provider portal to add an appointment and change the provider's location.


## Contributors

> Baoyi Li, Karl Kanitsch, Miguel Angel Rodriguez, Sujit Neupane

## License

This project uses the following license: <https://gitlab.com/baoyi.li/hope-app/-/blob/master/LICENSE>.


## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
