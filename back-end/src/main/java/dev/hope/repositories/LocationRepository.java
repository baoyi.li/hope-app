package dev.hope.repositories;

import dev.hope.models.Location;
import dev.hope.models.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface LocationRepository extends JpaRepository<Location, Integer> {

    List<Location> findByProvider(Provider provider);

    List<Location> findLocationBySiteNameContaining(String name);

    @Modifying
    @Query("update Location l set l.contactPerson = ?1, l.contactPersonPhoneNumber = ?2 where l.id = ?3")
    void setLocationContactInformation(String contactPerson, String contactPersonPhoneNumber, int id);

    @Modifying
    @Query("update Location l set l.onsite = ?1, l.siteName = ?2, l.address = ?3, l.city = ?4, l.state = ?5, l.postalCode = ?6 where l.id = ?7")
    void setLocationSiteInformation(boolean onsite, String siteName, String address, String city, String state, String postalCode, int id);


}
