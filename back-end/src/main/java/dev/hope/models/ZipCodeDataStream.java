package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ZipCodeDataStream {

    @JsonProperty("fields")
    public ZipCode zipCode;
}
