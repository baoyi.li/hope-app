import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { AppointmentTableComponent } from './components/appointment-table/appointment-table.component';
import { NewsPortalComponent } from './components/news-portal/news-portal.component';
import { PartnersComponent } from './components/partners/partners.component';
import { PatientPortalComponent } from './components/patient-portal/patient-portal.component';
import { ProviderPortalComponent } from './components/provider-portal/provider-portal.component';
import { RegisterComponent } from './components/register/register.component';
import { UserComponent } from './components/user/user.component';
import { HomeComponent } from './components/home/home.component';
import { LocationComponent } from './components/location/location.component';
import { AppointmentConfirmationComponent } from './components/appointment-confirmation/appointment-confirmation.component';

const routes: Routes = [
  
  { path: "", component:HomeComponent},
  { path: "home", component:HomeComponent},
  { path:"register", component:RegisterComponent},
  { path:"partners", component:PartnersComponent},
  { path:"about", component:AboutUsComponent},
  { path:"news", component:NewsPortalComponent},
  { path:"patient-portal", component:PatientPortalComponent},
  { path: 'provider-portal', component: ProviderPortalComponent, // this is the component with the <router-outlet> in the template
    children: [
      { path: '', component:AppointmentTableComponent},
      { path: 'appointment-table', component: AppointmentTableComponent},
      { path: 'location', component: LocationComponent},
      // { path: 'user', component: UserComponent},
    ],
  },
  { path:"**", component:HomeComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
