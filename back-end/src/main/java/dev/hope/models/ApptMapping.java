package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDate;

@Entity

@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
@Scope("prototype")
public class ApptMapping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="appointment_id")
    private int apptid;

    @ManyToOne
    @JoinColumn(name="provider_id")
    private Provider provider;

    @Column(name="appt_date")
    private LocalDate date;

    @Column(name="appt_time")
    private String time;

    @ManyToOne
    @JoinColumn(name="location_id")
    private Location location;

    @OneToOne
    @JoinColumn(name="patient_id")
    private Patient patient;
    private String vaccineMfr;

    public ApptMapping() {
    }

    public ApptMapping(int apptid, Provider provider, LocalDate date, String time, Location location, Patient patient, String vaccineMfr) {
        this.apptid = apptid;
        this.provider = provider;
        this.date = date;
        this.time = time;
        this.location = location;
        this.patient = patient;
        this.vaccineMfr = vaccineMfr;
    }

    public ApptMapping(Provider provider, LocalDate date, String time, Location location, Patient patient, String vaccineMfr) {
        this.provider = provider;
        this.date = date;
        this.time = time;
        this.location = location;
        this.patient = patient;
        this.vaccineMfr = vaccineMfr;
    }

    public int getApptid() {
        return apptid;
    }

    public void setApptid(int apptid) {
        this.apptid = apptid;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getVaccineMfr() {
        return vaccineMfr;
    }

    public void setVaccineMfr(String vaccineMfr) {
        this.vaccineMfr = vaccineMfr;
    }

    @Override
    public String toString() {
        return "ApptMapping{" +
                "apptid=" + apptid +
                ", provider=" + provider +
                ", date=" + date +
                ", time='" + time + '\'' +
                ", location=" + location +
                ", patient=" + patient +
                ", vaccineMfr='" + vaccineMfr + '\'' +
                '}';
    }
}
