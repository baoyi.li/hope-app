import { Component, OnInit } from '@angular/core';
import { Appointment } from 'src/app/models/appointment.model';
import { PatientPortalService } from 'src/app/services/patient-portal.service';

@Component({
  selector: 'app-appointment-status',
  templateUrl: './appointment-status.component.html',
  styleUrls: ['./appointment-status.component.css']
})
export class AppointmentStatusComponent implements OnInit {

  apptStatus: string = "";
  apptDateTime: string = "";
  apptProvider: string = "";
  apptLocation: string = "";
  apptType: string = ""
  appointmentDetails = this.ppService.getAppointmentDetails();

  appointment: Appointment = new Appointment;

  getAppointmentDetails() {
    this.appointmentDetails.subscribe((response)=>{
      this.appointment = response.body;
      console.log(this.appointment);
      if(this.appointment!=null){
        this.apptStatus="Confirmed";
        this.apptDateTime=String(this.appointment.date)+" at "+String(this.appointment.time);
        this.apptProvider=this.appointment.provider.providerName;
        let apptAddress: string = this.appointment.location.address+"<br>"+this.appointment.location.city+", "+this.appointment.location.state+" "+this.appointment.location.postalCode;
        this.apptLocation=apptAddress;
        this.apptType=this.appointment.vaccineMfr;
      } else {
        this.apptStatus="Pending";
      }
    });
  }

  constructor(private ppService: PatientPortalService) { }

  ngOnInit(): void {
    this.getAppointmentDetails();
  }
}
