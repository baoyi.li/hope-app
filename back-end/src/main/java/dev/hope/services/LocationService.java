package dev.hope.services;

import dev.hope.models.Location;
import dev.hope.models.Provider;
import dev.hope.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepo;
    @Autowired
    private ProviderService providerService;

    public List<Location> getAll(int providerID) {
        Provider provider = providerService.getProviderById(providerID);
        return locationRepo.findByProvider(provider);
    }


    public Location getLocationById(int id) {
        return locationRepo.getOne(id);
    }

    public Location addLocation(Location location) {
        return locationRepo.save(location);
    }

    public void deleteLocation(int id) {
        locationRepo.deleteById(id);
    }

    /**
     * @param id the id of the location
     */
    public Location updateLocation(int id, Location newLocation) {
        return locationRepo.findById(id)
                .map(location -> {
                    location.setOnsite(newLocation.isOnsite());
                    location.setSiteName(newLocation.getSiteName());
                    location.setAddress(newLocation.getAddress());
                    location.setCity(newLocation.getCity());
                    location.setState(newLocation.getState());
                    location.setPostalCode(newLocation.getPostalCode());
                    location.setContactPerson(newLocation.getContactPerson());
                    location.setContactPersonPhoneNumber(newLocation.getContactPersonPhoneNumber());
                    location.setProvider(newLocation.getProvider());
                    return locationRepo.save(location);
                })
                .orElseGet(() -> {
                    newLocation.setId(id);
                    return locationRepo.save(newLocation);
                });
    }

    public void updateLocationContactInformation(String contactPerson, String contactPersonPhoneNumber, int id) {
        locationRepo.setLocationContactInformation(contactPerson, contactPersonPhoneNumber, id);
    }

    public void updateLocationSiteInformation(boolean onsite, String siteName, String address, String city, String state, String postalCode, int id) {
        locationRepo.setLocationSiteInformation(onsite,siteName,address,city,state,postalCode,id);
    }


    public List<Location> getLocationsBySiteName(String name) {
        return locationRepo.findLocationBySiteNameContaining(name);
    }


}
