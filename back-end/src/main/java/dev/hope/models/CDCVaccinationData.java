package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name="vaccination_data")
@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
@Scope("prototype")
public class CDCVaccinationData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonProperty("Date")
    private String date;
    @JsonProperty("ShortName")
    private String shortName;
    @JsonProperty("LongName")
    private String longName;
    @JsonProperty("Doses_Distributed")
    private String dosesDistributed;
    @JsonProperty("Doses_Administered")
    private String dosesAdministered;


    public CDCVaccinationData() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getDosesDistributed() {
        return dosesDistributed;
    }

    public void setDosesDistributed(String dosesDistributed) {
        this.dosesDistributed = dosesDistributed;
    }

    public String getDosesAdministered() {
        return dosesAdministered;
    }

    public void setDosesAdministered(String dosesAdministered) {
        this.dosesAdministered = dosesAdministered;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CDCVaccinationData that = (CDCVaccinationData) o;
        return id == that.id && Objects.equals(date, that.date) && Objects.equals(shortName, that.shortName) && Objects.equals(longName, that.longName) && Objects.equals(dosesDistributed, that.dosesDistributed) && Objects.equals(dosesAdministered, that.dosesAdministered);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, shortName, longName, dosesDistributed, dosesAdministered);
    }

    @Override
    public String toString() {
        return "CDCVaccinationData{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", shortName='" + shortName + '\'' +
                ", longName='" + longName + '\'' +
                ", dosesDistributed='" + dosesDistributed + '\'' +
                ", dosesAdministered='" + dosesAdministered + '\'' +
                '}';
    }
}
