import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Appointment } from 'src/app/models/appointment.model';
import { Location } from 'src/app/models/location';
import { Provider } from 'src/app/models/provider';
import { AppointmentService } from 'src/app/services/appointment.service';
import { AuthService } from 'src/app/services/auth.service';
import { LocationService } from 'src/app/services/location.service';
import { AppointmentTableComponent } from '../appointment-table/appointment-table.component';


@Component({
  selector: 'app-appointment-form',
  templateUrl: './appointment-form.component.html',
  styleUrls: ['./appointment-form.component.css']
})
export class AppointmentFormComponent implements OnInit {
  
  appt: Appointment = new Appointment();
  message: string = "";
  errorMessage: string =""
  locations: Location[] = [];
  providerId: number = 0;
  
 
  
  constructor(private apptService: AppointmentService, private matDialog : MatDialog,private router : Router, private locationService: LocationService, private authService: AuthService) { 
    locationService.getAllLocations().subscribe(
        (locationsReturned)=>{ this.locations=locationsReturned},
        ()=>{ this.errorMessage = "There is an issue getting location"}
      )
  }

  ngOnInit(): void {
    this.authService.providerIdValue.subscribe((id)=>this.providerId=id);
  }

  createAppt(){
    this.appt.provider.id=this.providerId;
    this.apptService.addAppointment(this.appt).subscribe(
      ()=>{
        this.message = "Successfully added new appointment";
        this.matDialog.closeAll();
        this.reloadCurrentRoute();
        this.ngOnInit();
      },
      (response)=>{console.log(response.body);this.message = "There was an issue adding appointment"}
    )
  }


    reloadCurrentRoute() {
      let currentUrl = this.router.url;
      this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
          this.router.navigate([currentUrl]);
      });
  }
  
  

  
}
