package dev.hope.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name="provider")
@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
@Scope("prototype")
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="provider_id")
    private int id;
    private String providerName;
    private String mainAddress;
    private String city;
    private String state;
    private String postalCode;
    private String mainPhoneNumber;
    private String mainEmail;
    private String password;

    public Provider() {
    }

    public Provider(int id) {
        this.id = id;
    }

    public Provider(String providerName, String mainAddress, String city, String state, String postalCode, String mainPhoneNumber, String mainEmail, String password) {
        this.providerName = providerName;
        this.mainAddress = mainAddress;
        this.city = city;
        this.state = state;
        this.postalCode = postalCode;
        this.mainPhoneNumber = mainPhoneNumber;
        this.mainEmail = mainEmail;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getMainAddress() {
        return mainAddress;
    }

    public void setMainAddress(String mainAddress) {
        this.mainAddress = mainAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getMainPhoneNumber() {
        return mainPhoneNumber;
    }

    public void setMainPhoneNumber(String mainPhoneNumber) {
        this.mainPhoneNumber = mainPhoneNumber;
    }

    public String getMainEmail() {
        return mainEmail;
    }

    public void setMainEmail(String mainEmail) {
        this.mainEmail = mainEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Provider provider = (Provider) o;
        return id == provider.id && Objects.equals(providerName, provider.providerName) && Objects.equals(mainAddress, provider.mainAddress) && Objects.equals(city, provider.city) && Objects.equals(state, provider.state) && Objects.equals(postalCode, provider.postalCode) && Objects.equals(mainPhoneNumber, provider.mainPhoneNumber) && Objects.equals(mainEmail, provider.mainEmail) && Objects.equals(password, provider.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, providerName, mainAddress, city, state, postalCode, mainPhoneNumber, mainEmail, password);
    }

    @Override
    public String toString() {
        return "Provider{" +
                "id=" + id +
                ", providerName='" + providerName + '\'' +
                ", mainAddress='" + mainAddress + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", mainPhoneNumber='" + mainPhoneNumber + '\'' +
                ", mainEmail='" + mainEmail + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
