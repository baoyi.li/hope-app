package dev.hope.services;

import dev.hope.models.ApptMapping;
import dev.hope.models.Patient;
import dev.hope.models.Provider;
import dev.hope.models.UserAccount;
import dev.hope.repositories.AppointmentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppointmentServiceImpl implements  AppointmentService{

    @Autowired
    private AppointmentRepository apptRepository;
    @Autowired
    MatchingService matchingService;
    @Autowired
    EmailNotification emailNotification;
    @Autowired
    PatientService patientService;
    @Autowired
    ProviderService providerService;
    @Autowired
    UserService userService;

    private Logger logger = LogManager.getLogger(AppointmentServiceImpl.class);

    @Override
    public List<ApptMapping> listAllAppt(int providerId) {
        Provider provider = providerService.getProviderById(providerId);
        if(provider==null)
            return null;
        return apptRepository.findByProvider(provider);
    }


    @Override
    public ApptMapping getApptById(Integer id) {
        return apptRepository.getOne(id);
    }

    @Override
    public ApptMapping addAppt(ApptMapping appt) {
        appt = apptRepository.save(appt);
        Patient candidate = matchingService.findMatch(appt);
        if(candidate!=null) {
            logger.info(candidate.toString());
            // Call notification service on candidate.
            String email = candidate.getPatientEmail();
            String subject = "Vaccination Appointment Available Near You!";
            String text = "A provider in your area is ready to administer your vaccine.\n\n" +
                    "Provider: "+appt.getProvider().getProviderName()+"\n"+
                    appt.getLocation().emailString() + "\n"+
                    "Date: " + appt.getDate()+"\n"+
                    "Time: " + appt.getTime()+"\n\n"+
                    "Click the link below to confirm that you can make this appointment.\n" +
                    "http://52.150.14.143/appointments/"+appt.getApptid()+"/"+candidate.getId()+"/confirm";
            emailNotification.sendSimpleEmail("sujitin21@gmail.com", subject, text);
        } else {
            logger.warn("No suitable candidate found");
        }
        return appt;
    }

    @Override
    public boolean deleteAppt(Integer id) {
        boolean exist = apptRepository.existsById(id);
        if (exist){
            apptRepository.deleteById(id);
            return true;
        }
        return false;
    }

    @Override
    public ResponseEntity<ApptMapping> confirmAppointment(int apptID, int patID) {
        ApptMapping appointment = apptRepository.getOne(apptID);
        Patient patient = patientService.getById(patID);
        Provider provider = appointment.getProvider();
        appointment.setPatient(patient);
        apptRepository.save(appointment);
        String email = provider.getMainEmail();
        String subject = "Patient Confirmed Appointment";
        String text = "A patient has confirmed that they will be at your appointment.";
        emailNotification.sendSimpleEmail("sujitin21@gmail.com",subject,text);
        return ResponseEntity.ok().body(appointment);
    }

    @Override
    public ApptMapping getPatientAppointment(int userID) {
        UserAccount user = userService.getUserById(userID);
        if(user.getPatient()==null)
            return null;
        ApptMapping appointment = apptRepository.findByPatient(user.getPatient());
        return appointment;
    }


}
