package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import org.springframework.context.annotation.Scope;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name="patient")
@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
@Scope("prototype")
public class Patient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="patient_id")
    private int id;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String patientEmail;
    private String zipcode;
    private LocalDate dateOfBirth;
    private boolean preExistingCondition;
    private int maxDistance;

    public Patient() {
        super();
    }

    public Patient(String firstName, String lastName, String phoneNumber, String patientEmail, String zipcode, LocalDate dateOfBirth, boolean preExistingCondition, int maxDistance) {
        super();

        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.patientEmail = patientEmail;
        this.zipcode = zipcode;
        this.dateOfBirth = dateOfBirth;
        this.preExistingCondition = preExistingCondition;
        this.maxDistance = maxDistance;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPatientEmail() {
        return patientEmail;
    }

    public void setPatientEmail(String patientEmail) {
        this.patientEmail = patientEmail;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public boolean isPreExistingCondition() {
        return preExistingCondition;
    }

    public void setPreExistingCondition(boolean preExistingCondition) {
        this.preExistingCondition = preExistingCondition;
    }

    public int getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(int maxDistance) {
        this.maxDistance = maxDistance;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", patientEmail='" + patientEmail + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", preExistingCondition=" + preExistingCondition +
                ", maxDistance=" + maxDistance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return id == patient.id && preExistingCondition == patient.preExistingCondition && Objects.equals(firstName, patient.firstName) && Objects.equals(lastName, patient.lastName) && Objects.equals(phoneNumber, patient.phoneNumber) && Objects.equals(patientEmail, patient.patientEmail) && Objects.equals(zipcode, patient.zipcode) && Objects.equals(dateOfBirth, patient.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, phoneNumber, patientEmail, zipcode, dateOfBirth, preExistingCondition);
    }

}
