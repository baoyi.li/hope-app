import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // these three BehaviorSubjects need to set to false by default.
  // Set to true for testing purpose
  private loginStatus = new BehaviorSubject(false);
  private isProvider = new BehaviorSubject(false);
  private isPatient = new BehaviorSubject(false);
  private userID = new BehaviorSubject(0);
  private patientID = new BehaviorSubject(0);
  private providerID = new BehaviorSubject(0);
  loginStatusObservable = this.loginStatus.asObservable();
  isProviderObservable = this.isProvider.asObservable();
  isPatientObservable = this.isPatient.asObservable();
  userIdValue = this.userID.asObservable();
  patientIdValue = this.patientID.asObservable();
  providerIdValue = this.providerID.asObservable();

  loginUrl: string = environment.baseUrl+"login";
  registerUrl: string = environment.baseUrl+"register";

  constructor(private httpClient: HttpClient, private router: Router) { }

  attemptLogin(user: User): Observable<any> {
    return this.httpClient.post<string>(this.loginUrl,user, {observe: "response"});
  }

  attemptRegister(user: User): Observable<any> {
    return this.httpClient.post<string>(this.registerUrl,user, {observe: "response"});
  }

  logout(){
    sessionStorage.clear();
    this.updateLoginStatus(false);
    this.updateIsPatient(false);
    this.updateIsProvider(false);
    this.updatePatientID(0);
    this.updateProviderID(0);
    this.updateUserID(0);
    this.router.navigate(['home']);
  }

  updateLoginStatus(isLoggedIn: boolean) {
    this.loginStatus.next(isLoggedIn);
  }

  updateIsProvider(isProvider: boolean) {
    this.isProvider.next(isProvider);
  }

  updateIsPatient(isPatient: boolean) {
    this.isPatient.next(isPatient);
  }

  updateUserID(idValue: number){
    this.userID.next(idValue);
  }

  updatePatientID(idValue: number){
    this.patientID.next(idValue);
  }

  updateProviderID(idValue: number){
    this.providerID.next(idValue);
  }


}
