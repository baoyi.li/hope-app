--drop table user_account;
--drop table appt_mapping;
--drop table location;
--drop table provider;
--drop table patient;
--
--CREATE TABLE exposhr.dbo.patient (
--	patient_id int IDENTITY(1,1) NOT NULL,
--	date_of_birth date NULL,
--	first_name varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	last_name varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	max_distance int NOT NULL,
--	patient_email varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	phone_number varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	pre_existing_condition bit NOT NULL,
--	zipcode varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	CONSTRAINT PK__patient__4D5CE476DA5108B4 PRIMARY KEY (patient_id)
--);
--
--CREATE TABLE exposhr.dbo.provider (
--	provider_id int IDENTITY(1,1) NOT NULL,
--	city varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	main_address varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	main_email varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	main_phone_number varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	password varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	postal_code varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	provider_name varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	state varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	CONSTRAINT PK__provider__00E2131082AE2C6B PRIMARY KEY (provider_id)
--);
--
--CREATE TABLE exposhr.dbo.user_account (
--	id int IDENTITY(1,1) NOT NULL,
--	admin bit NOT NULL,
--	email varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	password varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	provider_id int NULL,
--	patient_id int NULL,
--	CONSTRAINT PK__user_acc__3213E83F64C2A191 PRIMARY KEY (id),
--	CONSTRAINT FKproviderID FOREIGN KEY (provider_id) REFERENCES exposhr.dbo.provider(provider_id),
--	CONSTRAINT FKuserpatientID FOREIGN KEY (patient_id) REFERENCES exposhr.dbo.patient(patient_id)
--);
--
--CREATE TABLE exposhr.dbo.location (
--	location_id int IDENTITY(1,1) NOT NULL,
--	address varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	city varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	contact_person varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	contact_person_phone_number varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	onsite bit NOT NULL,
--	postal_code varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	site_name varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	state varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	provider_provider_id int NULL,
--	CONSTRAINT PK__location__771831EAFB0E3746 PRIMARY KEY (location_id),
--	CONSTRAINT FKlocationproviderID FOREIGN KEY (provider_provider_id) REFERENCES exposhr.dbo.provider(provider_id)
--);
--
--CREATE TABLE exposhr.dbo.appt_mapping (
--	appointment_id int IDENTITY(1,1) NOT NULL,
--	appt_date date NULL,
--	appt_time varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	vaccine_mfr varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
--	location_id int NULL,
--	patient_id int NULL,
--	provider_id int NULL,
--	CONSTRAINT PK__appt_map__A50828FCCFA7D704 PRIMARY KEY (appointment_id),
--	CONSTRAINT FKapptproviderID FOREIGN KEY (provider_id) REFERENCES exposhr.dbo.provider(provider_id),
--	CONSTRAINT FKapptpatientID FOREIGN KEY (patient_id) REFERENCES exposhr.dbo.patient(patient_id),
--	CONSTRAINT FKapptlocationID FOREIGN KEY (location_id) REFERENCES exposhr.dbo.location(location_id)
--);

INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1992-02-17',N'Fredra',N'Hebditch',44,N'fhebditch0@uiuc.edu',N'717-243-1678',0,N'98087'),
	 ('1997-12-09',N'Elladine',N'Kordas',50,N'ekordas1@feedburner.com',N'393-658-8289',0,N'91316'),
	 ('1997-12-09',N'Elladine',N'Kordas',50,N'ekordas1@feedburner.com',N'393-658-8289',0,N'93224'),
	 ('1955-10-27',N'Joelie',N'Vaskin',40,N'jvaskin2@wiley.com',N'550-721-0717',1,N'91199'),
	 ('1992-06-23',N'Thorin',N'Elliot',23,N'telliot3@naver.com',N'340-349-4496',0,N'98233'),
	 ('1994-06-14',N'Geneva',N'Saban',11,N'gsaban4@businesswire.com',N'881-259-7194',0,N'93006'),
	 ('1990-10-03',N'Karoline',N'Stickford',22,N'kstickford5@netlog.com',N'781-831-4489',0,N'95992'),
	 ('1995-01-02',N'Elset',N'Gilbart',31,N'egilbart6@oaic.gov.au',N'932-127-9966',0,N'93245'),
	 ('1963-09-06',N'Cord',N'Brik',15,N'cbrik7@geocities.com',N'639-105-5977',0,N'93266'),
	 ('1970-09-26',N'Renell',N'Humbatch',12,N'rhumbatch8@washington.edu',N'722-219-4037',0,N'29636');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1998-05-20',N'Lurleen',N'Hayle',50,N'lhayle9@timesonline.co.uk',N'438-932-6889',1,N'91723'),
	 ('1951-01-30',N'Lurleen',N'Insworth',28,N'linswortha@usa.gov',N'132-878-8208',0,N'98331'),
	 ('1964-08-30',N'Ross',N'Roff',38,N'rroffb@samsung.com',N'200-141-7930',0,N'98384'),
	 ('1968-12-31',N'Hugh',N'Cashell',21,N'hcashellc@wisc.edu',N'319-180-6209',0,N'95437'),
	 ('1969-05-09',N'Candide',N'Streak',12,N'cstreakd@cbc.ca',N'823-235-3850',0,N'98284'),
	 ('1975-08-07',N'Ashien',N'Cuddihy',45,N'acuddihye@deliciousdays.com',N'570-737-6233',0,N'95951'),
	 ('1994-05-25',N'Royal',N'Thurber',39,N'rthurberf@ft.com',N'167-875-3690',0,N'94066'),
	 ('1994-08-25',N'Georgia',N'Raye',48,N'grayeg@scientificamerican.com',N'132-458-7547',0,N'99125'),
	 ('1961-12-26',N'Haley',N'Sang',37,N'hsangh@parallels.com',N'522-602-7948',0,N'93613'),
	 ('1984-01-30',N'Sheffie',N'Ballsdon',29,N'sballsdoni@howstuffworks.com',N'941-243-2867',1,N'95469');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1998-04-17',N'Kylie',N'Spivey',28,N'kspiveyj@samsung.com',N'711-115-3210',0,N'90717'),
	 ('1984-01-21',N'Kettie',N'Luckey',42,N'kluckeyk@tinyurl.com',N'471-759-7212',0,N'93210'),
	 ('1981-04-26',N'Aggy',N'Mates',22,N'amatesl@linkedin.com',N'330-460-9488',0,N'98602'),
	 ('1952-06-12',N'Otto',N'Ortelt',26,N'oorteltm@webeden.co.uk',N'550-907-5280',0,N'29129'),
	 ('1963-02-02',N'Tedd',N'Rishworth',25,N'trishworthn@shinystat.com',N'847-801-0109',0,N'91769'),
	 ('1990-09-30',N'Darlene',N'Stilling',38,N'dstillingo@wikipedia.org',N'972-961-4792',1,N'93505'),
	 ('1967-02-02',N'Jillana',N'Eastwood',47,N'jeastwoodp@biblegateway.com',N'985-569-3693',1,N'91745'),
	 ('1976-04-10',N'Lynnelle',N'Cisec',32,N'lcisecq@aol.com',N'594-631-3300',0,N'90043'),
	 ('1958-07-14',N'Claude',N'Tows',12,N'ctowsr@wisc.edu',N'388-445-3949',0,N'98668'),
	 ('1995-05-30',N'Corny',N'Marzelli',33,N'cmarzellis@so-net.ne.jp',N'864-479-0042',0,N'92178');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1995-03-22',N'Giulietta',N'Kiley',31,N'gkileyt@symantec.com',N'131-914-0123',0,N'98531'),
	 ('1956-02-15',N'Dudley',N'Biswell',45,N'dbiswellu@networkadvertising.org',N'256-385-0038',1,N'92384'),
	 ('1996-10-08',N'Derick',N'Sandiland',12,N'dsandilandv@auda.org.au',N'274-454-6798',0,N'94188'),
	 ('1989-01-08',N'Hedvig',N'Grundwater',23,N'hgrundwaterw@telegraph.co.uk',N'402-451-4669',0,N'95959'),
	 ('1994-09-24',N'Frederic',N'Ciccarello',34,N'fciccarellox@alexa.com',N'497-167-0010',0,N'91322'),
	 ('1956-04-05',N'Thomasina',N'Krishtopaittis',44,N'tkrishtopaittisy@drupal.org',N'880-441-6327',0,N'95335'),
	 ('1984-03-19',N'Pennie',N'Ogbourne',40,N'pogbournez@360.cn',N'895-449-3584',1,N'98260'),
	 ('1985-03-29',N'Kristofer',N'Parr',44,N'kparr10@salon.com',N'332-904-7222',0,N'91773'),
	 ('1995-11-19',N'Eleonora',N'Titterrell',15,N'etitterrell11@bbb.org',N'730-851-9955',0,N'93267'),
	 ('1965-01-14',N'Trixi',N'Alans',37,N'talans12@telegraph.co.uk',N'242-741-5086',0,N'98195');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1960-02-15',N'Peyton',N'Wakefield',23,N'pwakefield13@cnn.com',N'504-228-6102',0,N'90749'),
	 ('1982-07-14',N'Maje',N'Dear',23,N'mdear14@china.com.cn',N'561-326-6317',0,N'90640'),
	 ('1982-06-09',N'Monty',N'Berard',13,N'mberard15@intel.com',N'193-705-6354',0,N'92705'),
	 ('1998-11-07',N'Glennis',N'Slaight',13,N'gslaight16@blogspot.com',N'300-554-4293',0,N'95305'),
	 ('1987-09-20',N'Reynolds',N'Ghent',14,N'rghent17@foxnews.com',N'789-582-8712',0,N'95656'),
	 ('1968-02-24',N'Wake',N'Harriss',26,N'wharriss18@netscape.com',N'742-976-5586',0,N'91506'),
	 ('1951-12-15',N'Irena',N'Blann',49,N'iblann19@vistaprint.com',N'765-782-0878',0,N'90055'),
	 ('1970-06-20',N'Kalle',N'Telezhkin',16,N'ktelezhkin1a@weibo.com',N'148-131-3144',1,N'91357'),
	 ('1998-02-06',N'Fiann',N'Starte',42,N'fstarte1b@de.vu',N'827-582-6708',0,N'29229'),
	 ('1974-04-05',N'Anatola',N'Kibbee',26,N'akibbee1c@spiegel.de',N'470-633-2915',0,N'92536');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1979-07-21',N'Catherin',N'Slowan',12,N'cslowan1d@nbcnews.com',N'946-301-0124',1,N'92074'),
	 ('1989-12-22',N'Sosanna',N'Pordall',37,N'spordall1e@dyndns.org',N'779-381-4580',0,N'95747'),
	 ('1962-08-28',N'Babbie',N'Tomes',24,N'btomes1f@cocolog-nifty.com',N'707-638-9647',0,N'95798'),
	 ('1982-09-01',N'Fons',N'Mico',13,N'fmico1g@multiply.com',N'649-745-1852',0,N'98174'),
	 ('1980-04-01',N'Daryl',N'Belden',39,N'dbelden1h@eepurl.com',N'210-885-9356',0,N'91199'),
	 ('1977-07-23',N'Oneida',N'Samarth',22,N'osamarth1i@is.gd',N'271-451-0413',0,N'95992'),
	 ('1991-03-15',N'Chane',N'Turtle',31,N'cturtle1j@amazon.co.jp',N'338-574-8616',0,N'99350'),
	 ('1985-09-03',N'Danika',N'Cahani',49,N'dcahani1k@about.com',N'274-581-6427',1,N'92543'),
	 ('1992-09-26',N'Ruby',N'Lemmon',23,N'rlemmon1l@whitehouse.gov',N'762-376-4634',1,N'94080'),
	 ('1976-04-19',N'Dyan',N'Lidington',34,N'dlidington1m@youtu.be',N'129-285-4074',0,N'98031');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1956-02-04',N'Vasily',N'Dunbleton',50,N'vdunbleton1n@netlog.com',N'314-764-1660',1,N'95758'),
	 ('1973-01-11',N'Tadd',N'Hourstan',48,N'thourstan1o@thetimes.co.uk',N'701-186-5577',0,N'29340'),
	 ('1985-02-07',N'Benny',N'Sweetlove',21,N'bsweetlove1p@surveymonkey.com',N'333-385-3267',0,N'95826'),
	 ('1994-07-10',N'Torrin',N'Lound',46,N'tlound1q@instagram.com',N'638-103-6094',0,N'95638'),
	 ('1983-05-18',N'Milo',N'MacDougall',16,N'mmacdougall1r@army.mil',N'728-989-1034',0,N'95297'),
	 ('1965-08-09',N'Gerick',N'Pattini',45,N'gpattini1s@slideshare.net',N'328-799-5706',0,N'95534'),
	 ('1951-01-20',N'Phaedra',N'Besant',37,N'pbesant1t@histats.com',N'289-347-0307',0,N'98559'),
	 ('2000-04-12',N'Britteny',N'Chattell',17,N'bchattell1u@nih.gov',N'443-310-0680',0,N'92159'),
	 ('1965-05-17',N'Pandora',N'Dunridge',25,N'pdunridge1v@tinyurl.com',N'583-810-9727',1,N'93429'),
	 ('1980-09-07',N'Nicolais',N'Divis',30,N'ndivis1w@globo.com',N'889-341-1305',0,N'93616');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1972-09-22',N'Melinda',N'Ridde',16,N'mridde1x@wikia.com',N'516-264-4337',1,N'96124'),
	 ('1967-05-13',N'Meredith',N'Gozney',49,N'mgozney1y@smugmug.com',N'372-553-9052',1,N'93608'),
	 ('1950-10-03',N'Griswold',N'Reese',20,N'greese1z@w3.org',N'366-105-6486',1,N'98370'),
	 ('1958-03-17',N'Sigrid',N'Dureden',25,N'sdureden20@hostgator.com',N'713-976-8916',0,N'98038'),
	 ('1985-08-07',N'Buckie',N'Pendry',11,N'bpendry21@yahoo.com',N'549-175-4494',1,N'90604'),
	 ('1980-11-05',N'Madelon',N'Lorie',39,N'mlorie22@nature.com',N'278-946-8093',0,N'93519'),
	 ('1967-04-02',N'Arlan',N'Giscken',24,N'agiscken23@hostgator.com',N'370-577-3398',0,N'93044'),
	 ('1997-05-04',N'Keeley',N'Kemston',27,N'kkemston24@spiegel.de',N'948-210-1974',0,N'98644'),
	 ('1997-08-30',N'Celinka',N'Livard',20,N'clivard25@whitehouse.gov',N'232-114-5361',1,N'99301'),
	 ('1958-12-03',N'Lyda',N'Arnett',35,N'larnett26@ustream.tv',N'914-864-5883',0,N'95365');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1956-09-30',N'Isidora',N'Chevin',24,N'ichevin27@opensource.org',N'677-680-3962',0,N'95462'),
	 ('1997-10-11',N'Nataniel',N'Scholar',37,N'nscholar28@fc2.com',N'692-896-2147',1,N'91126'),
	 ('1952-09-10',N'Leroy',N'Hazael',50,N'lhazael29@infoseek.co.jp',N'263-580-3457',1,N'95109'),
	 ('1987-01-22',N'Rhys',N'Real',10,N'rreal2a@barnesandnoble.com',N'375-398-7162',1,N'93608'),
	 ('1988-04-09',N'Eli',N'Clarson',33,N'eclarson2b@feedburner.com',N'942-437-4513',0,N'98256'),
	 ('1963-08-11',N'Thor',N'Itzhaki',15,N'titzhaki2c@pen.io',N'430-236-6508',0,N'91793'),
	 ('1966-10-26',N'Rea',N'Shakshaft',20,N'rshakshaft2d@csmonitor.com',N'339-322-6354',0,N'98160'),
	 ('1998-06-20',N'Nealson',N'Hentze',17,N'nhentze2e@diigo.com',N'176-943-7113',0,N'90623'),
	 ('1984-01-09',N'Arleta',N'Angus',18,N'aangus2f@ocn.ne.jp',N'501-954-5769',0,N'95257'),
	 ('1974-12-12',N'Branden',N'Vinecombe',36,N'bvinecombe2g@smh.com.au',N'150-792-6170',0,N'98270');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1988-09-13',N'Jordana',N'Waldie',44,N'jwaldie2h@youtube.com',N'639-744-2396',1,N'93270'),
	 ('1965-05-26',N'Zondra',N'Shorie',20,N'zshorie2i@shareasale.com',N'854-373-9629',1,N'93067'),
	 ('1980-06-24',N'Mar',N'Evemy',35,N'mevemy2j@answers.com',N'153-731-8214',0,N'98552'),
	 ('1955-08-10',N'Davide',N'Stanett',19,N'dstanett2k@sun.com',N'786-784-3246',0,N'98255'),
	 ('1984-01-16',N'Catlin',N'Petkov',39,N'cpetkov2l@prlog.org',N'625-217-2066',0,N'94507'),
	 ('1995-10-30',N'Hale',N'Tilson',19,N'htilson2m@rediff.com',N'536-531-4310',0,N'92837'),
	 ('1962-01-28',N'Corette',N'Collecott',38,N'ccollecott2n@sfgate.com',N'768-428-6156',1,N'95418'),
	 ('1962-08-28',N'Hale',N'Crowdy',27,N'hcrowdy2o@mail.ru',N'566-484-4052',0,N'98528'),
	 ('1974-10-27',N'Glenn',N'Jaspar',43,N'gjaspar2p@meetup.com',N'733-930-1922',0,N'93064'),
	 ('1973-03-13',N'Bail',N'Codron',12,N'bcodron2q@boston.com',N'925-699-9356',0,N'91798');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1972-05-03',N'Gawen',N'Gronaver',17,N'ggronaver2r@ifeng.com',N'314-328-0382',0,N'98108'),
	 ('1982-08-23',N'Gayle',N'Lundy',18,N'glundy2s@theglobeandmail.com',N'655-776-1932',1,N'91759'),
	 ('1981-04-23',N'Sutherland',N'Cristea',43,N'scristea2t@soundcloud.com',N'279-561-4157',0,N'90711'),
	 ('1999-08-06',N'Adolphus',N'Claessens',42,N'aclaessens2u@wordpress.org',N'199-147-9988',0,N'94567'),
	 ('1990-12-15',N'Vinson',N'Greest',33,N'vgreest2v@cbc.ca',N'533-835-0309',0,N'95536'),
	 ('1958-11-29',N'Sal',N'Mowday',32,N'smowday2w@harvard.edu',N'246-967-6966',0,N'95161'),
	 ('1968-12-25',N'Rasia',N'O''Lenane',46,N'rolenane2x@gov.uk',N'507-685-7126',0,N'29691'),
	 ('1982-12-01',N'Ardelia',N'Kleynen',34,N'akleynen2y@opensource.org',N'670-908-9881',0,N'94171'),
	 ('1983-05-18',N'Damien',N'Benstead',18,N'dbenstead2z@google.ca',N'623-532-7239',0,N'95818'),
	 ('1952-05-12',N'Easter',N'Eadington',42,N'eeadington30@amazon.co.uk',N'341-697-1263',0,N'92677');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1979-09-14',N'Rea',N'Kennelly',20,N'rkennelly31@163.com',N'111-169-0073',1,N'29385'),
	 ('1976-02-16',N'Shirlene',N'Thorby',36,N'sthorby32@microsoft.com',N'456-700-1246',0,N'98204'),
	 ('1961-05-31',N'Pippy',N'Tackes',24,N'ptackes33@godaddy.com',N'556-682-7124',0,N'93844'),
	 ('1973-11-19',N'Loreen',N'Hanbury-Brown',38,N'lhanburybrown34@cocolog-nifty.com',N'516-118-6426',1,N'98199'),
	 ('1964-05-24',N'Shara',N'Riggulsford',28,N'sriggulsford35@blogspot.com',N'653-413-3942',0,N'90263'),
	 ('1991-06-14',N'Revkah',N'Kolyagin',18,N'rkolyagin36@yandex.ru',N'149-114-3929',0,N'94060'),
	 ('1956-05-30',N'Rochette',N'De Hailes',40,N'rdehailes37@wikispaces.com',N'168-304-6744',1,N'98671'),
	 ('1977-05-18',N'Michaelina',N'Eyers',37,N'meyers38@scribd.com',N'657-959-2660',0,N'93778'),
	 ('1976-01-10',N'Anton',N'Kealy',39,N'akealy39@prlog.org',N'816-127-8600',0,N'92423'),
	 ('1977-08-30',N'Carmel',N'O''Luney',41,N'coluney3a@wordpress.com',N'472-250-0920',0,N'94806');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1998-11-27',N'Katey',N'Cobley',42,N'kcobley3b@yellowbook.com',N'970-246-2764',0,N'91756'),
	 ('1969-11-28',N'Celina',N'Rawling',10,N'crawling3c@artisteer.com',N'299-731-8312',0,N'90058'),
	 ('1966-10-24',N'Leena',N'Gothup',40,N'lgothup3d@wikispaces.com',N'311-347-8882',1,N'93642'),
	 ('1955-10-26',N'Dick',N'Syne',24,N'dsyne3e@virginia.edu',N'291-571-2592',0,N'95944'),
	 ('1988-07-05',N'Hoyt',N'Holton',29,N'hholton3f@sciencedaily.com',N'924-683-7819',0,N'94590'),
	 ('1980-03-30',N'Sabina',N'Westerman',18,N'swesterman3g@google.co.jp',N'507-657-6188',0,N'95157'),
	 ('1955-11-06',N'Wendye',N'Crilley',18,N'wcrilley3h@delicious.com',N'288-887-7062',0,N'98378'),
	 ('1976-12-21',N'Madeline',N'Bartunek',50,N'mbartunek3i@scientificamerican.com',N'703-690-9848',1,N'95923'),
	 ('1963-10-07',N'Ric',N'Farney',14,N'rfarney3j@hubpages.com',N'872-526-3222',0,N'98953'),
	 ('1995-08-20',N'Dayna',N'Allum',13,N'dallum3k@newsvine.com',N'732-960-5208',0,N'95823');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1975-06-21',N'Arte',N'Shiel',13,N'ashiel3l@seattletimes.com',N'650-419-0244',0,N'92304'),
	 ('1953-01-05',N'Devin',N'Gorrick',42,N'dgorrick3m@dion.ne.jp',N'225-120-0449',0,N'92561'),
	 ('1986-03-29',N'Revkah',N'Filipychev',49,N'rfilipychev3n@wikia.com',N'475-179-7622',1,N'94948'),
	 ('1969-11-02',N'Tasia',N'McInulty',30,N'tmcinulty3o@jugem.jp',N'855-655-8992',0,N'93111'),
	 ('1977-11-19',N'Nicki',N'Crooks',46,N'ncrooks3p@moonfruit.com',N'247-639-5395',0,N'95451'),
	 ('1973-03-22',N'Ursula',N'Kidman',15,N'ukidman3q@vistaprint.com',N'223-531-8295',1,N'92255'),
	 ('1953-12-13',N'Jonell',N'Seares',33,N'jseares3r@list-manage.com',N'364-969-5120',0,N'95549'),
	 ('1953-07-05',N'Hymie',N'Breeze',46,N'hbreeze3s@mozilla.com',N'483-225-7006',0,N'92054'),
	 ('1973-01-23',N'Allegra',N'Skally',46,N'askally3t@yale.edu',N'869-522-2003',0,N'29330'),
	 ('1985-06-23',N'Mil',N'Ambrois',41,N'mambrois3u@examiner.com',N'253-698-3946',0,N'93285');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1996-06-13',N'Norris',N'Walewski',23,N'nwalewski3v@nature.com',N'138-619-3821',0,N'98134'),
	 ('1989-06-18',N'Aileen',N'Iacovino',34,N'aiacovino3w@vk.com',N'945-875-4951',0,N'92816'),
	 ('1974-06-07',N'Padraig',N'Clardge',17,N'pclardge3x@xrea.com',N'716-461-1697',0,N'93463'),
	 ('1977-08-17',N'Ransom',N'McSperrin',42,N'rmcsperrin3y@newyorker.com',N'640-135-4857',0,N'92147'),
	 ('1953-12-23',N'Lorenzo',N'Gookey',13,N'lgookey3z@answers.com',N'337-256-8834',0,N'90650'),
	 ('1983-03-20',N'Nicoli',N'Rowston',25,N'nrowston40@rediff.com',N'805-104-2236',0,N'29829'),
	 ('1999-01-21',N'Ker',N'Applewhaite',10,N'kapplewhaite41@goo.gl',N'383-482-1825',0,N'98540'),
	 ('1990-08-16',N'Hildegaard',N'Ough',13,N'hough42@vistaprint.com',N'309-712-8660',0,N'95130'),
	 ('1996-04-27',N'Boone',N'Kinnett',26,N'bkinnett43@canalblog.com',N'464-706-5385',0,N'94129'),
	 ('1995-05-13',N'Deeann',N'Gard',12,N'dgard44@sun.com',N'718-901-1906',0,N'95842');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1957-05-20',N'Freedman',N'Scantleberry',29,N'fscantleberry45@ucoz.ru',N'524-299-6704',0,N'98043'),
	 ('1974-05-11',N'Deloria',N'McPheat',35,N'dmcpheat46@kickstarter.com',N'679-159-8166',0,N'91208'),
	 ('1983-03-08',N'Tarah',N'Hanner',42,N'thanner47@go.com',N'936-158-2656',0,N'95372'),
	 ('1994-02-25',N'Klement',N'Benz',12,N'kbenz48@ibm.com',N'503-293-7694',0,N'93920'),
	 ('1985-01-23',N'Granville',N'Forth',18,N'gforth49@alibaba.com',N'505-985-7832',0,N'99017'),
	 ('1989-07-17',N'Sydel',N'Giacovazzo',48,N'sgiacovazzo4a@eepurl.com',N'695-106-8924',0,N'92274'),
	 ('1998-07-07',N'Wainwright',N'Bannon',38,N'wbannon4b@soundcloud.com',N'174-842-6509',0,N'95254'),
	 ('1991-07-22',N'Bess',N'Osmint',21,N'bosmint4c@google.pl',N'765-983-4303',1,N'98057'),
	 ('1983-09-30',N'Salvador',N'Ickov',28,N'sickov4d@tumblr.com',N'305-545-6858',0,N'29925'),
	 ('1952-09-13',N'Adore',N'Extill',17,N'aextill4e@vkontakte.ru',N'426-366-6261',0,N'90755');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1969-01-18',N'Jackie',N'Sneezum',17,N'jsneezum4f@cyberchimps.com',N'329-112-3544',0,N'95126'),
	 ('1993-08-18',N'Marco',N'Rafe',49,N'mrafe4g@youku.com',N'941-798-7811',1,N'92508'),
	 ('1951-11-24',N'Torrin',N'Tinmouth',50,N'ttinmouth4h@angelfire.com',N'827-766-5445',0,N'95968'),
	 ('1995-08-10',N'Vyky',N'Petschel',38,N'vpetschel4i@springer.com',N'701-207-8740',0,N'98356'),
	 ('1958-06-28',N'Helenelizabeth',N'Tizzard',40,N'htizzard4j@buzzfeed.com',N'625-447-7572',1,N'29423'),
	 ('1990-05-08',N'Rennie',N'Elsip',23,N'relsip4k@wikimedia.org',N'835-763-8240',0,N'95606'),
	 ('1982-08-16',N'Lucita',N'Joskovitch',17,N'ljoskovitch4l@google.fr',N'627-527-1386',0,N'92225'),
	 ('1993-04-13',N'Jonis',N'Curless',18,N'jcurless4m@usgs.gov',N'363-281-5526',1,N'96013'),
	 ('1985-06-29',N'Krissie',N'Emmines',35,N'kemmines4n@newyorker.com',N'113-125-1193',0,N'98236'),
	 ('1985-01-05',N'Florida',N'Hollingby',44,N'fhollingby4o@123-reg.co.uk',N'956-955-1072',0,N'90296');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1969-05-28',N'Mickey',N'MacGuiness',49,N'mmacguiness4p@mail.ru',N'895-102-1705',0,N'94659'),
	 ('1994-06-23',N'Karlee',N'Cowling',15,N'kcowling4q@wp.com',N'772-541-5492',0,N'98555'),
	 ('1960-06-05',N'Cherice',N'Anderl',50,N'canderl4r@forbes.com',N'547-336-9408',0,N'95686'),
	 ('1998-02-01',N'Elyse',N'Brinklow',40,N'ebrinklow4s@blogtalkradio.com',N'867-639-5556',1,N'94661'),
	 ('1975-03-02',N'Teodorico',N'Snoxill',47,N'tsnoxill4t@answers.com',N'445-115-7086',0,N'93030'),
	 ('1953-05-17',N'Gordan',N'Hasted',35,N'ghasted4u@usnews.com',N'297-933-2984',0,N'92806'),
	 ('1952-04-29',N'Frasquito',N'Welland',17,N'fwelland4v@cnn.com',N'883-589-0316',0,N'98363'),
	 ('1960-12-29',N'Gunner',N'Jiggins',30,N'gjiggins4w@facebook.com',N'773-269-9092',0,N'93234'),
	 ('1960-08-22',N'Holly',N'Shawdforth',45,N'hshawdforth4x@nationalgeographic.com',N'102-706-3380',0,N'92086'),
	 ('1974-10-02',N'Ginni',N'Coverlyn',27,N'gcoverlyn4y@examiner.com',N'689-819-7117',0,N'91203');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1970-10-13',N'Jeni',N'Parlour',41,N'jparlour4z@vinaora.com',N'585-793-1595',0,N'95445'),
	 ('1993-09-22',N'Alvie',N'Bristo',22,N'abristo50@cisco.com',N'905-890-2993',0,N'29067'),
	 ('1998-01-27',N'Kristofer',N'McDonand',31,N'kmcdonand51@wikipedia.org',N'325-978-4953',1,N'95469'),
	 ('1994-07-21',N'Eddy',N'Whal',37,N'ewhal52@sina.com.cn',N'395-869-7192',0,N'98923'),
	 ('1965-10-25',N'Corena',N'Goranov',41,N'cgoranov53@weebly.com',N'308-819-7755',0,N'29177'),
	 ('1987-11-11',N'Giacobo',N'Mabee',43,N'gmabee54@amazon.com',N'902-456-2513',1,N'96127'),
	 ('1979-10-24',N'Kingston',N'Stent',28,N'kstent55@wikia.com',N'472-333-3395',0,N'29824'),
	 ('1989-05-25',N'Wiley',N'Arrault',27,N'warrault56@newsvine.com',N'663-384-4816',0,N'93912'),
	 ('1997-11-04',N'Bryana',N'Savile',36,N'bsavile57@opensource.org',N'497-227-8012',0,N'95344'),
	 ('1966-05-04',N'Sondra',N'Gravie',21,N'sgravie58@liveinternet.ru',N'491-640-8098',0,N'95073');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1954-05-25',N'Hamilton',N'Woolaghan',40,N'hwoolaghan59@china.com.cn',N'892-926-0593',0,N'29333'),
	 ('1986-10-04',N'Duky',N'Matias',20,N'dmatias5a@intel.com',N'371-552-8240',0,N'94273'),
	 ('1961-04-28',N'Cherise',N'Atmore',42,N'catmore5b@bloglines.com',N'804-431-9134',0,N'29684'),
	 ('1959-09-28',N'Bertrando',N'Allett',32,N'ballett5c@spotify.com',N'304-933-1968',0,N'93247'),
	 ('1981-11-14',N'Burt',N'Mahy',10,N'bmahy5d@dell.com',N'292-678-9086',0,N'94156'),
	 ('1964-11-15',N'Carmita',N'Dussy',18,N'cdussy5e@weebly.com',N'431-134-9851',0,N'99403'),
	 ('1994-08-18',N'Ania',N'Cromwell',49,N'acromwell5f@smugmug.com',N'568-676-1573',0,N'90245'),
	 ('1977-11-13',N'Barbi',N'Gantzman',46,N'bgantzman5g@symantec.com',N'864-233-4739',0,N'90404'),
	 ('1952-06-20',N'Lexi',N'Arrol',24,N'larrol5h@biglobe.ne.jp',N'573-474-9935',0,N'90022'),
	 ('1994-02-14',N'Abby',N'Carverhill',20,N'acarverhill5i@uiuc.edu',N'226-365-0399',0,N'98538');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1955-05-15',N'Goldy',N'Sprowell',18,N'gsprowell5j@admin.ch',N'531-313-5829',0,N'93036'),
	 ('1978-05-17',N'Basilio',N'Blacker',20,N'bblacker5k@facebook.com',N'611-802-7285',0,N'99030'),
	 ('1955-01-02',N'Gail',N'Peetermann',48,N'gpeetermann5l@1und1.de',N'455-525-4738',0,N'93784'),
	 ('1951-09-24',N'Free',N'McVicar',30,N'fmcvicar5m@purevolume.com',N'403-452-7410',0,N'91987'),
	 ('1966-01-14',N'Brianne',N'Waylen',23,N'bwaylen5n@statcounter.com',N'409-617-0125',0,N'29685'),
	 ('1993-03-10',N'Lucy',N'Lavrinov',40,N'llavrinov5o@businessweek.com',N'422-923-3500',0,N'90731'),
	 ('1967-09-23',N'Adriana',N'McKinna',15,N'amckinna5p@wunderground.com',N'800-418-6149',0,N'98558'),
	 ('1986-04-27',N'Anallese',N'Rump',21,N'arump5q@drupal.org',N'797-372-8048',0,N'98203'),
	 ('1967-06-28',N'Aundrea',N'Coling',14,N'acoling5r@google.it',N'340-815-2934',1,N'29355'),
	 ('1986-09-28',N'Corly',N'Caistor',30,N'ccaistor5s@mayoclinic.com',N'919-704-5835',0,N'95601');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1969-08-25',N'Jennilee',N'Poulton',12,N'jpoulton5t@prweb.com',N'527-779-2028',0,N'98358'),
	 ('1986-04-20',N'Franky',N'Code',21,N'fcode5u@ucla.edu',N'779-578-8267',0,N'91708'),
	 ('1970-02-17',N'Dulcy',N'Rudgley',28,N'drudgley5v@freewebs.com',N'503-457-5623',0,N'94022'),
	 ('1972-11-14',N'Hanna',N'Cunnington',40,N'hcunnington5w@creativecommons.org',N'814-895-1335',1,N'92692'),
	 ('1975-08-09',N'Fernandina',N'Pattingson',28,N'fpattingson5x@wordpress.com',N'661-716-3541',0,N'93099'),
	 ('1967-09-14',N'Amandie',N'Belmont',21,N'abelmont5y@gmpg.org',N'384-330-7916',0,N'94089'),
	 ('1976-10-09',N'Lucien',N'Bedward',49,N'lbedward5z@webmd.com',N'861-360-4998',0,N'95111'),
	 ('1969-04-28',N'Vale',N'Gratland',33,N'vgratland60@last.fm',N'624-559-6053',1,N'93261'),
	 ('1996-08-13',N'Hill',N'Zellick',12,N'hzellick61@washington.edu',N'648-950-0297',0,N'94497'),
	 ('1959-05-18',N'Gustavo',N'Kybbye',45,N'gkybbye62@freewebs.com',N'955-991-7922',0,N'93730');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1951-02-20',N'Kylie',N'Masey',36,N'kmasey63@whitehouse.gov',N'533-408-3388',1,N'99131'),
	 ('1960-05-14',N'Haywood',N'Akast',34,N'hakast64@dagondesign.com',N'250-346-4813',0,N'91701'),
	 ('1997-10-12',N'Marcellina',N'Beddard',33,N'mbeddard65@marketwatch.com',N'230-324-1756',0,N'99320'),
	 ('1977-10-21',N'Nina',N'Jessel',12,N'njessel66@europa.eu',N'404-315-9368',0,N'92142'),
	 ('1954-12-22',N'Vic',N'Brodeau',32,N'vbrodeau67@myspace.com',N'132-297-9853',0,N'92801'),
	 ('1983-11-02',N'Theresa',N'Jowett',34,N'tjowett68@wufoo.com',N'158-661-6894',0,N'98282'),
	 ('1954-02-25',N'Harwell',N'Hugnin',35,N'hhugnin69@cafepress.com',N'841-921-1101',0,N'95033'),
	 ('1964-02-16',N'Mandi',N'Rainton',16,N'mrainton6a@sciencedirect.com',N'869-677-9926',0,N'93130'),
	 ('1977-11-27',N'Eugenie',N'Antonchik',34,N'eantonchik6b@histats.com',N'329-271-5330',0,N'98586'),
	 ('1990-05-17',N'Lucius',N'Georgius',30,N'lgeorgius6c@rakuten.co.jp',N'522-167-5292',1,N'90037');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1983-10-28',N'Camellia',N'Stather',13,N'cstather6d@sphinn.com',N'155-138-8255',1,N'98801'),
	 ('1996-11-28',N'Claudianus',N'Buckeridge',21,N'cbuckeridge6e@google.com',N'337-868-3220',0,N'91222'),
	 ('1993-05-15',N'Dorie',N'Verduin',22,N'dverduin6f@umich.edu',N'336-325-9328',0,N'29808'),
	 ('1979-08-11',N'Valentino',N'Meyer',32,N'vmeyer6g@amazon.com',N'276-262-6131',0,N'94250'),
	 ('1978-04-17',N'Maurizia',N'Pennock',32,N'mpennock6h@theguardian.com',N'460-667-0075',0,N'93002'),
	 ('1952-12-16',N'Linoel',N'Roomes',19,N'lroomes6i@google.fr',N'596-942-2154',0,N'29166'),
	 ('1975-04-27',N'Mallory',N'Kid',33,N'mkid6j@naver.com',N'624-333-4854',0,N'93604'),
	 ('1957-01-20',N'Dyan',N'Strange',48,N'dstrange6k@gov.uk',N'666-264-3923',0,N'94704'),
	 ('1983-03-21',N'Elysee',N'Lyard',22,N'elyard6l@domainmarket.com',N'245-496-9635',0,N'98591'),
	 ('1994-05-04',N'Odo',N'Teasdale-Markie',39,N'oteasdalemarkie6m@un.org',N'411-290-7384',1,N'29072');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1983-11-24',N'Galina',N'Fores',44,N'gfores6n@virginia.edu',N'915-913-4910',0,N'90746'),
	 ('1964-04-25',N'Ellwood',N'Sayes',29,N'esayes6o@cisco.com',N'246-809-4921',0,N'29635'),
	 ('1952-12-02',N'Colman',N'Sebire',15,N'csebire6p@discuz.net',N'842-691-0543',0,N'98270'),
	 ('1961-04-19',N'Casper',N'Demer',13,N'cdemer6q@ebay.com',N'274-186-2062',0,N'95678'),
	 ('1958-01-24',N'Lisha',N'Jansa',34,N'ljansa6r@stumbleupon.com',N'672-274-1769',0,N'95532'),
	 ('1981-04-24',N'Trixy',N'Chene',36,N'tchene6s@bing.com',N'833-515-7113',0,N'92825'),
	 ('1985-09-19',N'Bernadette',N'Dobeson',10,N'bdobeson6t@e-recht24.de',N'673-800-5767',0,N'91199'),
	 ('1997-05-04',N'Joyce',N'Muncer',11,N'jmuncer6u@phoca.cz',N'241-382-9594',0,N'92561'),
	 ('1972-09-06',N'Korey',N'Scherme',17,N'kscherme6v@opera.com',N'849-591-1689',0,N'93776'),
	 ('1966-07-05',N'Bibbye',N'Cuppleditch',31,N'bcuppleditch6w@google.de',N'814-289-7894',0,N'29487');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1968-12-26',N'Caldwell',N'Jobbins',15,N'cjobbins6x@nba.com',N'663-395-6781',0,N'94299'),
	 ('1985-01-27',N'Kaia',N'Wickey',24,N'kwickey6y@wsj.com',N'649-320-5536',0,N'95005'),
	 ('1974-11-18',N'Jesus',N'Ghelarducci',47,N'jghelarducci6z@histats.com',N'315-222-2049',0,N'95159'),
	 ('1992-03-23',N'Keelia',N'Fransseni',43,N'kfransseni70@cbslocal.com',N'498-172-9744',0,N'94530'),
	 ('1993-01-01',N'Gibb',N'Eveling',48,N'geveling71@yandex.ru',N'937-862-2266',0,N'29492'),
	 ('1972-09-28',N'Rhodie',N'Salvati',40,N'rsalvati72@eepurl.com',N'981-778-5881',0,N'90732'),
	 ('1994-10-26',N'Wittie',N'Abelwhite',17,N'wabelwhite73@jimdo.com',N'648-868-8610',0,N'93061'),
	 ('1954-07-14',N'Hermine',N'Humpherston',31,N'hhumpherston74@answers.com',N'464-959-6841',0,N'94129'),
	 ('1952-11-30',N'Valentine',N'Curtin',13,N'vcurtin75@livejournal.com',N'128-446-4613',0,N'98832'),
	 ('1956-11-01',N'Brandon',N'Keeling',17,N'bkeeling76@go.com',N'750-372-9572',0,N'94618');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1959-09-29',N'Adrea',N'Litzmann',27,N'alitzmann77@list-manage.com',N'825-317-9243',0,N'98438'),
	 ('1964-11-09',N'Daphne',N'McMickan',17,N'dmcmickan78@jiathis.com',N'751-933-2798',0,N'90743'),
	 ('1963-09-28',N'Cam',N'McCathay',45,N'cmccathay79@spiegel.de',N'177-392-2319',0,N'95231'),
	 ('1989-08-12',N'Anatol',N'Corey',28,N'acorey7a@ftc.gov',N'560-661-8870',0,N'95762'),
	 ('1985-04-07',N'Everett',N'Ley',31,N'eley7b@yale.edu',N'758-689-9590',0,N'95819'),
	 ('1962-10-23',N'Denice',N'Boston',23,N'dboston7c@boston.com',N'343-115-6078',1,N'95445'),
	 ('1951-11-22',N'Trisha',N'Blazey',50,N'tblazey7d@npr.org',N'405-704-0332',0,N'95233'),
	 ('1956-05-16',N'Rogerio',N'Mollatt',17,N'rmollatt7e@barnesandnoble.com',N'962-905-6084',0,N'93614'),
	 ('1958-10-28',N'Owen',N'Conerding',33,N'oconerding7f@tamu.edu',N'462-904-3872',0,N'92395'),
	 ('1977-01-27',N'Laurette',N'Caldecutt',19,N'lcaldecutt7g@businessinsider.com',N'412-345-3116',0,N'90248');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1977-10-04',N'Merle',N'Blitz',22,N'mblitz7h@creativecommons.org',N'339-850-0110',0,N'93726'),
	 ('1991-04-06',N'Florette',N'Smalecombe',30,N'fsmalecombe7i@typepad.com',N'534-891-5645',0,N'90034'),
	 ('1994-11-28',N'Dag',N'Dwerryhouse',14,N'ddwerryhouse7j@ustream.tv',N'893-754-4372',0,N'95156'),
	 ('1985-04-21',N'Victor',N'Hilldrup',16,N'vhilldrup7k@skyrock.com',N'335-897-2932',1,N'92113'),
	 ('1990-07-12',N'Ruthy',N'Wenden',39,N'rwenden7l@linkedin.com',N'192-365-3061',0,N'94249'),
	 ('1984-04-28',N'Brenn',N'Houlworth',31,N'bhoulworth7m@globo.com',N'602-114-9519',0,N'94513'),
	 ('1974-08-11',N'Aharon',N'Alsop',13,N'aalsop7n@webnode.com',N'978-240-1752',0,N'94168'),
	 ('1960-11-15',N'Alyosha',N'Leipoldt',45,N'aleipoldt7o@va.gov',N'611-347-4565',0,N'91750'),
	 ('1984-02-24',N'Dorolice',N'Baxstair',21,N'dbaxstair7p@huffingtonpost.com',N'756-459-5748',0,N'95451'),
	 ('1989-06-03',N'Bryana',N'Kynge',30,N'bkynge7q@wikipedia.org',N'125-752-7603',0,N'92226');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1961-04-28',N'Gaile',N'McTrustie',13,N'gmctrustie7r@microsoft.com',N'826-944-3842',0,N'98177'),
	 ('1967-02-26',N'Loralyn',N'Readwing',36,N'lreadwing7s@telegraph.co.uk',N'935-967-4194',0,N'98845'),
	 ('1955-08-16',N'Freeman',N'Fareweather',48,N'ffareweather7t@kickstarter.com',N'400-221-0510',0,N'29727'),
	 ('1995-05-29',N'Suzann',N'McCromley',33,N'smccromley7u@reverbnation.com',N'104-341-2897',0,N'99027'),
	 ('1955-06-11',N'Velvet',N'Darrow',34,N'vdarrow7v@vinaora.com',N'434-570-7996',1,N'94978'),
	 ('1951-05-20',N'Aymer',N'Dodridge',42,N'adodridge7w@intel.com',N'707-426-0872',0,N'92589'),
	 ('1983-11-07',N'Georas',N'Pinckney',32,N'gpinckney7x@clickbank.net',N'574-370-5284',0,N'93550'),
	 ('1965-01-27',N'Leilah',N'Tollady',35,N'ltollady7y@xing.com',N'163-722-4903',0,N'92258'),
	 ('1990-10-24',N'Wallas',N'Chaim',21,N'wchaim7z@businessweek.com',N'146-167-6549',0,N'98455'),
	 ('1964-10-18',N'Temple',N'Abbes',48,N'tabbes80@photobucket.com',N'748-287-6783',1,N'93604');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1974-01-15',N'Marion',N'Kassidy',32,N'mkassidy81@angelfire.com',N'756-109-8133',0,N'98374'),
	 ('1966-02-11',N'Devondra',N'Kreutzer',19,N'dkreutzer82@foxnews.com',N'267-438-7452',0,N'90504'),
	 ('1969-01-26',N'Lettie',N'Yexley',19,N'lyexley83@nature.com',N'337-484-5363',0,N'29582'),
	 ('1983-01-30',N'Emlynne',N'Rappaport',24,N'erappaport84@jalbum.net',N'704-520-4311',1,N'94540'),
	 ('1953-11-18',N'De witt',N'Broader',32,N'dbroader85@webmd.com',N'145-380-6042',0,N'94575'),
	 ('1968-07-29',N'Fabien',N'Graddon',45,N'fgraddon86@multiply.com',N'959-938-0496',0,N'98281'),
	 ('1964-02-15',N'Christan',N'Bark',21,N'cbark87@blogger.com',N'733-533-9506',0,N'95155'),
	 ('1995-12-16',N'Jamil',N'Luipold',34,N'jluipold88@is.gd',N'999-745-5164',0,N'29318'),
	 ('1975-11-28',N'Becca',N'McClifferty',44,N'bmcclifferty89@reverbnation.com',N'556-519-3048',0,N'93002'),
	 ('1969-12-29',N'Rogers',N'Jaze',48,N'rjaze8a@msn.com',N'530-663-8402',1,N'98283');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1971-10-06',N'Ilaire',N'Kruger',39,N'ikruger8b@elegantthemes.com',N'425-139-0275',0,N'94965'),
	 ('1987-10-01',N'Fredia',N'Pena',31,N'fpena8c@bbb.org',N'468-606-6636',1,N'93666'),
	 ('1956-02-07',N'Fawne',N'Gott',39,N'fgott8d@e-recht24.de',N'471-176-7953',0,N'93562'),
	 ('1975-01-01',N'Hally',N'Lipprose',26,N'hlipprose8e@slideshare.net',N'947-326-0741',0,N'93528'),
	 ('1957-05-31',N'Darby',N'Vasey',14,N'dvasey8f@about.com',N'304-944-5036',1,N'95113'),
	 ('1976-11-18',N'Margit',N'Sutter',47,N'msutter8g@unesco.org',N'611-420-8859',0,N'98358'),
	 ('1973-08-09',N'Homerus',N'Kelshaw',23,N'hkelshaw8h@facebook.com',N'501-107-2862',1,N'29842'),
	 ('1956-07-09',N'Stefa',N'Boyle',34,N'sboyle8i@bloglines.com',N'880-145-5457',0,N'95206'),
	 ('1976-02-09',N'Kerr',N'Petrollo',31,N'kpetrollo8j@cnbc.com',N'363-891-4830',0,N'99403'),
	 ('1997-05-11',N'Gray',N'Merrien',33,N'gmerrien8k@eepurl.com',N'995-898-1437',0,N'92177');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1957-01-27',N'Lyle',N'Di Angelo',29,N'ldiangelo8l@google.ru',N'169-495-8201',0,N'92225'),
	 ('1966-02-18',N'Helga',N'Buncombe',45,N'hbuncombe8m@php.net',N'142-384-1535',1,N'98860'),
	 ('1953-01-14',N'Putnem',N'Saurin',20,N'psaurin8n@bbc.co.uk',N'148-724-0802',0,N'91510'),
	 ('1975-07-25',N'Marwin',N'Wiszniewski',15,N'mwiszniewski8o@vkontakte.ru',N'348-722-5643',0,N'92543'),
	 ('1988-12-31',N'Anetta',N'Wardel',18,N'awardel8p@taobao.com',N'362-837-0383',0,N'29598'),
	 ('1990-03-15',N'Hobie',N'Janosevic',42,N'hjanosevic8q@java.com',N'853-364-8315',0,N'92003'),
	 ('1966-09-02',N'Albert',N'Sex',10,N'asex8r@tumblr.com',N'178-795-8746',0,N'91341'),
	 ('1973-01-21',N'Domini',N'Gimenez',23,N'dgimenez8s@nydailynews.com',N'354-300-9118',0,N'95363'),
	 ('1974-04-27',N'Reese',N'Speenden',41,N'rspeenden8t@stanford.edu',N'757-256-8756',0,N'29031'),
	 ('1963-01-30',N'Cally',N'Buchanan',24,N'cbuchanan8u@europa.eu',N'961-783-3185',0,N'98208');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1962-02-12',N'Murielle',N'Jedrysik',44,N'mjedrysik8v@jalbum.net',N'854-225-2642',0,N'95193'),
	 ('1961-12-11',N'Edna',N'Gorstidge',22,N'egorstidge8w@themeforest.net',N'977-336-4937',0,N'95966'),
	 ('1955-12-10',N'Eustace',N'Beltzner',22,N'ebeltzner8x@imdb.com',N'935-761-8094',0,N'98373'),
	 ('1984-12-16',N'Brady',N'Lexa',33,N'blexa8y@canalblog.com',N'798-575-9249',0,N'95303'),
	 ('1985-12-22',N'Gregoire',N'Lemonby',48,N'glemonby8z@printfriendly.com',N'300-609-5284',1,N'98126'),
	 ('1989-11-18',N'Clementine',N'Maraga',28,N'cmaraga90@techcrunch.com',N'663-500-7996',0,N'90293'),
	 ('1973-09-05',N'Margy',N'Tibalt',19,N'mtibalt91@istockphoto.com',N'731-616-8094',0,N'98110'),
	 ('1952-02-22',N'Alexandro',N'Deering',38,N'adeering92@example.com',N'711-819-3270',1,N'94109'),
	 ('1974-12-28',N'Lyndy',N'Goundsy',40,N'lgoundsy93@ed.gov',N'705-275-8810',0,N'93516'),
	 ('1974-02-26',N'Toni',N'Bate',20,N'tbate94@tumblr.com',N'557-166-7158',0,N'98811');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1971-11-29',N'Harlene',N'Blakeley',25,N'hblakeley95@mysql.com',N'678-768-1184',0,N'90220'),
	 ('1966-07-02',N'Gayel',N'Divall',33,N'gdivall96@patch.com',N'983-792-4338',0,N'92083'),
	 ('1977-07-21',N'Stevana',N'Heustace',43,N'sheustace97@senate.gov',N'199-567-5697',0,N'95111'),
	 ('1976-07-08',N'Darlleen',N'Friberg',40,N'dfriberg98@sohu.com',N'885-680-1553',0,N'90093'),
	 ('1986-07-19',N'Hetti',N'Dennison',13,N'hdennison99@mac.com',N'126-559-6584',1,N'91747'),
	 ('1991-03-06',N'Clayborn',N'Panyer',42,N'cpanyer9a@omniture.com',N'278-127-1744',0,N'98857'),
	 ('1992-05-11',N'Domini',N'Hanburry',31,N'dhanburry9b@friendfeed.com',N'316-905-6335',0,N'92846'),
	 ('1967-06-09',N'Deane',N'Megroff',47,N'dmegroff9c@twitter.com',N'897-301-9373',0,N'29304'),
	 ('1972-12-31',N'Kerry',N'Lydiard',11,N'klydiard9d@biglobe.ne.jp',N'459-468-6074',0,N'90661'),
	 ('1953-11-16',N'Ariadne',N'Gumly',25,N'agumly9e@free.fr',N'972-619-1995',1,N'95007');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1961-05-31',N'Sal',N'Trigg',36,N'strigg9f@ucoz.ru',N'110-218-9906',0,N'98325'),
	 ('1981-11-13',N'Betteanne',N'Forre',23,N'bforre9g@cnet.com',N'944-228-7358',0,N'96087'),
	 ('1985-05-13',N'Evangelia',N'Sherborne',11,N'esherborne9h@cyberchimps.com',N'369-362-9944',0,N'93504'),
	 ('1963-09-16',N'Rivi',N'Scantlebury',24,N'rscantlebury9i@telegraph.co.uk',N'741-423-2330',0,N'98288'),
	 ('1961-08-03',N'Angelica',N'Hesser',47,N'ahesser9j@de.vu',N'815-790-4718',0,N'98045'),
	 ('1980-09-11',N'Benny',N'Wickman',36,N'bwickman9k@virginia.edu',N'746-607-8773',0,N'29375'),
	 ('1978-04-22',N'Kailey',N'Schutter',31,N'kschutter9l@github.com',N'492-927-3512',0,N'98593'),
	 ('1969-10-04',N'Abigael',N'Mephan',12,N'amephan9m@cbsnews.com',N'276-127-0692',0,N'95959'),
	 ('1969-09-13',N'Chandra',N'Hellier',19,N'chellier9n@slashdot.org',N'752-517-1743',0,N'93920'),
	 ('1994-06-13',N'Jason',N'Froud',34,N'jfroud9o@cbslocal.com',N'319-315-1469',1,N'90015');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1959-05-15',N'Bjorn',N'Greber',42,N'bgreber9p@adobe.com',N'524-961-2536',0,N'98113'),
	 ('1975-05-25',N'Egon',N'Ocheltree',38,N'eocheltree9q@google.cn',N'604-278-4075',0,N'95312'),
	 ('1994-01-21',N'Aylmar',N'MacCallion',12,N'amaccallion9r@mozilla.org',N'919-702-5072',0,N'29929'),
	 ('2000-04-16',N'Averill',N'Beacroft',24,N'abeacroft9s@cpanel.net',N'642-707-9773',0,N'90305'),
	 ('1987-05-25',N'Melisenda',N'Este',25,N'meste9t@webeden.co.uk',N'318-574-6498',1,N'93745'),
	 ('1967-03-14',N'Melba',N'Wint',10,N'mwint9u@biblegateway.com',N'383-160-7498',0,N'91750'),
	 ('1954-05-01',N'Gabriell',N'Roumier',21,N'groumier9v@ebay.com',N'380-936-2136',0,N'29588'),
	 ('1993-05-12',N'Cornie',N'Wylder',48,N'cwylder9w@last.fm',N'495-753-0852',0,N'98430'),
	 ('1983-07-26',N'Lenee',N'Malsher',12,N'lmalsher9x@tripadvisor.com',N'582-795-7810',0,N'95120'),
	 ('1956-07-28',N'Tadio',N'Grisley',14,N'tgrisley9y@topsy.com',N'662-873-1816',0,N'94155');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1951-05-31',N'Benedicto',N'Starr',19,N'bstarr9z@dagondesign.com',N'193-927-2947',0,N'90101'),
	 ('1973-05-30',N'Sibella',N'Van der Spohr',31,N'svanderspohra0@disqus.com',N'114-888-6018',0,N'94237'),
	 ('1960-07-17',N'Kennith',N'Quinion',50,N'kquiniona1@webmd.com',N'495-762-8983',0,N'95315'),
	 ('1963-07-12',N'Dolores',N'Osmar',47,N'dosmara2@studiopress.com',N'609-199-6808',0,N'95301'),
	 ('1981-10-25',N'Susann',N'Kenny',27,N'skennya3@jugem.jp',N'267-371-8715',0,N'98901'),
	 ('1998-02-05',N'Van',N'Fullagar',46,N'vfullagara4@rediff.com',N'516-555-7762',0,N'98623'),
	 ('1977-10-17',N'Daron',N'Dyhouse',44,N'ddyhousea5@google.pl',N'913-347-5302',0,N'95431'),
	 ('1991-01-08',N'Rosalynd',N'Bampton',42,N'rbamptona6@aol.com',N'223-487-3653',0,N'29706'),
	 ('1967-02-05',N'Melody',N'Gowanson',31,N'mgowansona7@tinypic.com',N'631-873-5893',0,N'29214'),
	 ('1962-06-01',N'Felipa',N'Rivallant',19,N'frivallanta8@cpanel.net',N'595-976-1281',1,N'29102');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1981-07-26',N'Tarra',N'Cantopher',42,N'tcantophera9@ocn.ne.jp',N'445-669-7514',1,N'94250'),
	 ('1974-12-06',N'Dulsea',N'Barok',44,N'dbarokaa@newsvine.com',N'178-443-9299',0,N'92845'),
	 ('1976-06-20',N'Raffaello',N'Polland',46,N'rpollandab@illinois.edu',N'407-418-9729',1,N'99359'),
	 ('1988-10-11',N'Yasmeen',N'Behninck',40,N'ybehninckac@forbes.com',N'465-378-0797',0,N'94949'),
	 ('1957-11-09',N'Emmy',N'Clayson',22,N'eclaysonad@angelfire.com',N'521-460-0927',0,N'96154'),
	 ('1957-03-30',N'Rani',N'Simond',24,N'rsimondae@scientificamerican.com',N'522-240-2937',0,N'91899'),
	 ('1952-04-25',N'Dee',N'Brimson',27,N'dbrimsonaf@vimeo.com',N'478-326-5436',0,N'95124'),
	 ('1962-11-02',N'Rivalee',N'Willard',36,N'rwillardag@gravatar.com',N'698-949-4768',0,N'95012'),
	 ('1955-06-04',N'Brennan',N'Guidone',16,N'bguidoneah@yahoo.co.jp',N'582-550-9103',0,N'93614'),
	 ('1974-03-22',N'Putnam',N'Recke',14,N'preckeai@google.co.jp',N'181-627-2727',0,N'96134');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1999-08-22',N'Renault',N'Few',19,N'rfewaj@pinterest.com',N'104-885-6871',0,N'93235'),
	 ('1959-05-30',N'Toby',N'Van Dijk',15,N'tvandijkak@xinhuanet.com',N'862-928-7281',0,N'93592'),
	 ('1961-06-21',N'Rudd',N'Kubicki',10,N'rkubickial@cpanel.net',N'536-293-8433',0,N'92278'),
	 ('1979-10-25',N'Bentley',N'Clymo',26,N'bclymoam@scientificamerican.com',N'437-919-6328',1,N'93255'),
	 ('1969-03-21',N'Ethyl',N'Swatland',37,N'eswatlandan@thetimes.co.uk',N'335-693-2410',0,N'94963'),
	 ('1979-06-27',N'Rheba',N'Kennsley',25,N'rkennsleyao@msn.com',N'185-855-0653',0,N'94535'),
	 ('1988-01-15',N'Morrie',N'Nore',32,N'mnoreap@washington.edu',N'713-524-7975',1,N'92602'),
	 ('1989-03-17',N'Hope',N'Kuschek',16,N'hkuschekaq@mtv.com',N'559-608-8851',0,N'29581'),
	 ('1953-10-23',N'Zarla',N'Stranio',38,N'zstranioar@indiegogo.com',N'146-504-3633',0,N'90018'),
	 ('1967-12-26',N'Rania',N'Hards',40,N'rhardsas@economist.com',N'928-688-8364',1,N'91387');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1977-04-17',N'Dix',N'Hecks',22,N'dhecksat@cisco.com',N'160-700-6326',0,N'29609'),
	 ('1992-12-02',N'Janeen',N'Brecknell',30,N'jbrecknellau@wikipedia.org',N'714-919-0783',0,N'93707'),
	 ('1968-03-24',N'Kelby',N'Sobey',28,N'ksobeyav@dailymotion.com',N'313-546-5579',1,N'98932'),
	 ('1986-02-19',N'Jsandye',N'Gleadle',27,N'jgleadleaw@mail.ru',N'791-504-6534',0,N'92153'),
	 ('1979-02-22',N'Ali',N'Rosgen',13,N'arosgenax@geocities.jp',N'498-436-2127',1,N'92170'),
	 ('1985-07-14',N'Coralie',N'Burgis',35,N'cburgisay@woothemes.com',N'240-780-7952',1,N'98610'),
	 ('1995-05-07',N'Stillman',N'Hellens',16,N'shellensaz@flavors.me',N'730-143-5614',0,N'98829'),
	 ('1989-02-23',N'Kristine',N'Klehn',14,N'kklehnb0@csmonitor.com',N'699-918-0464',1,N'93255'),
	 ('1989-05-11',N'Nikaniki',N'Windeatt',12,N'nwindeattb1@mayoclinic.com',N'455-683-1098',1,N'29078'),
	 ('1968-04-19',N'Christoforo',N'Tsarovic',46,N'ctsarovicb2@amazon.com',N'858-697-8063',0,N'92268');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1996-05-21',N'Mayor',N'Holleworth',45,N'mholleworthb3@pcworld.com',N'260-260-4055',1,N'98326'),
	 ('1975-06-06',N'Hephzibah',N'Vicent',27,N'hvicentb4@joomla.org',N'532-700-1540',1,N'90402'),
	 ('1966-11-11',N'Flossi',N'Madle',20,N'fmadleb5@behance.net',N'313-990-7528',0,N'29504'),
	 ('1977-04-01',N'Audi',N'Prene',34,N'apreneb6@cnn.com',N'318-738-3418',0,N'94044'),
	 ('1982-01-20',N'Raymund',N'Yantsurev',14,N'ryantsurevb7@csmonitor.com',N'575-698-9639',0,N'90845'),
	 ('1992-02-02',N'North',N'Fawthrop',46,N'nfawthropb8@joomla.org',N'444-780-1219',0,N'90620'),
	 ('1966-02-08',N'Vance',N'Titterrell',28,N'vtitterrellb9@scientificamerican.com',N'383-318-7305',0,N'95410'),
	 ('1991-07-06',N'Barbee',N'Betje',50,N'bbetjeba@simplemachines.org',N'611-127-8117',0,N'95443'),
	 ('1952-11-24',N'Jerrilee',N'Jenckes',44,N'jjenckesbb@jigsy.com',N'705-559-9071',0,N'95534'),
	 ('1973-06-06',N'Aleta',N'Kropach',27,N'akropachbc@google.ru',N'719-287-8580',0,N'92651');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1965-04-03',N'Hastings',N'Simonsson',25,N'hsimonssonbd@apache.org',N'687-877-9778',0,N'95703'),
	 ('1981-07-29',N'Aurelea',N'Cowthard',21,N'acowthardbe@dot.gov',N'218-714-9193',1,N'90242'),
	 ('1974-03-10',N'Nydia',N'Bony',16,N'nbonybf@github.com',N'541-448-3979',1,N'95721'),
	 ('1970-03-12',N'Verna',N'Balcock',36,N'vbalcockbg@myspace.com',N'298-622-5093',0,N'90408'),
	 ('1953-10-04',N'Gretel',N'Morilla',17,N'gmorillabh@ehow.com',N'191-141-5217',0,N'94519'),
	 ('1989-05-11',N'Vallie',N'Skayman',40,N'vskaymanbi@mapy.cz',N'619-289-0787',0,N'93640'),
	 ('1960-10-20',N'Cornall',N'Dayes',41,N'cdayesbj@theglobeandmail.com',N'115-109-4085',1,N'98943'),
	 ('1966-11-17',N'Valeria',N'Lathwell',46,N'vlathwellbk@cnn.com',N'149-381-2854',0,N'29163'),
	 ('1983-03-07',N'Philis',N'Wyd',14,N'pwydbl@jigsy.com',N'820-553-6934',1,N'92414'),
	 ('1962-09-04',N'Cahra',N'Lembke',17,N'clembkebm@nps.gov',N'336-923-7887',0,N'90062');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1991-03-31',N'Arlen',N'Wheal',33,N'awhealbn@amazon.co.jp',N'870-788-9542',0,N'94975'),
	 ('1968-10-12',N'Ellis',N'Whelan',35,N'ewhelanbo@google.co.jp',N'711-628-4949',0,N'92782'),
	 ('1955-07-30',N'Beret',N'Corkell',17,N'bcorkellbp@forbes.com',N'276-794-9002',0,N'98024'),
	 ('1993-09-14',N'Erda',N'Blaxeland',39,N'eblaxelandbq@archive.org',N'609-594-1925',1,N'98648'),
	 ('1950-09-22',N'Myrtice',N'Golledge',50,N'mgolledgebr@sfgate.com',N'268-951-8054',0,N'94229'),
	 ('1983-03-17',N'Xerxes',N'Langrick',17,N'xlangrickbs@google.pl',N'123-734-5641',0,N'90075'),
	 ('1953-12-02',N'Wildon',N'Dell ''Orto',15,N'wdellortobt@purevolume.com',N'152-113-7968',0,N'96128'),
	 ('1972-05-01',N'Gav',N'Cherry Holme',18,N'gcherryholmebu@bloglovin.com',N'501-606-4515',0,N'95269'),
	 ('1995-03-07',N'Leonanie',N'Beran',20,N'lberanbv@bing.com',N'389-260-5907',0,N'29137'),
	 ('1963-08-06',N'Nikolai',N'Durber',34,N'ndurberbw@examiner.com',N'826-114-7137',0,N'95636');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1990-03-13',N'Den',N'Vasilchikov',49,N'dvasilchikovbx@sfgate.com',N'567-268-3047',0,N'98815'),
	 ('1987-05-24',N'Adella',N'Harrow',50,N'aharrowby@aol.com',N'320-361-7821',0,N'95330'),
	 ('1978-11-19',N'Erica',N'Di Biagio',19,N'edibiagiobz@sina.com.cn',N'646-943-9497',0,N'98617'),
	 ('1997-03-23',N'Brittan',N'Loche',32,N'blochec0@webs.com',N'661-528-4918',0,N'94561'),
	 ('1950-04-26',N'Hetti',N'Aspall',37,N'haspallc1@cdbaby.com',N'146-585-9172',0,N'99181'),
	 ('1977-07-11',N'Harriet',N'Jozwiak',22,N'hjozwiakc2@parallels.com',N'667-852-7046',0,N'90809'),
	 ('1984-12-13',N'Ethelbert',N'Weaving',19,N'eweavingc3@icio.us',N'522-941-5006',0,N'94156'),
	 ('1971-09-14',N'Isobel',N'Got',48,N'igotc4@un.org',N'184-528-2415',0,N'92398'),
	 ('1989-07-11',N'Sheeree',N'Antushev',11,N'santushevc5@mediafire.com',N'651-787-3939',0,N'91305'),
	 ('1962-02-28',N'Rebecca',N'Yurinov',23,N'ryurinovc6@smh.com.au',N'926-623-4343',0,N'96044');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1959-02-06',N'Storm',N'Hackly',46,N'shacklyc7@bbb.org',N'564-662-2991',0,N'95534'),
	 ('1985-06-28',N'Chilton',N'Enston',20,N'censtonc8@patch.com',N'356-162-1473',0,N'95191'),
	 ('1954-04-24',N'Johann',N'Bultitude',19,N'jbultitudec9@theglobeandmail.com',N'367-139-3578',0,N'95758'),
	 ('1959-03-02',N'Rog',N'Ivanyukov',28,N'rivanyukovca@edublogs.org',N'891-175-6901',0,N'92230'),
	 ('1956-10-24',N'Otis',N'Waitland',39,N'owaitlandcb@stumbleupon.com',N'490-691-2380',0,N'29840'),
	 ('1984-10-22',N'Anet',N'Baukham',11,N'abaukhamcc@princeton.edu',N'621-920-1872',1,N'95860'),
	 ('1976-12-29',N'Tammie',N'Pleaden',27,N'tpleadencd@bigcartel.com',N'750-588-0941',1,N'92605'),
	 ('1977-04-04',N'Archy',N'Anthes',24,N'aanthesce@mashable.com',N'576-119-7060',0,N'95929'),
	 ('1970-06-22',N'Reggis',N'MacAvddy',23,N'rmacavddycf@sitemeter.com',N'151-121-7898',0,N'92515'),
	 ('1979-11-20',N'Dal',N'McDell',18,N'dmcdellcg@jalbum.net',N'780-844-3292',0,N'29030');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1990-02-24',N'Mayor',N'Schonfelder',30,N'mschonfelderch@google.com.br',N'299-270-1837',0,N'92014'),
	 ('1972-10-21',N'Maynord',N'Chalfant',35,N'mchalfantci@disqus.com',N'447-907-7446',1,N'29175'),
	 ('1997-06-25',N'Emeline',N'Bedle',13,N'ebedlecj@cdc.gov',N'649-320-2834',0,N'29033'),
	 ('1960-08-17',N'Sherman',N'Paddingdon',17,N'spaddingdonck@symantec.com',N'895-349-7777',0,N'94044'),
	 ('1995-06-25',N'Tarrance',N'Raithmill',47,N'traithmillcl@apple.com',N'386-103-2797',0,N'91618'),
	 ('1997-01-27',N'Vina',N'Pharoah',24,N'vpharoahcm@about.me',N'354-581-0822',0,N'92223'),
	 ('1994-04-23',N'Dunc',N'Meddick',48,N'dmeddickcn@w3.org',N'457-487-6852',1,N'91102'),
	 ('1962-09-07',N'Bryana',N'Bascombe',25,N'bbascombeco@vistaprint.com',N'576-448-0042',0,N'92833'),
	 ('1964-12-08',N'Sergio',N'Croston',49,N'scrostoncp@ucla.edu',N'629-391-5455',0,N'98826'),
	 ('1971-11-13',N'Alica',N'Ebbetts',27,N'aebbettscq@angelfire.com',N'390-181-1976',0,N'94937');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1993-06-16',N'Janos',N'Barker',22,N'jbarkercr@walmart.com',N'170-890-7170',0,N'98415'),
	 ('1951-08-04',N'Hortensia',N'Rosas',45,N'hrosascs@businessweek.com',N'742-811-3446',0,N'94568'),
	 ('1956-09-10',N'Conn',N'Rosenfield',35,N'crosenfieldct@sogou.com',N'700-868-6238',1,N'94515'),
	 ('1987-03-19',N'Nicolis',N'Everett',24,N'neverettcu@answers.com',N'905-862-2513',0,N'93560'),
	 ('1973-01-27',N'Crystie',N'Rodson',25,N'crodsoncv@gizmodo.com',N'953-264-2314',0,N'91222'),
	 ('1993-08-04',N'Denis',N'Wadlow',30,N'dwadlowcw@jugem.jp',N'647-769-4592',0,N'29481'),
	 ('1955-02-21',N'Madeline',N'Di Domenico',49,N'mdidomenicocx@telegraph.co.uk',N'133-472-5887',0,N'94954'),
	 ('1998-07-11',N'Etty',N'Thon',36,N'ethoncy@sun.com',N'296-427-4909',1,N'96088'),
	 ('1988-05-11',N'Valerye',N'Wishart',23,N'vwishartcz@1und1.de',N'805-189-0821',0,N'94538'),
	 ('1961-10-23',N'Cordie',N'Stoke',44,N'cstoked0@usnews.com',N'199-928-2413',0,N'90505');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1991-09-09',N'Alon',N'Archdeckne',20,N'aarchdeckned1@google.nl',N'849-414-5106',0,N'90035'),
	 ('1998-03-04',N'Janice',N'Leyre',46,N'jleyred2@dailymail.co.uk',N'306-378-9961',0,N'95624'),
	 ('1996-11-25',N'Harper',N'Wyeth',36,N'hwyethd3@hhs.gov',N'821-749-5071',1,N'92093'),
	 ('1973-07-03',N'Saraann',N'Corrington',26,N'scorringtond4@whitehouse.gov',N'463-727-3485',1,N'95472'),
	 ('1990-05-27',N'Jamaal',N'Lindenblatt',47,N'jlindenblattd5@usgs.gov',N'140-546-3032',0,N'90307'),
	 ('1965-10-25',N'Cloe',N'Capel',19,N'ccapeld6@squarespace.com',N'127-670-3311',0,N'91980'),
	 ('1958-04-26',N'Jo ann',N'Bolino',28,N'jbolinod7@cbslocal.com',N'356-632-0092',0,N'29928'),
	 ('1999-03-20',N'Gardner',N'Dolley',22,N'gdolleyd8@wikispaces.com',N'708-617-6945',0,N'92317'),
	 ('1951-09-10',N'Mindy',N'Oldmeadow',43,N'moldmeadowd9@example.com',N'812-645-6236',0,N'29127'),
	 ('1972-01-29',N'Vidovik',N'Lansdown',30,N'vlansdownda@behance.net',N'571-327-4028',0,N'98158');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1979-08-06',N'Meryl',N'Inkle',25,N'minkledb@nymag.com',N'145-535-9719',1,N'98418'),
	 ('1958-01-07',N'Dino',N'Nanson',36,N'dnansondc@freewebs.com',N'837-823-2690',0,N'93434'),
	 ('1985-07-04',N'Sharl',N'Le Lievre',26,N'slelievredd@sbwire.com',N'306-821-1184',0,N'95646'),
	 ('1990-08-05',N'Clotilda',N'Kincey',35,N'ckinceyde@sakura.ne.jp',N'945-884-5182',0,N'98178'),
	 ('1978-03-28',N'Luce',N'Finlay',31,N'lfinlaydf@wikipedia.org',N'794-497-9787',1,N'93718'),
	 ('1975-11-02',N'Camile',N'Stonard',31,N'cstonarddg@hao123.com',N'745-498-6375',0,N'29065'),
	 ('1983-10-10',N'Diane',N'Thumann',17,N'dthumanndh@ucoz.ru',N'545-426-5744',1,N'98009'),
	 ('1957-01-29',N'Florencia',N'Sneyd',20,N'fsneyddi@discovery.com',N'345-869-6505',0,N'95962'),
	 ('2000-03-16',N'Amerigo',N'Deex',12,N'adeexdj@berkeley.edu',N'322-984-3753',0,N'92074'),
	 ('1965-03-06',N'Conway',N'Poytheras',15,N'cpoytherasdk@rambler.ru',N'487-788-4887',0,N'92324');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1977-11-08',N'Towney',N'Tomasz',28,N'ttomaszdl@paginegialle.it',N'195-959-3400',0,N'98151'),
	 ('1990-07-07',N'Brice',N'Espinola',33,N'bespinoladm@soup.io',N'558-611-4711',0,N'98184'),
	 ('1980-05-10',N'Germaine',N'Milbourn',45,N'gmilbourndn@google.ru',N'198-900-3516',0,N'96052'),
	 ('1991-03-13',N'Talbot',N'Casaro',23,N'tcasarodo@canalblog.com',N'485-195-8845',0,N'90045'),
	 ('1994-05-05',N'Barby',N'Sicha',33,N'bsichadp@house.gov',N'532-960-6522',0,N'98343'),
	 ('1992-06-11',N'Kristos',N'Feehely',40,N'kfeehelydq@mashable.com',N'161-699-1750',1,N'95843'),
	 ('1983-04-27',N'Cirillo',N'Croston',10,N'ccrostondr@baidu.com',N'744-489-7724',0,N'94710'),
	 ('1993-06-18',N'Chariot',N'Vasnev',10,N'cvasnevds@studiopress.com',N'327-439-4714',0,N'93118'),
	 ('1991-10-02',N'Helenelizabeth',N'Aldwinckle',42,N'haldwinckledt@google.com.br',N'298-586-4761',0,N'94534'),
	 ('1969-04-06',N'Kimbell',N'Hurring',33,N'khurringdu@businessinsider.com',N'479-481-7016',1,N'92845');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1976-03-07',N'Erhard',N'Callar',33,N'ecallardv@istockphoto.com',N'622-199-2923',0,N'93955'),
	 ('1964-03-11',N'Gwendolyn',N'Snedden',42,N'gsneddendw@vinaora.com',N'468-997-0448',1,N'98397'),
	 ('1964-10-18',N'Ingamar',N'Greeve',26,N'igreevedx@simplemachines.org',N'527-318-9752',0,N'95436'),
	 ('1964-11-30',N'Jorey',N'Cawdell',42,N'jcawdelldy@typepad.com',N'331-244-9881',0,N'93539'),
	 ('1969-12-30',N'Ingaberg',N'Alten',19,N'ialtendz@alexa.com',N'687-341-1850',1,N'99103'),
	 ('1961-02-14',N'Ambros',N'Anster',22,N'aanstere0@narod.ru',N'802-611-4693',0,N'93703'),
	 ('1987-09-23',N'Jaymie',N'Lygoe',40,N'jlygoee1@ning.com',N'698-537-9259',0,N'93544'),
	 ('1965-07-17',N'Aldo',N'Hadwick',36,N'ahadwicke2@gizmodo.com',N'673-919-1249',1,N'92832'),
	 ('1952-10-01',N'Janey',N'Collett',14,N'jcollette3@tripadvisor.com',N'214-733-6180',0,N'29452'),
	 ('1989-12-13',N'Vachel',N'Olivella',27,N'volivellae4@mashable.com',N'435-155-6243',0,N'29654');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1968-11-09',N'Tommy',N'Fredi',19,N'tfredie5@meetup.com',N'717-679-6115',0,N'91333'),
	 ('1967-03-13',N'Meredithe',N'Shinn',47,N'mshinne6@virginia.edu',N'872-963-9354',0,N'29708'),
	 ('1986-06-29',N'Loralie',N'Smithson',17,N'lsmithsone7@tmall.com',N'857-157-6585',0,N'95221'),
	 ('1982-03-01',N'Sheila-kathryn',N'Philpotts',27,N'sphilpottse8@csmonitor.com',N'738-300-0927',1,N'94509'),
	 ('1958-01-15',N'Cris',N'Zavattieri',45,N'czavattierie9@indiatimes.com',N'357-526-3948',0,N'29142'),
	 ('1967-04-06',N'Mattie',N'Capewell',28,N'mcapewellea@ibm.com',N'988-541-7629',0,N'95585'),
	 ('1993-08-16',N'Ragnar',N'Corroyer',24,N'rcorroyereb@blinklist.com',N'313-843-2523',0,N'29506'),
	 ('1980-07-06',N'Gaelan',N'Earley',33,N'gearleyec@lulu.com',N'617-463-6463',0,N'94120'),
	 ('1967-10-06',N'Wash',N'Kleinstein',37,N'wkleinsteined@google.fr',N'375-455-2578',1,N'94014'),
	 ('1985-02-16',N'Ricca',N'Roger',31,N'rrogeree@cisco.com',N'766-844-1623',0,N'95632');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1971-08-17',N'Llewellyn',N'Cartmer',28,N'lcartmeref@nbcnews.com',N'191-463-3219',0,N'93701'),
	 ('1964-10-12',N'Tiebold',N'Pigram',10,N'tpigrameg@mediafire.com',N'586-349-7213',1,N'98075'),
	 ('1972-11-18',N'Jazmin',N'Hoggetts',12,N'jhoggettseh@wikimedia.org',N'215-598-9225',0,N'92345'),
	 ('1984-08-09',N'Bee',N'Widdicombe',14,N'bwiddicombeei@about.me',N'309-356-7701',1,N'92253'),
	 ('1962-04-08',N'Hanan',N'Bridgman',26,N'hbridgmanej@github.com',N'640-575-6026',0,N'90010'),
	 ('1979-06-28',N'Elia',N'Le Barr',36,N'elebarrek@people.com.cn',N'526-780-6307',0,N'96086'),
	 ('1950-08-21',N'Lil',N'Bickford',41,N'lbickfordel@sohu.com',N'329-237-8191',0,N'92587'),
	 ('2000-03-16',N'Karmen',N'Halladay',23,N'khalladayem@japanpost.jp',N'444-649-0848',0,N'90804'),
	 ('1963-10-28',N'Claiborn',N'Maccaddie',18,N'cmaccaddieen@unc.edu',N'808-781-6297',0,N'93002'),
	 ('1996-05-11',N'Renault',N'Mullin',33,N'rmullineo@cdc.gov',N'767-207-5607',0,N'29569');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1963-10-27',N'Randi',N'MacSkeaghan',20,N'rmacskeaghanep@home.pl',N'953-592-3524',0,N'95194'),
	 ('1997-10-30',N'Susann',N'Gringley',45,N'sgringleyeq@a8.net',N'577-691-4061',1,N'95327'),
	 ('1988-01-18',N'Sauveur',N'Ginman',35,N'sginmaner@opera.com',N'595-122-8511',0,N'91780'),
	 ('1971-04-07',N'Ezra',N'Boness',31,N'ebonesses@shareasale.com',N'661-222-0273',0,N'29488'),
	 ('1996-12-07',N'Agnes',N'Frift',41,N'afriftet@hibu.com',N'911-751-8659',0,N'90744'),
	 ('1987-09-15',N'Win',N'Reyna',22,N'wreynaeu@squarespace.com',N'782-722-4841',0,N'91412'),
	 ('1991-12-06',N'Rayshell',N'Dreakin',37,N'rdreakinev@ning.com',N'230-756-9461',0,N'91609'),
	 ('1975-01-13',N'Brigida',N'Ding',18,N'bdingew@pen.io',N'871-614-8408',0,N'92551'),
	 ('1987-07-12',N'Zorana',N'Taudevin',45,N'ztaudevinex@gizmodo.com',N'919-960-5831',0,N'95404'),
	 ('1977-09-01',N'Britney',N'Carff',32,N'bcarffey@sphinn.com',N'537-944-0867',0,N'96133');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1960-07-10',N'Elwood',N'Pady',50,N'epadyez@sfgate.com',N'830-317-0856',1,N'95192'),
	 ('1962-06-07',N'Angie',N'Rayworth',39,N'arayworthf0@nbcnews.com',N'224-823-1628',1,N'95064'),
	 ('1961-09-24',N'Stern',N'Penton',17,N'spentonf1@oaic.gov.au',N'856-684-4947',0,N'95213'),
	 ('1982-06-17',N'Chick',N'Shepperd',26,N'cshepperdf2@earthlink.net',N'627-738-0287',0,N'90301'),
	 ('1970-08-16',N'Perry',N'Hanley',15,N'phanleyf3@creativecommons.org',N'191-423-0044',0,N'98117'),
	 ('1978-02-27',N'Ainslee',N'Baseke',41,N'abasekef4@de.vu',N'538-494-1526',1,N'99206'),
	 ('1984-02-18',N'Zebulen',N'Cashin',28,N'zcashinf5@prnewswire.com',N'915-400-3081',1,N'98164'),
	 ('1955-10-16',N'Marquita',N'Callen',40,N'mcallenf6@springer.com',N'997-866-0479',0,N'29015'),
	 ('1969-12-24',N'Nonah',N'Simioni',21,N'nsimionif7@nps.gov',N'214-719-7657',1,N'93225'),
	 ('1996-09-19',N'Deane',N'Mattaser',24,N'dmattaserf8@home.pl',N'318-333-8913',1,N'94274');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1959-03-07',N'Marco',N'Wrefford',46,N'mwreffordf9@ihg.com',N'527-550-4976',0,N'95209'),
	 ('1952-03-02',N'Noemi',N'Fonte',47,N'nfontefa@cpanel.net',N'537-812-0522',0,N'91340'),
	 ('1967-01-26',N'Rochell',N'Goodbarr',18,N'rgoodbarrfb@ovh.net',N'533-163-4074',0,N'94240'),
	 ('1982-09-26',N'Hernando',N'Andrei',37,N'handreifc@sbwire.com',N'950-460-5357',0,N'91024'),
	 ('1952-07-30',N'Gary',N'Navarre',24,N'gnavarrefd@com.com',N'822-792-7405',0,N'94018'),
	 ('1984-01-27',N'Alonzo',N'Livoir',41,N'alivoirfe@unc.edu',N'998-511-6368',1,N'29860'),
	 ('1968-09-01',N'Felipa',N'Castagno',22,N'fcastagnoff@bizjournals.com',N'362-516-2742',0,N'98235'),
	 ('1984-02-11',N'Midge',N'Mapowder',47,N'mmapowderfg@google.it',N'239-371-6055',0,N'29831'),
	 ('1969-01-28',N'Hunt',N'Sallis',22,N'hsallisfh@smh.com.au',N'820-217-1389',0,N'29922'),
	 ('1950-10-26',N'Andee',N'Filinkov',22,N'afilinkovfi@mayoclinic.com',N'828-955-1184',0,N'29376');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1963-05-14',N'Adelind',N'Woolston',14,N'awoolstonfj@clickbank.net',N'108-476-5661',0,N'93022'),
	 ('1970-07-14',N'Rozele',N'Cornall',27,N'rcornallfk@jugem.jp',N'345-704-5684',0,N'92325'),
	 ('1971-03-03',N'Noe',N'Wooton',21,N'nwootonfl@cnn.com',N'414-163-4789',0,N'95978'),
	 ('1960-01-08',N'Franchot',N'Carlin',41,N'fcarlinfm@xing.com',N'317-593-0794',0,N'95128'),
	 ('1972-12-05',N'Livy',N'Shark',48,N'lsharkfn@uol.com.br',N'448-946-4749',0,N'92612'),
	 ('1953-03-21',N'Shir',N'Chesney',27,N'schesneyfo@bandcamp.com',N'285-460-3805',0,N'91357'),
	 ('1973-01-22',N'Laryssa',N'Parrish',38,N'lparrishfp@nbcnews.com',N'743-770-9726',0,N'90074'),
	 ('1973-12-19',N'Marco',N'Wincer',24,N'mwincerfq@wordpress.org',N'310-928-5950',0,N'29610'),
	 ('1958-09-14',N'Eadith',N'Whacket',32,N'ewhacketfr@npr.org',N'377-851-5740',0,N'29112'),
	 ('1965-01-05',N'Tybie',N'Beddingham',43,N'tbeddinghamfs@bbc.co.uk',N'466-234-8731',0,N'29471');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1975-01-18',N'Melina',N'Greetland',49,N'mgreetlandft@webmd.com',N'717-234-1588',0,N'93306'),
	 ('1989-09-28',N'Iris',N'Medling',48,N'imedlingfu@mozilla.com',N'415-237-3277',0,N'95148'),
	 ('1956-08-03',N'Anette',N'Gilpillan',39,N'agilpillanfv@squidoo.com',N'264-788-3858',0,N'94802'),
	 ('1964-09-04',N'Clive',N'Shambroke',45,N'cshambrokefw@harvard.edu',N'937-624-6693',0,N'95429'),
	 ('1950-11-12',N'Efrem',N'Skinner',37,N'eskinnerfx@vkontakte.ru',N'666-602-2327',1,N'95558'),
	 ('1955-06-01',N'Verne',N'Koschek',33,N'vkoschekfy@usgs.gov',N'882-571-2610',0,N'29058'),
	 ('1997-06-07',N'Mischa',N'Foker',32,N'mfokerfz@sourceforge.net',N'246-873-2148',0,N'29547'),
	 ('1986-06-15',N'Beatrisa',N'Joel',15,N'bjoelg0@photobucket.com',N'936-618-8430',0,N'90397'),
	 ('1955-07-13',N'Ariana',N'Brader',11,N'abraderg1@ed.gov',N'887-194-7954',0,N'90267'),
	 ('1961-11-04',N'Gabrila',N'Flemming',15,N'gflemmingg2@blogs.com',N'422-541-1116',1,N'98006');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1999-08-27',N'Ricky',N'Duffell',41,N'rduffellg3@xinhuanet.com',N'761-117-4180',0,N'95233'),
	 ('1972-09-17',N'Maisie',N'Ducarne',24,N'mducarneg4@cnet.com',N'708-536-3153',0,N'94011'),
	 ('1973-01-13',N'Trevor',N'Colebeck',26,N'tcolebeckg5@sciencedirect.com',N'916-929-2466',0,N'98813'),
	 ('1988-12-11',N'Kelcy',N'Slixby',26,N'kslixbyg6@merriam-webster.com',N'943-231-7672',0,N'93930'),
	 ('1982-04-30',N'Rogers',N'Extal',13,N'rextalg7@google.com.au',N'631-663-8967',0,N'95945'),
	 ('1978-03-07',N'Edwin',N'Wallbank',36,N'ewallbankg8@answers.com',N'627-358-8545',1,N'98010'),
	 ('1988-09-20',N'Stefano',N'Oglethorpe',11,N'soglethorpeg9@google.es',N'776-794-6127',0,N'93440'),
	 ('1977-06-09',N'Chico',N'Blackater',24,N'cblackaterga@delicious.com',N'122-648-4640',1,N'93610'),
	 ('1967-08-10',N'Sibby',N'Traylen',20,N'straylengb@dailymotion.com',N'864-422-6426',0,N'29218'),
	 ('1996-10-01',N'Sargent',N'Maudling',33,N'smaudlinggc@rakuten.co.jp',N'995-132-5981',0,N'29439');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1970-07-16',N'Den',N'Yegorkov',38,N'dyegorkovgd@goodreads.com',N'783-892-3579',0,N'95759'),
	 ('1970-04-16',N'Gilligan',N'Parlor',43,N'gparlorge@vk.com',N'370-342-4208',0,N'94931'),
	 ('1970-04-02',N'Cirstoforo',N'Thornborrow',29,N'cthornborrowgf@yellowpages.com',N'391-547-1260',0,N'92584'),
	 ('1972-05-15',N'Andriette',N'Jollands',24,N'ajollandsgg@lycos.com',N'839-429-2143',1,N'99131'),
	 ('1993-05-18',N'Dalt',N'Thaim',11,N'dthaimgh@redcross.org',N'763-268-2005',0,N'90659'),
	 ('1953-01-11',N'Gerri',N'Routh',44,N'grouthgi@thetimes.co.uk',N'351-356-1012',0,N'98006'),
	 ('2000-01-16',N'Laurie',N'Verner',23,N'lvernergj@globo.com',N'417-990-3305',0,N'29334'),
	 ('1982-04-11',N'Johanna',N'Sommerland',44,N'jsommerlandgk@si.edu',N'952-148-3718',0,N'95536'),
	 ('1962-10-30',N'Knox',N'Smalley',11,N'ksmalleygl@diigo.com',N'698-155-8585',0,N'92033'),
	 ('1992-12-20',N'Val',N'Anstis',43,N'vanstisgm@independent.co.uk',N'476-209-8253',0,N'96052');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1989-12-28',N'Zitella',N'Riediger',30,N'zriedigergn@samsung.com',N'167-402-7131',1,N'29448'),
	 ('1962-11-25',N'Damon',N'Bielefeld',22,N'dbielefeldgo@trellian.com',N'871-912-5888',0,N'98582'),
	 ('1967-11-02',N'Samson',N'Courtonne',22,N'scourtonnegp@census.gov',N'741-533-7551',0,N'29628'),
	 ('1993-03-10',N'Corrie',N'Lidbetter',46,N'clidbettergq@tuttocitta.it',N'833-112-6026',1,N'96095'),
	 ('1955-04-28',N'Jamie',N'Erbe',25,N'jerbegr@youtu.be',N'418-986-9144',1,N'29082'),
	 ('1952-02-23',N'Ana',N'Biddwell',15,N'abiddwellgs@vistaprint.com',N'285-755-8424',0,N'93550'),
	 ('1997-02-15',N'Garey',N'Berney',48,N'gberneygt@ebay.com',N'261-775-7540',0,N'92055'),
	 ('1986-02-13',N'Vanna',N'Derrington',27,N'vderringtongu@delicious.com',N'648-245-6016',1,N'29744'),
	 ('1975-02-23',N'Gabriellia',N'Comizzoli',25,N'gcomizzoligv@drupal.org',N'134-628-2272',1,N'92522'),
	 ('1988-09-14',N'Cross',N'Fuzzard',48,N'cfuzzardgw@friendfeed.com',N'346-226-5449',0,N'29180');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1961-08-18',N'Paige',N'O''Docherty',32,N'podochertygx@t-online.de',N'337-373-2792',0,N'94115'),
	 ('1981-08-17',N'Vicki',N'Vokins',40,N'vvokinsgy@jimdo.com',N'481-394-1402',0,N'29105'),
	 ('1991-05-06',N'Hazel',N'Suttling',30,N'hsuttlinggz@google.pl',N'485-524-3868',0,N'94524'),
	 ('1982-06-25',N'Aindrea',N'Lymer',19,N'alymerh0@sciencedirect.com',N'886-705-3825',0,N'99150'),
	 ('1986-01-26',N'Margaret',N'Oswald',31,N'moswaldh1@geocities.jp',N'608-123-4018',0,N'29402'),
	 ('1954-11-03',N'Kermy',N'Pimm',40,N'kpimmh2@homestead.com',N'350-767-4498',0,N'93716'),
	 ('1980-10-05',N'Reeba',N'Ruck',29,N'rruckh3@sphinn.com',N'168-684-2787',0,N'29924'),
	 ('1965-01-24',N'Mabelle',N'Burne',31,N'mburneh4@seesaa.net',N'371-380-4402',0,N'96070'),
	 ('1987-09-23',N'Remington',N'Blackeby',37,N'rblackebyh5@4shared.com',N'342-544-7429',0,N'93660'),
	 ('1956-01-29',N'Daphene',N'Storer',20,N'dstorerh6@bandcamp.com',N'324-928-3637',0,N'98635');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1957-03-05',N'Cheri',N'Cromer',23,N'ccromerh7@guardian.co.uk',N'315-418-4059',0,N'29743'),
	 ('1996-09-14',N'Alvinia',N'Denkel',10,N'adenkelh8@bloomberg.com',N'424-232-1485',0,N'29570'),
	 ('1993-05-05',N'Wyatt',N'McIlvaney',34,N'wmcilvaneyh9@china.com.cn',N'471-370-0159',0,N'92781'),
	 ('1990-05-18',N'Monro',N'Sambiedge',26,N'msambiedgeha@webmd.com',N'677-481-7462',0,N'91768'),
	 ('1972-09-06',N'Trumaine',N'Robertet',38,N'trobertethb@newsvine.com',N'283-450-3827',0,N'95007'),
	 ('1973-06-15',N'Teri',N'Ellison',21,N'tellisonhc@flavors.me',N'538-805-9672',0,N'90715'),
	 ('1994-02-28',N'Elden',N'McLarnon',31,N'emclarnonhd@tinyurl.com',N'888-898-4196',1,N'90212'),
	 ('1956-02-04',N'Worth',N'McKeachie',13,N'wmckeachiehe@usda.gov',N'623-219-8078',0,N'92332'),
	 ('1953-05-16',N'Mabelle',N'Papaminas',14,N'mpapaminashf@nymag.com',N'591-947-5475',0,N'94517'),
	 ('1967-10-14',N'Cyndia',N'Rosenhaus',19,N'crosenhaushg@nydailynews.com',N'818-908-2205',0,N'90623');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1984-07-13',N'Stefan',N'Pfeffel',44,N'spfeffelhh@ted.com',N'112-549-2949',1,N'29172'),
	 ('1966-10-09',N'Ransell',N'Cumberpatch',38,N'rcumberpatchhi@instagram.com',N'716-540-9828',1,N'98288'),
	 ('1991-07-09',N'Lucille',N'Beebee',26,N'lbeebeehj@youtube.com',N'387-618-9214',1,N'90309'),
	 ('1974-08-23',N'Rhetta',N'Benson',48,N'rbensonhk@bigcartel.com',N'632-699-2978',0,N'93714'),
	 ('1969-02-23',N'Anthea',N'Perico',39,N'apericohl@boston.com',N'554-433-5177',0,N'29320'),
	 ('1987-02-10',N'Cointon',N'Edes',12,N'cedeshm@sciencedirect.com',N'524-537-8044',0,N'29018'),
	 ('1966-09-14',N'Gretel',N'Rapson',39,N'grapsonhn@shareasale.com',N'733-932-4611',0,N'94106'),
	 ('1967-05-04',N'Kynthia',N'Bambrugh',32,N'kbambrughho@cdbaby.com',N'885-297-8893',0,N'92140'),
	 ('1969-05-20',N'Thomasa',N'Worrell',29,N'tworrellhp@meetup.com',N'265-996-2290',0,N'91325'),
	 ('1973-02-18',N'Uriah',N'Bagenal',30,N'ubagenalhq@squidoo.com',N'257-684-7520',0,N'29612');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1963-09-27',N'Maisie',N'Hicklingbottom',24,N'mhicklingbottomhr@topsy.com',N'833-725-1310',0,N'99357'),
	 ('1969-08-06',N'Devora',N'Pettisall',44,N'dpettisallhs@csmonitor.com',N'747-905-9028',0,N'92712'),
	 ('1991-02-26',N'Brion',N'Jouanet',40,N'bjouanetht@cdc.gov',N'435-130-6152',1,N'95370'),
	 ('1967-12-17',N'Vale',N'Kovacs',48,N'vkovacshu@oracle.com',N'738-800-8533',0,N'29743'),
	 ('1966-11-13',N'Winifield',N'Cage',13,N'wcagehv@dyndns.org',N'485-136-3778',0,N'99326'),
	 ('1999-08-11',N'Milka',N'Brown',18,N'mbrownhw@addthis.com',N'155-406-0842',0,N'93065'),
	 ('1959-04-07',N'Cacilia',N'Dressell',31,N'cdressellhx@tripadvisor.com',N'996-556-5125',0,N'99022'),
	 ('1958-10-02',N'Pamela',N'Hazeldene',48,N'phazeldenehy@deliciousdays.com',N'273-944-6474',0,N'98312'),
	 ('1956-02-14',N'Malissa',N'Bracey',24,N'mbraceyhz@va.gov',N'732-455-6152',0,N'98036'),
	 ('1994-04-13',N'Nikkie',N'Firbanks',49,N'nfirbanksi0@vinaora.com',N'773-368-9600',0,N'98155');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1954-03-02',N'Kesley',N'Badsworth',13,N'kbadsworthi1@wix.com',N'448-728-2252',1,N'92169'),
	 ('1982-05-03',N'Felix',N'MacGuigan',17,N'fmacguigani2@mapquest.com',N'650-674-8123',0,N'93624'),
	 ('1993-10-10',N'Braden',N'Bison',38,N'bbisoni3@ted.com',N'149-446-8320',1,N'98114'),
	 ('1975-02-06',N'Cesar',N'Kinneir',26,N'ckinneiri4@mozilla.org',N'594-678-4925',0,N'93515'),
	 ('1956-03-24',N'Sharleen',N'Ben',24,N'sbeni5@vistaprint.com',N'454-803-7567',0,N'29479'),
	 ('1993-07-19',N'Michal',N'Duck',29,N'mducki6@bloglines.com',N'411-525-8368',0,N'92860'),
	 ('1977-04-29',N'Guillaume',N'Odgaard',42,N'godgaardi7@twitpic.com',N'577-849-7819',0,N'95456'),
	 ('1982-02-05',N'Tedie',N'Deniset',41,N'tdeniseti8@yellowbook.com',N'832-894-6573',0,N'93555'),
	 ('1991-07-15',N'Anne',N'Burniston',22,N'aburnistoni9@ning.com',N'533-758-2876',0,N'91755'),
	 ('1989-08-14',N'Joscelin',N'Tomlin',32,N'jtomlinia@cargocollective.com',N'569-723-6440',1,N'92678');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1951-09-16',N'Noella',N'Jumeau',32,N'njumeauib@ucoz.com',N'480-566-3286',0,N'96099'),
	 ('1984-03-15',N'Colin',N'Bloan',47,N'cbloanic@hp.com',N'908-467-0319',1,N'98387'),
	 ('2000-04-09',N'Evyn',N'MacKaig',18,N'emackaigid@wunderground.com',N'338-460-6386',0,N'92239'),
	 ('1983-03-15',N'Gratiana',N'Belfrage',49,N'gbelfrageie@about.com',N'485-407-9118',1,N'90639'),
	 ('1987-08-14',N'Stillman',N'Korb',31,N'skorbif@twitter.com',N'671-356-5534',0,N'92801'),
	 ('1968-08-06',N'Buffy',N'Eastham',31,N'beasthamig@is.gd',N'192-434-1695',1,N'98337'),
	 ('1980-05-14',N'Belia',N'Cockney',30,N'bcockneyih@altervista.org',N'187-820-1741',0,N'99201'),
	 ('1989-07-16',N'Emmie',N'Saulter',28,N'esaulterii@yelp.com',N'199-167-6324',0,N'92341'),
	 ('1950-09-30',N'Karine',N'Zeal',49,N'kzealij@usatoday.com',N'178-176-5600',0,N'93032'),
	 ('1987-10-25',N'Cy',N'Angus',23,N'cangusik@sina.com.cn',N'899-216-4574',0,N'93646');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1956-08-23',N'Lola',N'Hirth',30,N'lhirthil@hao123.com',N'713-933-1669',0,N'94137'),
	 ('1983-09-20',N'Chastity',N'Gill',36,N'cgillim@tiny.cc',N'639-339-3132',0,N'95134'),
	 ('1957-09-23',N'Rourke',N'Willarton',39,N'rwillartonin@rediff.com',N'188-556-1809',0,N'95310'),
	 ('1966-08-17',N'Mufi',N'Kenwyn',25,N'mkenwynio@google.co.jp',N'907-646-5618',0,N'29114'),
	 ('1986-03-17',N'Dulcia',N'Berkley',17,N'dberkleyip@washington.edu',N'813-201-7132',0,N'95316'),
	 ('1983-02-09',N'Sula',N'Kneel',20,N'skneeliq@behance.net',N'412-285-8536',0,N'98270'),
	 ('1975-01-22',N'Felice',N'Drewitt',40,N'fdrewittir@bloglovin.com',N'990-220-7548',1,N'95935'),
	 ('1961-06-12',N'Boote',N'Witt',41,N'bwittis@hud.gov',N'379-286-2637',0,N'94521'),
	 ('1950-08-08',N'Willabella',N'Farens',21,N'wfarensit@cbsnews.com',N'260-190-5549',0,N'98151'),
	 ('1991-05-17',N'Vevay',N'Scampion',31,N'vscampioniu@gravatar.com',N'825-198-9906',0,N'29677');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1956-09-12',N'Mellicent',N'Capineer',32,N'mcapineeriv@ocn.ne.jp',N'113-152-3087',0,N'91507'),
	 ('1952-05-15',N'Kissie',N'Teresse',44,N'kteresseiw@topsy.com',N'779-930-8251',0,N'95741'),
	 ('1998-07-10',N'Rosanna',N'Gravey',11,N'rgraveyix@europa.eu',N'817-555-3057',0,N'95917'),
	 ('1993-11-10',N'Anne',N'Haggerston',50,N'ahaggerstoniy@histats.com',N'216-401-1623',0,N'98584'),
	 ('1983-07-31',N'Gustav',N'Freschini',38,N'gfreschiniiz@mit.edu',N'871-647-6247',1,N'95681'),
	 ('1991-03-01',N'Salli',N'Bilbie',11,N'sbilbiej0@google.pl',N'769-264-3540',0,N'98126'),
	 ('1975-01-22',N'Catarina',N'Egarr',41,N'cegarrj1@wunderground.com',N'681-849-6847',1,N'91364'),
	 ('1962-06-16',N'Elka',N'Gotling',49,N'egotlingj2@wikia.com',N'552-722-5178',0,N'98068'),
	 ('1986-07-16',N'Anabel',N'Spikeings',46,N'aspikeingsj3@harvard.edu',N'391-377-6144',0,N'94706'),
	 ('1991-08-25',N'Rolf',N'Fennelly',35,N'rfennellyj4@skype.com',N'784-116-3285',0,N'29078');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1955-01-30',N'Maurene',N'Emmines',47,N'memminesj5@dagondesign.com',N'299-753-5810',0,N'98576'),
	 ('1986-11-02',N'Wat',N'Brittle',37,N'wbrittlej6@go.com',N'432-194-4636',0,N'96101'),
	 ('1965-11-30',N'Frances',N'Boles',32,N'fbolesj7@engadget.com',N'571-511-9811',0,N'92162'),
	 ('1951-09-25',N'Layton',N'Eastham',24,N'leasthamj8@zdnet.com',N'584-399-1079',1,N'95488'),
	 ('1953-07-19',N'Missy',N'Gowdridge',19,N'mgowdridgej9@dropbox.com',N'357-734-6731',0,N'94805'),
	 ('1954-04-07',N'Heddie',N'Jeffry',14,N'hjeffryja@tripadvisor.com',N'716-443-4330',1,N'98154'),
	 ('1992-02-23',N'Morganne',N'Mash',24,N'mmashjb@virginia.edu',N'336-285-4806',1,N'93601'),
	 ('1954-05-01',N'Muffin',N'Kachel',46,N'mkacheljc@so-net.ne.jp',N'514-340-5389',0,N'94513'),
	 ('1954-05-31',N'Armand',N'Abramowitch',14,N'aabramowitchjd@indiatimes.com',N'408-305-3804',0,N'92703'),
	 ('1976-06-06',N'Caty',N'Paulack',49,N'cpaulackje@elpais.com',N'169-290-3630',1,N'92308');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1993-04-23',N'Krystyna',N'Stuehmeier',10,N'kstuehmeierjf@hubpages.com',N'696-376-6535',0,N'94230'),
	 ('1952-01-28',N'Bev',N'Petricek',47,N'bpetricekjg@bloomberg.com',N'244-828-9511',0,N'93544'),
	 ('1950-10-13',N'Frederique',N'Stace',40,N'fstacejh@zimbio.com',N'513-407-0718',0,N'96101'),
	 ('1966-01-17',N'Alidia',N'Stapleton',39,N'astapletonji@topsy.com',N'629-283-7379',0,N'93036'),
	 ('1994-06-16',N'Jarrid',N'Fernez',12,N'jfernezjj@chicagotribune.com',N'279-254-6949',0,N'29580'),
	 ('1984-09-09',N'Celinda',N'Bunney',19,N'cbunneyjk@newyorker.com',N'952-568-6083',0,N'98663'),
	 ('1981-08-07',N'Alix',N'Mingauld',12,N'amingauldjl@newsvine.com',N'871-207-1360',0,N'98233'),
	 ('1958-07-14',N'Raynor',N'Cordet',40,N'rcordetjm@pagesperso-orange.fr',N'186-909-7960',0,N'98430'),
	 ('1952-07-26',N'Natale',N'Beckmann',45,N'nbeckmannjn@ox.ac.uk',N'266-520-1873',0,N'95824'),
	 ('1956-11-25',N'Leola',N'Cunliffe',23,N'lcunliffejo@cnbc.com',N'820-966-9090',0,N'92132');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1961-06-16',N'Godfry',N'Baden',11,N'gbadenjp@mozilla.com',N'935-144-3189',1,N'91788'),
	 ('1957-07-20',N'Wilhelmina',N'Redmond',49,N'wredmondjq@oakley.com',N'446-212-3087',0,N'94061'),
	 ('1952-04-19',N'Vallie',N'Smedley',37,N'vsmedleyjr@huffingtonpost.com',N'864-976-4446',0,N'96108'),
	 ('1991-06-15',N'Erhard',N'Mc Trusty',36,N'emctrustyjs@washington.edu',N'113-858-6901',1,N'98249'),
	 ('1959-09-14',N'Cecilla',N'Clawley',35,N'cclawleyjt@independent.co.uk',N'914-497-7684',0,N'98446'),
	 ('1962-05-22',N'Arlette',N'Braferton',23,N'abrafertonju@newsvine.com',N'973-233-0187',0,N'99022'),
	 ('1960-08-04',N'Solly',N'Hardwicke',31,N'shardwickejv@pcworld.com',N'818-796-4586',1,N'94557'),
	 ('1999-05-03',N'Annadiana',N'Baudasso',15,N'abaudassojw@tripadvisor.com',N'592-721-1105',0,N'95379'),
	 ('1960-11-26',N'Corabelle',N'Matiebe',15,N'cmatiebejx@tuttocitta.it',N'237-645-0463',1,N'94501'),
	 ('1972-05-17',N'Galven',N'Knifton',21,N'gkniftonjy@thetimes.co.uk',N'655-565-9358',1,N'90251');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1971-01-29',N'Darline',N'Slingsby',10,N'dslingsbyjz@tiny.cc',N'848-279-4589',0,N'29153'),
	 ('1987-06-28',N'Harry',N'Jaray',16,N'hjarayk0@discuz.net',N'715-915-3714',0,N'90662'),
	 ('1990-11-12',N'Ezequiel',N'Leemans',27,N'eleemansk1@devhub.com',N'525-565-4642',0,N'91963'),
	 ('1966-06-30',N'Galina',N'Curragh',48,N'gcurraghk2@nature.com',N'416-417-2913',0,N'29581'),
	 ('1983-11-16',N'Alick',N'Beaton',37,N'abeatonk3@economist.com',N'559-189-5175',0,N'29062'),
	 ('1976-10-02',N'Jessa',N'Degenhardt',17,N'jdegenhardtk4@disqus.com',N'932-787-5017',0,N'98059'),
	 ('1960-04-26',N'Jolene',N'Andrelli',19,N'jandrellik5@e-recht24.de',N'290-646-7580',0,N'95837'),
	 ('1994-09-18',N'Karlyn',N'Etoile',11,N'ketoilek6@homestead.com',N'505-837-5446',0,N'95610'),
	 ('1986-09-26',N'Luz',N'Adiscot',16,N'ladiscotk7@homestead.com',N'634-796-7469',0,N'90039'),
	 ('1980-11-25',N'Lucio',N'Churchouse',50,N'lchurchousek8@arizona.edu',N'806-523-7802',0,N'95369');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1972-06-10',N'Sari',N'Digg',47,N'sdiggk9@deviantart.com',N'976-892-0092',0,N'98619'),
	 ('1994-02-07',N'Spike',N'Dienes',35,N'sdieneska@bloglovin.com',N'488-215-5706',0,N'91505'),
	 ('1984-02-20',N'Rosabelle',N'Bloxham',25,N'rbloxhamkb@examiner.com',N'526-568-4003',1,N'92264'),
	 ('1976-05-14',N'Reggie',N'Tofanelli',12,N'rtofanellikc@360.cn',N'934-991-5492',0,N'92571'),
	 ('1969-05-19',N'Maryjo',N'Colbert',22,N'mcolbertkd@harvard.edu',N'908-407-2248',0,N'93654'),
	 ('1953-02-04',N'Gael',N'Fardoe',17,N'gfardoeke@ed.gov',N'920-341-6576',1,N'98841'),
	 ('1971-01-15',N'Helsa',N'Wardhaw',25,N'hwardhawkf@twitter.com',N'134-990-0175',0,N'91202'),
	 ('1976-10-24',N'Cal',N'Joblin',16,N'cjoblinkg@washington.edu',N'175-967-7423',1,N'91409'),
	 ('1977-11-16',N'Sayers',N'Howell',47,N'showellkh@gmpg.org',N'175-623-8028',0,N'94559'),
	 ('1986-03-10',N'Susannah',N'Clemits',50,N'sclemitski@friendfeed.com',N'321-463-7040',0,N'98181');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1974-03-22',N'Leopold',N'Tomsett',39,N'ltomsettkj@jimdo.com',N'509-228-9195',0,N'90749'),
	 ('1956-02-20',N'Jillayne',N'Ainsby',13,N'jainsbykk@psu.edu',N'739-203-0041',0,N'94042'),
	 ('1999-11-13',N'Terrence',N'Bowle',38,N'tbowlekl@vinaora.com',N'206-957-9363',0,N'95673'),
	 ('1950-11-05',N'Danette',N'Duckworth',46,N'dduckworthkm@nytimes.com',N'461-240-1010',0,N'93304'),
	 ('1956-01-10',N'Griswold',N'Metherell',27,N'gmetherellkn@ustream.tv',N'918-939-3173',0,N'94510'),
	 ('1964-10-25',N'Lydie',N'Ponnsett',32,N'lponnsettko@google.pl',N'719-232-2892',0,N'29379'),
	 ('1992-11-24',N'Taddeo',N'Berkley',18,N'tberkleykp@jigsy.com',N'367-687-7427',0,N'92673'),
	 ('1961-05-28',N'Bax',N'Bwye',44,N'bbwyekq@hubpages.com',N'522-208-2736',0,N'92509'),
	 ('1952-04-05',N'Garold',N'Marr',11,N'gmarrkr@meetup.com',N'267-178-6910',1,N'94109'),
	 ('1971-07-25',N'Bethena',N'Armsby',13,N'barmsbyks@elegantthemes.com',N'765-532-9982',0,N'91732');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1973-05-25',N'Retha',N'Zottoli',21,N'rzottolikt@latimes.com',N'374-443-1649',0,N'94963'),
	 ('1984-09-05',N'Cristy',N'Woolis',25,N'cwoolisku@chron.com',N'726-457-3096',0,N'94165'),
	 ('1971-04-07',N'Garvey',N'Charley',12,N'gcharleykv@fastcompany.com',N'622-611-4470',0,N'91797'),
	 ('1973-08-31',N'Bone',N'Loghan',35,N'bloghankw@shop-pro.jp',N'648-900-8568',0,N'29642'),
	 ('1994-09-28',N'Mollie',N'Bourgourd',18,N'mbourgourdkx@altervista.org',N'586-778-6418',0,N'94136'),
	 ('1994-09-13',N'Gabie',N'Band',29,N'gbandky@yahoo.co.jp',N'369-203-2230',0,N'92116'),
	 ('1964-09-15',N'Grayce',N'Gaskal',15,N'ggaskalkz@biblegateway.com',N'410-746-4190',1,N'94105'),
	 ('1983-08-06',N'Kalil',N'Bonn',42,N'kbonnl0@prnewswire.com',N'757-421-3341',0,N'95473'),
	 ('1995-06-04',N'Tonye',N'Prendergast',39,N'tprendergastl1@salon.com',N'412-130-2573',0,N'92014'),
	 ('1969-10-24',N'Gilbertine',N'Aynsley',47,N'gaynsleyl2@issuu.com',N'673-737-9527',1,N'98255');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1992-01-09',N'Preston',N'Meek',42,N'pmeekl3@jigsy.com',N'557-322-6690',0,N'92879'),
	 ('1978-01-22',N'Thomas',N'Ozelton',13,N'tozeltonl4@dmoz.org',N'968-273-2013',0,N'91346'),
	 ('1960-08-05',N'Alessandro',N'Henrot',43,N'ahenrotl5@redcross.org',N'342-170-4122',0,N'91908'),
	 ('1972-08-21',N'Ronna',N'Culley',15,N'rculleyl6@shop-pro.jp',N'117-142-6055',0,N'99137'),
	 ('1993-08-22',N'Rafe',N'Scay',38,N'rscayl7@addtoany.com',N'806-867-3406',0,N'92883'),
	 ('1974-01-26',N'Palm',N'Salmoni',37,N'psalmonil8@webmd.com',N'448-614-4301',0,N'98064'),
	 ('1994-12-29',N'Nanon',N'MacFadzan',17,N'nmacfadzanl9@usnews.com',N'919-276-1603',0,N'95161'),
	 ('1965-04-27',N'Major',N'Broster',26,N'mbrosterla@cpanel.net',N'906-833-4525',0,N'92605'),
	 ('1957-06-13',N'Cam',N'Waulker',42,N'cwaulkerlb@ox.ac.uk',N'565-709-6125',0,N'91910'),
	 ('1956-06-05',N'Boy',N'Enderlein',31,N'benderleinlc@sakura.ne.jp',N'393-887-0421',0,N'29658');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1959-12-21',N'Willow',N'Back',34,N'wbackld@buzzfeed.com',N'582-817-8938',1,N'99005'),
	 ('1977-04-22',N'Lazarus',N'Billes',46,N'lbillesle@admin.ch',N'600-850-7584',0,N'94612'),
	 ('1979-11-15',N'Selena',N'Wimbury',30,N'swimburylf@apple.com',N'787-955-6739',0,N'92158'),
	 ('1990-08-07',N'Reggy',N'Fearns',48,N'rfearnslg@dell.com',N'123-972-8023',0,N'99154'),
	 ('1961-08-18',N'Annice',N'Haggett',36,N'ahaggettlh@businesswire.com',N'784-748-2749',0,N'29921'),
	 ('1984-12-21',N'Dennie',N'Maddrell',45,N'dmaddrellli@sphinn.com',N'593-317-8605',0,N'91495'),
	 ('1963-12-04',N'Xerxes',N'Kenefick',50,N'xkeneficklj@zimbio.com',N'861-578-6102',0,N'94572'),
	 ('1958-09-08',N'Saidee',N'Melloy',25,N'smelloylk@gmpg.org',N'381-790-1147',0,N'90094'),
	 ('1986-09-30',N'Stacie',N'Chatt',30,N'schattll@census.gov',N'705-433-1060',0,N'98593'),
	 ('1953-04-27',N'Jill',N'Farre',42,N'jfarrelm@google.co.jp',N'665-491-6076',0,N'90637');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1960-06-20',N'Tobiah',N'Riggoll',16,N'triggollln@about.me',N'399-775-0206',0,N'29924'),
	 ('1983-11-07',N'Josh',N'Alu',10,N'jalulo@nature.com',N'341-546-3455',0,N'95210'),
	 ('1969-06-05',N'Norton',N'Brimblecomb',47,N'nbrimblecomblp@techcrunch.com',N'105-803-4380',0,N'93382'),
	 ('1972-02-12',N'Piper',N'Pester',15,N'ppesterlq@slideshare.net',N'703-668-8104',0,N'99169'),
	 ('1964-10-09',N'Zack',N'Luttgert',25,N'zluttgertlr@umn.edu',N'820-477-5109',0,N'91799'),
	 ('1998-01-14',N'Hill',N'Oseland',15,N'hoselandls@woothemes.com',N'609-729-8477',0,N'99321'),
	 ('1968-08-06',N'Krishna',N'Donati',45,N'kdonatilt@webeden.co.uk',N'686-347-2625',0,N'93403'),
	 ('1981-10-09',N'Tiff',N'Eshelby',35,N'teshelbylu@census.gov',N'196-624-3730',0,N'98660'),
	 ('1959-04-25',N'Nannie',N'Sapp',11,N'nsapplv@goo.gl',N'791-735-0513',0,N'95019'),
	 ('1955-10-05',N'Cullen',N'Dubois',14,N'cduboislw@china.com.cn',N'553-408-1864',0,N'29385');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1957-06-04',N'Roobbie',N'Ruffey',26,N'rruffeylx@kickstarter.com',N'290-846-2210',0,N'99216'),
	 ('1994-11-02',N'Hedwig',N'Berkowitz',21,N'hberkowitzly@merriam-webster.com',N'396-967-3210',0,N'91944'),
	 ('1977-04-27',N'Zelma',N'Denerley',27,N'zdenerleylz@vinaora.com',N'817-550-5871',0,N'94124'),
	 ('1992-09-03',N'Niel',N'Fintoph',17,N'nfintophm0@guardian.co.uk',N'465-340-6259',0,N'92602'),
	 ('1951-09-15',N'Oliver',N'Jonin',19,N'ojoninm1@cbsnews.com',N'758-952-6019',0,N'92168'),
	 ('1959-03-02',N'Eustacia',N'Ellice',18,N'eellicem2@usda.gov',N'958-893-7717',0,N'99128'),
	 ('1999-01-04',N'Natividad',N'Grcic',40,N'ngrcicm3@t.co',N'152-319-5220',0,N'90070'),
	 ('1962-02-14',N'Danie',N'Longmate',48,N'dlongmatem4@xing.com',N'588-721-4952',1,N'94608'),
	 ('1996-08-11',N'Sidney',N'Rishbrook',42,N'srishbrookm5@de.vu',N'299-769-3337',0,N'92654'),
	 ('1997-10-14',N'Sadye',N'Praten',43,N'spratenm6@discuz.net',N'598-392-8255',0,N'93561');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1969-08-17',N'Linoel',N'Edmott',17,N'ledmottm7@is.gd',N'895-982-6078',1,N'98203'),
	 ('1951-11-19',N'Deloris',N'Scoble',31,N'dscoblem8@cdc.gov',N'494-877-5660',0,N'94598'),
	 ('1972-04-28',N'Tonnie',N'Bywaters',46,N'tbywatersm9@mit.edu',N'719-365-0417',0,N'29040'),
	 ('1956-01-28',N'Diarmid',N'Tillyer',18,N'dtillyerma@mtv.com',N'875-510-1543',0,N'93744'),
	 ('1991-06-07',N'Isacco',N'Meininking',34,N'imeininkingmb@opensource.org',N'729-811-8514',1,N'98661'),
	 ('1975-11-15',N'Nonnah',N'Ortiger',40,N'nortigermc@vk.com',N'936-219-3521',0,N'92282'),
	 ('1999-01-29',N'Devlin',N'Haresign',11,N'dharesignmd@phpbb.com',N'799-496-5271',1,N'95012'),
	 ('1964-07-09',N'Kerrin',N'Szubert',50,N'kszubertme@scientificamerican.com',N'127-737-4447',0,N'95320'),
	 ('1981-01-01',N'Georgette',N'Singyard',13,N'gsingyardmf@gmpg.org',N'135-943-2249',0,N'91390'),
	 ('1988-08-26',N'Pat',N'Blacksland',29,N'pblackslandmg@hao123.com',N'802-702-7999',1,N'90102');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1964-01-03',N'Joly',N'Napoli',28,N'jnapolimh@quantcast.com',N'435-746-9026',0,N'92211'),
	 ('1956-04-24',N'Jorge',N'Ledram',10,N'jledrammi@squidoo.com',N'269-848-2010',0,N'29686'),
	 ('1972-04-12',N'Giselbert',N'O''Harney',27,N'goharneymj@adobe.com',N'398-617-1858',0,N'92596'),
	 ('1952-01-23',N'Spencer',N'Keneleyside',33,N'skeneleysidemk@ow.ly',N'470-460-0296',0,N'98557'),
	 ('1965-08-18',N'Bob',N'Meake',15,N'bmeakeml@cisco.com',N'286-417-1534',0,N'99356'),
	 ('1979-05-01',N'Herold',N'Lenham',34,N'hlenhammm@mail.ru',N'338-293-5005',0,N'94027'),
	 ('1977-04-02',N'Megen',N'Finlator',43,N'mfinlatormn@unesco.org',N'474-711-2859',1,N'95813'),
	 ('1957-01-29',N'Susannah',N'Gurton',25,N'sgurtonmo@csmonitor.com',N'789-643-3778',0,N'93722'),
	 ('1993-09-09',N'Archie',N'Craigg',49,N'acraiggmp@engadget.com',N'192-801-4649',1,N'95932'),
	 ('1962-03-11',N'Wallas',N'Addison',47,N'waddisonmq@discovery.com',N'256-932-7670',0,N'91124');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1969-12-06',N'Rosella',N'Jerrams',32,N'rjerramsmr@merriam-webster.com',N'592-325-9380',0,N'90102'),
	 ('1950-04-26',N'Dalton',N'Winks',29,N'dwinksms@quantcast.com',N'305-157-1996',0,N'29322'),
	 ('1952-02-10',N'Omero',N'Coronas',38,N'ocoronasmt@vkontakte.ru',N'297-482-9790',0,N'95472'),
	 ('1972-02-02',N'Maia',N'Benka',33,N'mbenkamu@rakuten.co.jp',N'306-370-8576',1,N'95131'),
	 ('1984-11-24',N'Marchall',N'Cadlock',44,N'mcadlockmv@lycos.com',N'206-390-2069',0,N'92692'),
	 ('1994-10-27',N'Carmon',N'Pavkovic',12,N'cpavkovicmw@usnews.com',N'646-319-9771',1,N'91404'),
	 ('1983-04-02',N'Jarret',N'Ruperto',26,N'jrupertomx@youtu.be',N'185-712-1980',0,N'93455'),
	 ('1984-09-29',N'Ruben',N'Stollberger',16,N'rstollbergermy@wikia.com',N'294-401-6459',0,N'98374'),
	 ('1999-07-22',N'Teriann',N'Gregoriou',39,N'tgregorioumz@example.com',N'385-318-2942',0,N'95406'),
	 ('1964-05-18',N'Nanci',N'Liveing',46,N'nliveingn0@deliciousdays.com',N'607-637-9884',0,N'93771');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1970-08-03',N'Meryl',N'Scheu',48,N'mscheun1@jiathis.com',N'720-597-1728',0,N'99016'),
	 ('1952-09-16',N'Iggy',N'Benettini',44,N'ibenettinin2@ucoz.com',N'836-425-2769',0,N'29640'),
	 ('1958-09-27',N'Ibrahim',N'Alldis',36,N'ialldisn3@auda.org.au',N'135-740-7369',0,N'95637'),
	 ('2000-02-13',N'Gaynor',N'Shawley',16,N'gshawleyn4@ask.com',N'790-669-6596',0,N'95202'),
	 ('1980-08-13',N'Angy',N'Spellessy',33,N'aspellessyn5@wix.com',N'492-706-4642',0,N'90011'),
	 ('1997-04-21',N'Bernice',N'Skypp',21,N'bskyppn6@cnn.com',N'540-855-1722',0,N'91506'),
	 ('1969-08-05',N'Shamus',N'Burnsyde',50,N'sburnsyden7@merriam-webster.com',N'903-578-2593',0,N'95450'),
	 ('1985-08-18',N'Mohammed',N'Gerling',20,N'mgerlingn8@tiny.cc',N'877-301-2440',0,N'98041'),
	 ('1967-08-08',N'Iris',N'Mattingson',49,N'imattingsonn9@prnewswire.com',N'282-290-5677',0,N'29594'),
	 ('1994-06-24',N'Shina',N'Gowdy',33,N'sgowdyna@networksolutions.com',N'156-506-9801',0,N'95363');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1968-04-23',N'Nelia',N'Shakeshaft',42,N'nshakeshaftnb@hao123.com',N'680-359-1547',0,N'90706'),
	 ('1990-09-24',N'Flss',N'Greeveson',12,N'fgreevesonnc@flickr.com',N'789-661-6260',0,N'94248'),
	 ('1992-07-12',N'Meris',N'Donegan',14,N'mdonegannd@dailymotion.com',N'873-325-0685',0,N'98903'),
	 ('1994-07-02',N'Dorie',N'Frammingham',15,N'dframminghamne@wikispaces.com',N'912-791-0118',0,N'95444'),
	 ('1962-08-29',N'Carlo',N'Bruster',49,N'cbrusternf@feedburner.com',N'969-393-8292',0,N'91775'),
	 ('1996-12-26',N'Nichols',N'Pervew',35,N'npervewng@timesonline.co.uk',N'465-247-7194',0,N'92595'),
	 ('1967-12-29',N'Janeen',N'Wyllcocks',38,N'jwyllcocksnh@myspace.com',N'363-580-1952',0,N'91108'),
	 ('1995-11-22',N'Blinnie',N'Westmerland',49,N'bwestmerlandni@mail.ru',N'408-260-1603',0,N'93448'),
	 ('1998-06-03',N'Monty',N'Fumagall',49,N'mfumagallnj@amazon.com',N'168-422-1624',0,N'29405'),
	 ('1998-12-13',N'Esther',N'Nani',42,N'enanink@barnesandnoble.com',N'705-492-8745',0,N'91761');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1950-07-02',N'Vi',N'Wimp',10,N'vwimpnl@sogou.com',N'599-257-6691',0,N'91750'),
	 ('1991-12-11',N'Lizabeth',N'Lerigo',19,N'llerigonm@ox.ac.uk',N'167-780-8613',0,N'99224'),
	 ('1952-09-27',N'Tull',N'Cord',34,N'tcordnn@elpais.com',N'305-892-4876',0,N'93701'),
	 ('1952-03-20',N'Bev',N'O''Dunneen',42,N'bodunneenno@about.com',N'973-293-7358',1,N'29002'),
	 ('1989-07-13',N'Cate',N'Keynes',40,N'ckeynesnp@adobe.com',N'581-845-4780',0,N'92202'),
	 ('1993-03-22',N'Ingeberg',N'Bramwich',31,N'ibramwichnq@comcast.net',N'565-982-0793',0,N'94580'),
	 ('1980-03-05',N'Earvin',N'Brahmer',30,N'ebrahmernr@github.io',N'526-954-3983',0,N'91381'),
	 ('1999-04-11',N'Madalyn',N'Diwell',38,N'mdiwellns@dot.gov',N'382-648-4213',1,N'98164'),
	 ('1979-05-04',N'Millie',N'Greatorex',24,N'mgreatorexnt@time.com',N'420-218-5656',0,N'99348'),
	 ('1979-12-31',N'Justinn',N'Vanyarkin',17,N'jvanyarkinnu@cdbaby.com',N'879-946-5285',0,N'93230');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1986-09-08',N'Horten',N'Topley',37,N'htopleynv@shop-pro.jp',N'622-850-3415',1,N'98374'),
	 ('1966-03-06',N'Orv',N'Alessandrini',36,N'oalessandrininw@ezinearticles.com',N'840-954-5365',0,N'98576'),
	 ('1982-05-11',N'Merralee',N'Tembey',50,N'mtembeynx@adobe.com',N'727-622-9763',0,N'92026'),
	 ('1971-10-05',N'Leif',N'Davidwitz',22,N'ldavidwitzny@addthis.com',N'299-526-1017',0,N'95388'),
	 ('1967-04-27',N'Cynthy',N'Dewett',18,N'cdewettnz@sbwire.com',N'561-567-5268',0,N'95062'),
	 ('1970-01-07',N'Lindie',N'Golda',43,N'lgoldao0@cmu.edu',N'193-519-9332',1,N'95665'),
	 ('1989-01-25',N'Scotti',N'Hugland',26,N'shuglando1@privacy.gov.au',N'907-324-0906',0,N'90853'),
	 ('1996-10-31',N'El',N'Fitzhenry',44,N'efitzhenryo2@goodreads.com',N'777-699-1297',0,N'93516'),
	 ('1992-12-17',N'Brnaby',N'Chastney',37,N'bchastneyo3@creativecommons.org',N'306-595-4435',0,N'98366'),
	 ('1962-02-24',N'Sunny',N'Greasty',31,N'sgreastyo4@dell.com',N'240-750-0173',1,N'29708');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1952-04-01',N'Benoite',N'Wheelton',41,N'bwheeltono5@imdb.com',N'684-154-2624',1,N'92308'),
	 ('1986-11-20',N'Retha',N'Hambrick',11,N'rhambricko6@sogou.com',N'172-729-7469',0,N'99218'),
	 ('1976-07-24',N'Huntlee',N'Perrone',17,N'hperroneo7@xrea.com',N'731-855-0419',0,N'92024'),
	 ('1992-06-06',N'Kirstyn',N'Friedlos',20,N'kfriedloso8@baidu.com',N'366-946-6506',0,N'91496'),
	 ('1982-10-23',N'Melba',N'Pallis',34,N'mpalliso9@discovery.com',N'407-564-0089',0,N'95833'),
	 ('1970-12-22',N'Gavin',N'Enochsson',12,N'genochssonoa@e-recht24.de',N'986-152-4263',0,N'29407'),
	 ('1967-02-11',N'Clarisse',N'Caso',22,N'ccasoob@bbc.co.uk',N'669-673-6872',1,N'95375'),
	 ('1950-06-13',N'Charline',N'Kimmons',20,N'ckimmonsoc@sitemeter.com',N'394-242-3111',0,N'90061'),
	 ('1955-01-25',N'Dennis',N'Fishbourn',42,N'dfishbournod@purevolume.com',N'762-946-1001',1,N'94565'),
	 ('1988-05-10',N'Alexis',N'Fyrth',26,N'afyrthoe@latimes.com',N'346-109-4403',0,N'90805');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1981-01-03',N'Minne',N'Calladine',35,N'mcalladineof@constantcontact.com',N'923-430-3045',0,N'29841'),
	 ('1997-08-04',N'Hyacinthe',N'Hulstrom',39,N'hhulstromog@foxnews.com',N'929-384-0279',0,N'98843'),
	 ('1973-04-06',N'Andres',N'Gosnay',10,N'agosnayoh@china.com.cn',N'437-921-1386',0,N'90055'),
	 ('1999-01-26',N'Leta',N'Konert',17,N'lkonertoi@adobe.com',N'199-485-3583',0,N'29805'),
	 ('1973-03-05',N'Happy',N'Goodlake',33,N'hgoodlakeoj@ucoz.com',N'106-768-6466',0,N'98539'),
	 ('1959-09-25',N'Pauline',N'Rentcome',44,N'prentcomeok@irs.gov',N'795-499-0772',0,N'94273'),
	 ('1983-09-07',N'Martainn',N'Corpe',39,N'mcorpeol@irs.gov',N'555-842-6063',0,N'90401'),
	 ('1976-03-17',N'Effie',N'Semper',14,N'esemperom@ted.com',N'675-372-2917',0,N'29388'),
	 ('1955-02-04',N'Ferd',N'O''Moylane',40,N'fomoylaneon@fc2.com',N'485-846-3303',0,N'98599'),
	 ('1997-07-01',N'Ronda',N'Jeakins',20,N'rjeakinsoo@weebly.com',N'315-246-9383',1,N'90070');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1979-12-14',N'Marylee',N'Leadston',24,N'mleadstonop@seesaa.net',N'688-373-9499',1,N'29912'),
	 ('1958-04-10',N'Fons',N'Pickston',15,N'fpickstonoq@github.com',N'546-610-2880',0,N'29613'),
	 ('1981-06-30',N'Coral',N'Cheng',48,N'cchengor@pinterest.com',N'413-907-4654',0,N'94937'),
	 ('1984-06-18',N'Kariotta',N'Hadden',15,N'khaddenos@economist.com',N'103-923-8520',0,N'92648'),
	 ('1964-06-26',N'Kore',N'Braid',16,N'kbraidot@so-net.ne.jp',N'898-635-4792',1,N'29154'),
	 ('1993-09-01',N'Margery',N'Hutchin',45,N'mhutchinou@intel.com',N'793-712-6646',0,N'92544'),
	 ('1975-03-11',N'Hedda',N'Ammer',20,N'hammerov@java.com',N'532-843-3417',0,N'29226'),
	 ('1992-07-18',N'Joli',N'Naton',41,N'jnatonow@stanford.edu',N'574-178-4909',0,N'93266'),
	 ('1994-03-11',N'Cory',N'Macrae',32,N'cmacraeox@parallels.com',N'176-233-5784',0,N'99012'),
	 ('1998-09-06',N'Meredeth',N'Woolvett',21,N'mwoolvettoy@pen.io',N'605-694-7631',0,N'98604');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1952-06-03',N'Lester',N'Backshill',22,N'lbackshilloz@imageshack.us',N'978-596-3115',0,N'93654'),
	 ('1958-12-17',N'Raphaela',N'Stoneham',42,N'rstonehamp0@upenn.edu',N'529-504-7761',0,N'90508'),
	 ('1985-02-23',N'Moe',N'Lendon',36,N'mlendonp1@php.net',N'237-896-8712',0,N'94706'),
	 ('1996-08-05',N'Holden',N'Brittin',14,N'hbrittinp2@yandex.ru',N'147-515-1015',0,N'98851'),
	 ('1960-12-24',N'Westleigh',N'Routley',37,N'wroutleyp3@about.me',N'882-447-6308',0,N'99252'),
	 ('1985-10-06',N'Alyse',N'Dugall',43,N'adugallp4@mozilla.com',N'773-589-2006',0,N'99113'),
	 ('1963-10-15',N'Shanie',N'Treneer',30,N'streneerp5@aboutads.info',N'460-849-5285',0,N'92401'),
	 ('1950-08-24',N'Adriena',N'Kornel',36,N'akornelp6@archive.org',N'353-746-9198',0,N'94609'),
	 ('1972-04-08',N'Thekla',N'Novill',32,N'tnovillp7@pcworld.com',N'798-958-4386',0,N'98207'),
	 ('1972-06-07',N'Rhianna',N'Nussen',30,N'rnussenp8@icio.us',N'791-592-8207',1,N'95155');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1983-03-03',N'Laurence',N'Loveless',43,N'llovelessp9@wsj.com',N'562-860-8490',0,N'94153'),
	 ('1996-11-07',N'Lyn',N'Spendlove',31,N'lspendlovepa@biblegateway.com',N'162-956-6904',0,N'95012'),
	 ('1971-10-01',N'Faye',N'Topp',46,N'ftopppb@odnoklassniki.ru',N'760-668-0704',0,N'94401'),
	 ('1995-01-07',N'Thomasine',N'Burdess',15,N'tburdesspc@google.es',N'167-547-0281',0,N'90621'),
	 ('1951-01-17',N'Sonny',N'Spelman',42,N'sspelmanpd@youku.com',N'816-759-5217',0,N'92317'),
	 ('1956-01-09',N'Reginald',N'Siflet',43,N'rsifletpe@so-net.ne.jp',N'863-141-9805',0,N'92161'),
	 ('1972-02-13',N'Emlyn',N'Georgel',26,N'egeorgelpf@ucoz.ru',N'658-279-6003',1,N'93111'),
	 ('1995-02-18',N'Vilhelmina',N'Borgne',49,N'vborgnepg@sun.com',N'407-470-3200',1,N'91963'),
	 ('1960-08-17',N'Jake',N'Burcombe',38,N'jburcombeph@163.com',N'632-644-6738',0,N'93662'),
	 ('1980-04-29',N'Devin',N'Brocket',15,N'dbrocketpi@github.com',N'826-887-9576',0,N'92039');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1997-12-02',N'Georgia',N'Slides',49,N'gslidespj@marriott.com',N'255-478-5375',0,N'91946'),
	 ('1953-11-30',N'Jone',N'Phillins',49,N'jphillinspk@youku.com',N'898-274-9574',1,N'95191'),
	 ('1970-12-28',N'Shandie',N'Quarless',49,N'squarlesspl@imgur.com',N'664-932-6636',1,N'94544'),
	 ('1980-06-07',N'Charmaine',N'O''Fihillie',20,N'cofihilliepm@gmpg.org',N'408-437-6850',1,N'95674'),
	 ('1955-01-21',N'Cristiano',N'Liddyard',20,N'cliddyardpn@stanford.edu',N'264-928-2644',0,N'94020'),
	 ('1982-08-13',N'Konstantine',N'Clague',37,N'kclaguepo@edublogs.org',N'889-285-3609',0,N'91405'),
	 ('1983-09-11',N'Austina',N'Westlake',10,N'awestlakepp@com.com',N'323-131-8054',0,N'93607'),
	 ('1988-08-14',N'Stern',N'Dearl',19,N'sdearlpq@netvibes.com',N'825-208-9132',0,N'29204'),
	 ('1994-11-01',N'Emogene',N'Djorvic',14,N'edjorvicpr@mapquest.com',N'804-311-3713',0,N'90224'),
	 ('1972-08-03',N'Augusto',N'Bernardotti',50,N'abernardottips@skype.com',N'498-846-9070',0,N'29844');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1998-10-22',N'Nolly',N'Rehor',27,N'nrehorpt@washington.edu',N'554-783-8865',0,N'95674'),
	 ('1983-07-28',N'Jodee',N'Marians',23,N'jmarianspu@vimeo.com',N'295-807-9577',0,N'29587'),
	 ('1970-12-15',N'Constancy',N'Mougin',35,N'cmouginpv@canalblog.com',N'830-230-3819',0,N'90265'),
	 ('1961-01-20',N'Caryn',N'Piken',15,N'cpikenpw@nbcnews.com',N'105-132-2436',0,N'92160'),
	 ('1994-10-25',N'Benedict',N'Panchen',24,N'bpanchenpx@rakuten.co.jp',N'662-540-4195',0,N'98548'),
	 ('1962-04-19',N'Christoforo',N'Ruckhard',17,N'cruckhardpy@merriam-webster.com',N'547-297-1957',0,N'93553'),
	 ('1960-09-24',N'Karlens',N'Sneaker',18,N'ksneakerpz@desdev.cn',N'371-866-1678',1,N'92107'),
	 ('1962-10-31',N'Felicity',N'Gheerhaert',40,N'fgheerhaertq0@mac.com',N'540-204-4721',0,N'95587'),
	 ('1962-04-29',N'Frieda',N'Jeenes',13,N'fjeenesq1@yahoo.co.jp',N'922-305-2133',0,N'95073'),
	 ('1994-02-21',N'Carina',N'Salliss',38,N'csallissq2@google.cn',N'802-880-0344',0,N'98392');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1984-08-14',N'Clair',N'Esberger',34,N'cesbergerq3@fastcompany.com',N'279-745-3484',1,N'92415'),
	 ('1952-12-07',N'Joela',N'Casaro',10,N'jcasaroq4@nationalgeographic.com',N'538-373-5256',0,N'99155'),
	 ('1999-03-29',N'Alexina',N'Bale',42,N'abaleq5@si.edu',N'576-897-0957',0,N'95538'),
	 ('1965-11-03',N'Arv',N'Concannon',25,N'aconcannonq6@alibaba.com',N'551-135-7987',0,N'98687'),
	 ('1994-12-05',N'Bethanne',N'Quinnet',14,N'bquinnetq7@w3.org',N'163-774-3738',0,N'93553'),
	 ('1999-04-08',N'Boris',N'Harbison',44,N'bharbisonq8@mozilla.org',N'319-244-0193',0,N'95077'),
	 ('1996-03-10',N'Tamara',N'Mountjoy',30,N'tmountjoyq9@slideshare.net',N'807-796-3152',0,N'90833'),
	 ('1964-12-10',N'Curtis',N'Gytesham',34,N'cgyteshamqa@hugedomains.com',N'202-555-4949',0,N'98841'),
	 ('1987-07-10',N'Austina',N'Pecey',40,N'apeceyqb@netlog.com',N'459-201-4506',0,N'98830'),
	 ('1988-03-14',N'Garth',N'Du Fray',26,N'gdufrayqc@gov.uk',N'138-633-7510',0,N'93221');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1986-07-05',N'Devin',N'Sushams',12,N'dsushamsqd@last.fm',N'324-850-5406',0,N'93465'),
	 ('1966-03-28',N'Hewet',N'Klimowski',37,N'hklimowskiqe@pcworld.com',N'410-898-4621',0,N'29135'),
	 ('1958-12-06',N'Ferdinand',N'Bagg',39,N'fbaggqf@mysql.com',N'308-134-2310',1,N'92262'),
	 ('1953-05-31',N'Rockey',N'Gillmor',40,N'rgillmorqg@com.com',N'715-220-3053',0,N'99114'),
	 ('1981-03-23',N'Kin',N'Lys',13,N'klysqh@dot.gov',N'693-690-4528',0,N'29146'),
	 ('1981-02-22',N'Alvira',N'O''Kerin',21,N'aokerinqi@who.int',N'652-788-4646',0,N'92624'),
	 ('1956-03-11',N'Barn',N'Mizen',14,N'bmizenqj@yellowpages.com',N'551-663-2484',0,N'29385'),
	 ('1988-11-14',N'Reece',N'Duguid',20,N'rduguidqk@cbsnews.com',N'295-989-4576',0,N'93524'),
	 ('1961-11-12',N'Carmella',N'Janacek',14,N'cjanacekql@amazonaws.com',N'756-902-2856',0,N'98338'),
	 ('1989-08-14',N'Casi',N'Antoniewski',30,N'cantoniewskiqm@netscape.com',N'382-976-2975',0,N'96145');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1956-12-25',N'Cassondra',N'Clorley',10,N'cclorleyqn@latimes.com',N'508-164-1415',0,N'98852'),
	 ('1988-10-14',N'Mortimer',N'Habard',49,N'mhabardqo@army.mil',N'106-249-7334',0,N'93010'),
	 ('1982-02-22',N'Byrom',N'Filipychev',11,N'bfilipychevqp@census.gov',N'546-741-4158',0,N'95831'),
	 ('1985-01-15',N'Penelopa',N'Heckner',26,N'phecknerqq@yahoo.com',N'384-656-4409',1,N'90307'),
	 ('1967-09-09',N'Jan',N'Rawsen',18,N'jrawsenqr@merriam-webster.com',N'550-138-5835',0,N'92088'),
	 ('1986-02-10',N'Thain',N'Obert',25,N'tobertqs@baidu.com',N'756-498-1462',0,N'95866'),
	 ('1980-07-29',N'Birdie',N'Rogez',36,N'brogezqt@cnbc.com',N'109-136-4432',0,N'29493'),
	 ('1968-08-14',N'Ilka',N'Anfonsi',24,N'ianfonsiqu@amazonaws.com',N'613-106-4376',0,N'94293'),
	 ('1958-10-10',N'Birch',N'Zannini',31,N'bzanniniqv@lulu.com',N'569-911-7551',0,N'92570'),
	 ('1985-08-26',N'Inge',N'Vermer',12,N'ivermerqw@newsvine.com',N'307-108-2349',0,N'95695');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1953-11-05',N'Minda',N'Shoubridge',15,N'mshoubridgeqx@sakura.ne.jp',N'475-719-6368',0,N'95497'),
	 ('1950-12-14',N'Hughie',N'Trevon',36,N'htrevonqy@hubpages.com',N'205-787-1040',1,N'94286'),
	 ('1959-08-07',N'Julita',N'Blindermann',43,N'jblindermannqz@buzzfeed.com',N'725-860-9127',1,N'91903'),
	 ('1958-07-20',N'Maureene',N'Klais',43,N'mklaisr0@redcross.org',N'116-834-0031',0,N'29340'),
	 ('1965-01-26',N'Alyda',N'Berridge',30,N'aberridger1@adobe.com',N'198-639-8798',0,N'95005'),
	 ('1997-12-03',N'Erie',N'Gibbin',27,N'egibbinr2@cyberchimps.com',N'889-123-7261',0,N'93541'),
	 ('1951-08-28',N'Alameda',N'Frizzell',33,N'afrizzellr3@lycos.com',N'786-558-4712',1,N'92337'),
	 ('1970-06-26',N'Garvey',N'Tunaclift',33,N'gtunacliftr4@uiuc.edu',N'829-955-9860',1,N'90507'),
	 ('1963-08-19',N'Johanna',N'Tottem',10,N'jtottemr5@nbcnews.com',N'314-891-4918',0,N'92654'),
	 ('1956-05-09',N'Jenifer',N'Santo',23,N'jsantor6@noaa.gov',N'266-241-5015',1,N'95703');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1968-09-14',N'Hillery',N'Dineen',48,N'hdineenr7@yellowpages.com',N'681-451-8888',0,N'98421'),
	 ('1993-12-05',N'Roz',N'Willmot',24,N'rwillmotr8@google.de',N'695-656-5253',0,N'94118'),
	 ('1961-08-08',N'Izzy',N'Langelaan',47,N'ilangelaanr9@quantcast.com',N'322-381-7490',0,N'92356'),
	 ('1969-01-05',N'Aube',N'McGahern',22,N'amcgahernra@etsy.com',N'298-112-1409',0,N'92301'),
	 ('1979-12-05',N'Virge',N'Verry',11,N'vverryrb@yellowpages.com',N'723-900-4663',0,N'95120'),
	 ('1985-03-14',N'Babs',N'Deverson',24,N'bdeversonrc@symantec.com',N'678-111-1263',0,N'95031'),
	 ('2000-03-14',N'Dirk',N'Cicetti',20,N'dcicettird@seattletimes.com',N'291-266-4789',0,N'98261'),
	 ('1990-09-17',N'Bonnie',N'Winmill',22,N'bwinmillre@nydailynews.com',N'319-606-5304',0,N'98448'),
	 ('1986-11-04',N'Pauly',N'Parkyn',29,N'pparkynrf@hc360.com',N'641-164-2491',0,N'92158'),
	 ('1987-05-21',N'Eloise',N'Ambresin',49,N'eambresinrg@princeton.edu',N'282-799-3322',0,N'95157');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1998-02-02',N'Kikelia',N'Dumbrell',38,N'kdumbrellrh@sohu.com',N'475-383-5772',0,N'91708'),
	 ('1979-08-24',N'Corby',N'Rostron',47,N'crostronri@desdev.cn',N'447-517-7879',0,N'94617'),
	 ('1975-10-30',N'Torey',N'Grigorescu',42,N'tgrigorescurj@va.gov',N'905-522-6497',0,N'98609'),
	 ('1991-03-18',N'Keriann',N'Slarke',35,N'kslarkerk@google.fr',N'493-182-0833',0,N'92281'),
	 ('1955-12-19',N'Bettine',N'Malmar',42,N'bmalmarrl@reuters.com',N'460-166-6112',1,N'29329'),
	 ('1952-11-11',N'Emmalee',N'Alcide',38,N'ealciderm@chron.com',N'569-246-8262',0,N'94061'),
	 ('1976-02-26',N'Averyl',N'Ovise',22,N'aovisern@webeden.co.uk',N'746-514-4136',0,N'93940'),
	 ('1978-08-11',N'Gian',N'Foreman',14,N'gforemanro@icq.com',N'248-268-0257',0,N'95422'),
	 ('1995-08-29',N'Lynett',N'Kiossel',47,N'lkiosselrp@homestead.com',N'161-524-0084',0,N'91497'),
	 ('1989-11-25',N'Brewster',N'Chavey',39,N'bchaveyrq@intel.com',N'865-102-6918',0,N'90040');
INSERT INTO exposhr.dbo.patient (date_of_birth,first_name,last_name,max_distance,patient_email,phone_number,pre_existing_condition,zipcode) VALUES
	 ('1953-03-11',N'Fredericka',N'Minette',20,N'fminetterr@spotify.com',N'759-507-1875',0,N'29659');

	
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('Oakland','89 Pawling Point','reinabaldelli@abrazohealth.com','164-869-3107','pw','94705','Abrazo Health','CA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('Olympia','99386 Derek Alley','mateldaledbetter@kaiser-permanente.com','303-718-0431','pw','98502','Kaiser-Permanente','WA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('Olympia','48735 Division Circle','dudjurick@cvs.com','756-467-9204','pw','98503','CVS','WA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('San Francisco','36 Chive Alley','andonisrupke@walgreens.com','402-506-7591','pw','94140','Walgreens','CA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('Carmel','303 Spohn Park','callamenichelli@urgentclinics.com','506-356-3251','pw','93921','Urgent Clinics','CA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('Oakland','1 Cordelia Plaza','rockymcamish@osco.com','893-118-9747','pw','94611','Osco','CA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('Oakland','3 Merchant Avenue','saundraboyington@harristeeter.com','202-451-1968','pw','94649','Harris Teeter','CA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('San Francisco','38 Everett Place','jermaynedallmann@walmartpharmacy.com','220-486-9444','pw','94108','Walmart Pharmacy','CA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('Tacoma','5747 Hazelcrest Drive','pavelloblie@babbages.com','794-661-7612','pw','98471','Babbages','WA');
insert into provider (city ,main_address, main_email,main_phone_number, password, postal_code, provider_name, state) values ('San Francisco','98 Sullivan Point','edmunddaouze@berkeleyhealth.com','659-711-1361','pw','94132','Berkeley Health','CA');


insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('0 Fordem Point','Seattle','Rossy Cree','273-824-2884',0,'98101','Colorado','WA','5');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('884 Killdeer Center','Seattle','Concettina Hassent','991-847-6353',0,'98115','Glacier Hill','WA','3');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('378 Graedel Street','San Francisco','Abbie Juggins','869-865-8993',0,'94120','New Castle','CA','7');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('086 Waubesa Point','San Jose','Kienan Maguire','822-549-7978',0,'94008','Packers','CA','9');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('10 Summerview Hill','Oakland','Valeria Bulfoot','683-477-4168',0,'94501','Maple','CA','10');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('97945 Debs Court','Oakland','Eben Santer','366-682-1834',0,'94603','Clemons','CA','2');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('69961 Fuller Plaza','San Francisco','Alwyn Heazel','107-368-9352',0,'94142','Warrior','CA','4');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('749 Drewry Road','Seattle','Robinetta Pavlovsky','368-507-1658',0,'98141','Fuller','WA','10');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('32 Garrison Plaza','Charleston','Koo Deares','687-496-5559',0,'29461','Kim','SC','3');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('64 Fisk Pass','Tacoma','Mame Reeson','230-684-0061',0,'98421','Magdeline','WA','2');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('775 Lake View Junction','Tacoma','Alica Sysland','722-225-1980',0,'98466','Karstens','WA','5');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('04 Nova Place','Charleston','Meade De Metz','656-178-2489',0,'29412','Anzinger','SC','4');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('89 Pawling Point','Oakland','Reina Baldelli','164-869-3107',0,'94705','Butternut','CA','1');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('68 Hayes Point','Columbia','Camey Di Claudio','540-741-2743',0,'29044','Hoepker','SC','2');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('874 Tennyson Parkway','Seattle','Ingelbert Durrad','341-635-3263',0,'98165','Meadow Vale','WA','10');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('81 Arrowood Alley','Charleston','Gleda Wadly','974-437-5400',0,'29425','Hayes','SC','9');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('14901 Tony Trail','Tacoma','Spense Piggen','423-209-1442',0,'98405','Nobel','WA','9');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('11122 Packers Circle','San Francisco','Armand Squelch','575-358-5627',0,'94137','Oriole','CA','9');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('5846 Walton Plaza','Tacoma','Izak Roggerone','871-648-3016',0,'98424','Hovde','WA','7');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('17 Veith Junction','Tacoma','Anabelle Mathey','506-284-5093',0,'98404','Killdeer','WA','7');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('3 Kensington Court','Seattle','Cherilynn Boules','921-605-8202',0,'98117','Granby','WA','3');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('42 Gerald Alley','Seattle','Stephie Kubik','249-755-6996',0,'98175','Delladonna','WA','4');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('38 Everett Place','San Francisco','Jermayne Dallmann','220-486-9444',0,'94108','Barby','CA','8');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('77482 Muir Alley','San Jose','Dorothea Sandbach','644-914-9135',0,'95126','Oakridge','CA','7');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('9186 Stone Corner Lane','Oakland','Howey Cranston','654-377-1384',0,'94649','Atwood','CA','6');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('1295 Tennessee Hill','Oakland','Elfrida Baalham','849-558-9673',0,'94610','Service','CA','1');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('36 Chive Alley','San Francisco','Andonis Rupke','402-506-7591',0,'94140','Buell','CA','4');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('7 Pankratz Circle','Seattle','Filippa Pittwood','536-505-3894',0,'98121','Maryland','WA','9');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('303 Spohn Park','Carmel','Calla Menichelli','506-356-3251',0,'93921','Longview','CA','5');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('57 Bayside Parkway','San Jose','Hazlett Wannes','191-296-7233',0,'95103','Tony','CA','9');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('08200 Veith Road','Charleston','Scottie Lebell','860-839-0163',0,'29455','Coolidge','SC','1');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('48735 Division Circle','Olympia','Dud Jurick','756-467-9204',0,'98503','Morrow','WA','3');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('4106 4th Road','Seattle','Kristi Renols','533-838-6766',0,'98181','Lerdahl','WA','6');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('5747 Hazelcrest Drive','Tacoma','Pavel Loblie','794-661-7612',0,'98471','Killdeer','WA','9');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('88 Sheridan Trail','Oakland','Lockwood McCormack','745-468-5526',0,'94606','Bobwhite','CA','3');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('98 Sullivan Point','San Francisco','Edmund Daouze','659-711-1361',0,'94132','Clove','CA','10');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('4371 Northland Hill','Columbia','Ravi Kiss','769-651-3850',0,'29250','Forest Dale','SC','1');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('1 Cordelia Plaza','Oakland','Rocky McAmish','893-118-9747',0,'94611','Fairfield','CA','6');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('64524 Del Sol Place','San Francisco','Gerome Isenor','459-627-5457',0,'94110','Clyde Gallagher','CA','4');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('267 Columbus Crossing','Carmel','Mark Delgardo','752-762-2668',0,'93923','Mariners Cove','CA','7');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('15811 Novick Way','Columbia','Murial Shemelt','238-209-5393',0,'29212','Macpherson','SC','3');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('159 Armistice Lane','Olympia','Lilllie Maytom','796-592-4903',0,'98599','Sunbrook','WA','7');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('76 Fulton Street','Seattle','Jessie Ballister','989-703-4910',0,'98164','Elgar','WA','7');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('30 Sunfield Street','Carmel','Ambrose Haugen','507-671-3614',0,'93923','Heffernan','CA','1');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('709 Erie Point','San Jose','Jeanne Manvell','649-852-1489',0,'94560','Straubel','CA','2');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('35 Sage Street','Charleston','Leonid Yelland','434-587-3587',0,'29402','Hagan','SC','7');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('99386 Derek Alley','Olympia','Matelda Ledbetter','303-718-0431',0,'98502','Comanche','WA','2');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('79 Division Court','Seattle','Delcina Gillbanks','530-295-5433',0,'98133','Oneill','WA','1');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('601 Pond Alley','Tacoma','Willem Hryniewicz','370-800-7576',0,'98409','4th','WA','10');
insert into location (address, city, contact_person, contact_person_phone_number, onsite, postal_code, site_name,state,provider_provider_id) values ('3 Merchant Avenue','Oakland','Saundra Boyington','202-451-1968',0,'94649','Fulton','CA','7');


update user_account set provider_id=2 where user_id = 2;
update user_account set provider_id=1 where user_id = 1;
--insert into appt_mapping (appt_date, appt_time, vaccine_mfr, location_id, patient_id, provider_id) values ('2021-05-03','11:00am',);
update appt_mapping set patient_id = 1 where appointment_id = 1;
