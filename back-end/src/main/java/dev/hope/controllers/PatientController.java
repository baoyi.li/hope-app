package dev.hope.controllers;

import dev.hope.models.Patient;
import dev.hope.services.AuthService;
import dev.hope.services.PatientService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/patients")
@CrossOrigin // handles cors issue
public class PatientController {

    private static Logger logger = LogManager.getLogger(PatientController.class);

    @Autowired
    private PatientService patientService;
    @Autowired
    private AuthService authService;

    @GetMapping
    public List<Patient> getAllPatients(@RequestParam(value = "name", required = false) String nameParam) {
        if (nameParam != null) {
            logger.info("Getting patient by name: " + nameParam);
            return patientService.getByName(nameParam);
        }
        logger.info("Getting all patients");
        return patientService.getAll();
    }

    @GetMapping(value = "/{id}", produces = "application/json") // id can be accessed as path variable
    public Patient getPatientById(@PathVariable("id") int id) {
        return patientService.getById(id);
    }

    @PostMapping
    public ResponseEntity<Patient> createNewPatient(@RequestHeader("Authorization")String token, @RequestBody Patient patient) {
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        int userID;
        if(claimsMap.get("userId")==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        try {
            userID = Integer.parseInt((String) claimsMap.get("userId"));
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return patientService.create(patient, userID);
    }
}
