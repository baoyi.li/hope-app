package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name="zipcode")
@Component
@Scope("prototype")
public class ZipCode {

    @Id
    @JsonProperty("zip")
    private int zipcode;
    private double latitude;
    private double longitude;

    public ZipCode() {
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ZipCode zipCode = (ZipCode) o;
        return zipcode == zipCode.zipcode && Double.compare(zipCode.latitude, latitude) == 0 && Double.compare(zipCode.longitude, longitude) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(zipcode, latitude, longitude);
    }

    @Override
    public String toString() {
        return "ZipCode{" +
                "zipcode=" + zipcode +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
