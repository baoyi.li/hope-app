package dev.hope.repositories;


import dev.hope.models.ApptMapping;
import dev.hope.models.Patient;
import dev.hope.models.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppointmentRepository extends JpaRepository<ApptMapping, Integer> {

    public ApptMapping findByPatient(Patient patient);
    public List<ApptMapping> findByProvider(Provider provider);
}
