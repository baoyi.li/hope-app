package dev.hope.services;

import dev.hope.exceptions.UnauthorizedException;
import dev.hope.models.Patient;
import dev.hope.models.UserAccount;
import dev.hope.repositories.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthService authService;

    @Autowired
    private PatientService patientService;

    private Logger logger = LogManager.getLogger(UserService.class);


    public List<UserAccount> getAllUsers(){
        return userRepository.findAll();
    }


    public UserAccount getUserById(int id){

        return userRepository.getOne(id);
    }

    public UserAccount  SetPatientId(int userId, Patient patient){
        UserAccount user = userRepository.getOne(userId);
        //Patient patient = patientService.getById(patientId);
        if(user == null){
            logger.info("user does not exist");
            throw new IllegalArgumentException("user can not be null");
        }else if (patient == null){
            logger.info("patient does not exist");
            throw new IllegalArgumentException("patient can not be null");
        }
        user.setPatient(patient);
        logger.info("Add Patient " + patient.getId() + "to user account" + userId);
        return userRepository.save(user);
    }

    public ResponseEntity<String> login(String email, String password){
        UserAccount registeredCredentials = userRepository.findUserAccountByEmail(email);
        if(registeredCredentials==null){
            logger.info("User Account not found");
            throw new UnauthorizedException("User Account not found.");
        }
        if(authService.checkPassword(password, registeredCredentials.getPassword())){
            String jwt = authService.generateToken(registeredCredentials).toString();
            HttpHeaders headers = new HttpHeaders();
            List<String> exposeHeaders = new ArrayList<>();
            exposeHeaders.add("Authorization");
            headers.add("Authorization",jwt);
            if(registeredCredentials.getProvider()!=null){
                headers.add("Provider",String.valueOf(registeredCredentials.getProvider().getId()));
                exposeHeaders.add("Provider");
            }
            if(registeredCredentials.getPatient()!=null){
                System.out.println(String.valueOf(registeredCredentials.getPatient().getId()));
                headers.add("Patient",String.valueOf(registeredCredentials.getPatient().getId()));
                exposeHeaders.add("Patient");
            }
            headers.add("User",String.valueOf(registeredCredentials.getId()));
            exposeHeaders.add("User");
            headers.setAccessControlExposeHeaders(exposeHeaders);
            logger.info("Login Successful");
            return ResponseEntity.status(HttpStatus.OK).headers(headers).body("");
        }
        logger.info("Login Failed: Passwords don't match");
        throw new UnauthorizedException("Passwords don't match");
    }

    public ResponseEntity<String> register(String email, String password){
        if(userRepository.findUserAccountByEmail(email)!=null){
            return ResponseEntity.badRequest().body("A User Account already exists with that e-mail address. If you have forgotten your password, please try resetting it.");
        }
        // Here we need to hash the password.
        UserAccount newUser = new UserAccount(email, authService.hashPassword(password));
        newUser = userRepository.save(newUser);
        String jwt = authService.generateToken(newUser).toString();
        return ResponseEntity.status(HttpStatus.CREATED).header("Access-Control-Expose-Headers","Authorization, User").header("Authorization",jwt).header("User",String.valueOf(newUser.getId())).body("");
    }

    public UserAccount updateUserAccount (UserAccount account){
        return userRepository.save(account);
    }


}
