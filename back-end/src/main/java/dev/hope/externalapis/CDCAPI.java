package dev.hope.externalapis;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.hope.models.CDCCovidData;
import dev.hope.models.CDCVaccinationData;
import dev.hope.models.Placeholder;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class CDCAPI {

    private String covidURL = "https://data.cdc.gov/resource/9mfq-cb36.json?submission_date=";

    private static final String vaccinationURL = "https://covid.cdc.gov/covid-data-tracker/COVIDData/getAjaxData?id=vaccination_data";

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public List<CDCCovidData> getAllCovidData(){

        List<CDCCovidData> cdcCovidDataList = null;
        int dayToSubtract = 0;
        do {
            LocalDate dataDate = LocalDate.now().minusDays(dayToSubtract);
            String submissionDate = dataDate.toString() + "T00:00:00.000";
            objectMapper.configure(
                    DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            HttpResponse<String> response = Unirest.get(covidURL + submissionDate).asString();
            try {
                CDCCovidData[] cdcCovidDataArray = objectMapper.readValue(response.getBody(),CDCCovidData[].class);
                cdcCovidDataList = new ArrayList<CDCCovidData>(Arrays.asList(cdcCovidDataArray));
                if (cdcCovidDataList.size() > 0) {
                    return cdcCovidDataList;
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            dayToSubtract += 1;
        } while (cdcCovidDataList.size() <= 0);
        return null;
    }

    public List<CDCVaccinationData> getAllVaccinationData(){
        objectMapper.configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        HttpResponse<String> response = Unirest.get(vaccinationURL).asString();
        try {
            Placeholder ph = objectMapper.readValue(response.getBody(),Placeholder.class);
            CDCVaccinationData[] cdcVaccinationDataArray = ph.getVaccinationData();
            List<CDCVaccinationData> cdcVaccinationDataList = new ArrayList<CDCVaccinationData>(Arrays.asList(cdcVaccinationDataArray));
            return cdcVaccinationDataList;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
