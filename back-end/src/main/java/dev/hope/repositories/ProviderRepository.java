package dev.hope.repositories;

import dev.hope.models.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

// this is our dao interface

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Integer> {


    public List<Provider> findProviderByProviderNameContaining(String name);


}
