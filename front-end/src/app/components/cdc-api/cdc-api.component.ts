import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import {CovidData} from 'src/app/models/covid-data';

import {VaccinationData} from 'src/app/models/vaccination-data';

import {CdcAPIService} from 'src/app/services/cdc-api.service';


@Component({
  selector: 'app-cdc-api',
  templateUrl: './cdc-api.component.html',
  styleUrls: ['./cdc-api.component.css']
})
export class CdcAPIComponent implements OnInit {

  inputStateSelection:string = "";

  covidData: CovidData[] = [];
  vaccinationData: VaccinationData[] = []; 

  currentCovidDataPoint:CovidData = new CovidData();
  currentVaccinationDataPoint:VaccinationData = new VaccinationData(); 

  errorMessage: string = "";

  states: String[] = [];
  
  constructor(private cdcAPIService:CdcAPIService) { 
  } 
    

  ngOnInit(): void {
      this.getCovidData();
      this.getVaccinationData();
  }

  stateSelection = new FormGroup({
    inputState: new FormControl("",[
      Validators.required
    ]),
  });

  get inputState() {
    return this.stateSelection?.get("inputState");
  }

  fillStates(): void {
    for (let i = 0; i < this.covidData.length; i++) {
      this.states[i] = this.covidData[i].state;
    }
    this.states.sort();
  }

  getCovidData(): void {
    this.cdcAPIService.getAllCovidData()
    .subscribe(data => {
      this.covidData = data;
    });
  }

  getVaccinationData(): void {
    this.cdcAPIService.getAllVaccinationData()
      .subscribe(data=> {
        this.vaccinationData = data;
    });
  }

  updateData(): void {
    this.inputStateSelection = this.inputState?.value;
    this.findDoses_AdministeredByState(this.inputStateSelection);
    this.findDoses_DistributedByState(this.inputStateSelection);
    this.findTotalDeaths(this.inputStateSelection);
    this.findTotalCasess(this.inputStateSelection);
  }
  

  findTotalCasesByState(state: string): void {
    for(let element of this.covidData) {
        if(state !=null && state == element.state){
          this.currentCovidDataPoint.tot_cases = element.tot_cases;
          return;
        }
    }
  }

  findTotalDeathsByState(state: string): void {
    for(let element of this.covidData) {
        if(state !=null && state == element.state){
          this.currentCovidDataPoint.tot_death = element.tot_death;
          return;
        }
    }
  }



  findDoses_AdministeredByState(state: string): void {
    for(let element of this.vaccinationData) {
        if(state !=null && state == element.ShortName.substring(0,2)){
          this.currentVaccinationDataPoint.Doses_Administered = element.Doses_Administered;
          return;
        }      
    }
  }

  findDoses_DistributedByState(state: string): void {
    for(let element of this.vaccinationData) {
        if(state !=null && state == element.ShortName.substring(0,2)){
          this.currentVaccinationDataPoint.Doses_Distributed = element.Doses_Distributed;
          return;
        }
    }
  }

  findTotalDeaths(state: string): void {
    for(let element of this.covidData) {
      if(state !=null && state == element.state){
        this.currentCovidDataPoint.tot_death = element.tot_death;
        return;
      }
      
    }
  }

  findTotalCasess(state: string): void {
    for(let element of this.covidData) {
      if(state !=null && state == element.state){
        this.currentCovidDataPoint.tot_cases = element.tot_cases;
        return;
      }
    }
  }

}
