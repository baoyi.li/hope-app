import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Location } from "../models/location"; 

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  locationsURL: string = environment.baseUrl +"/locations";

  constructor(private http: HttpClient) { }

    // get method
    getAllLocations(): Observable<Location[]> {
      let token: string = sessionStorage.getItem("Authorization") || "";
      let httpOptions = {
        headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
      }
      return this.http.get<Location[]>(this.locationsURL, httpOptions);
    }

    // get method
    getLocationById(id:number): Observable<Location> {
      let token: string = sessionStorage.getItem("Authorization") || "";
      let httpOptions = {
        headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
      }
      const url = `${this.locationsURL}/${id}`;
      return this.http.get<Location>(url, httpOptions);
    }




    // update method
    updateLocation(location:Location): Observable<any> {
      let token: string = sessionStorage.getItem("Authorization") || "";
      let httpOptions = {
        headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
      }
      return this.http.put<Location>(this.locationsURL, location, httpOptions);
    }

    // delete method
    deleteLocation(id:number): Observable<Location> {
      let token: string = sessionStorage.getItem("Authorization") || "";
      let httpOptions = {
        headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
      }
      const url = `${this.locationsURL}/${id}`;
      return this.http.delete<Location>(url, httpOptions);
    }

    // post method
    addLocation(location:Location): Observable<Location> {
      let token: string = sessionStorage.getItem("Authorization") || "";
      let httpOptions = {
        headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
      }
      return this.http.post<Location>(this.locationsURL, location, httpOptions);
    }

}
