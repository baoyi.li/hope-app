package dev.hope.services;

import dev.hope.models.ApptMapping;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface AppointmentService {
    List<ApptMapping> listAllAppt (int providerId);

    ApptMapping getApptById(Integer id);

    ApptMapping addAppt(ApptMapping appt);

    boolean deleteAppt(Integer id);

    ResponseEntity<ApptMapping> confirmAppointment(int apptID, int patID);

    ApptMapping getPatientAppointment(int userID);
}
