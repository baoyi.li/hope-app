import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string = "";
  password: string = "";
  message: string = "";

  constructor(private authService: AuthService, private matDialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    let user: User = new User(this.email, this.password);
    let dest: string = "patient-portal";
    // call authService's method here
    this.authService.attemptLogin(user).subscribe((res)=>{
      let token: string = res.headers.get("Authorization");
      // console.log(token);
      sessionStorage.setItem("Authorization",token);
      if(res.headers.get("Provider")){
        // if this user is a provider, update isProvider to true
        this.authService.updateIsProvider(true);
        console.log("User is provider!\npid: "+res.headers.get("Provider"));
        this.authService.updateProviderID(res.headers.get("Provider"));
        dest = "provider-portal";
      }
      // after I am authorized as user, update login status to true
      this.authService.updateLoginStatus(true);
      this.authService.updateUserID(res.headers.get("User"))
      // if this user is a patient, update isPatient to true
      if(res.headers.get("Patient")){
        console.log(res.headers.get("Patient"));
        this.authService.updateIsPatient(true);
        this.authService.updatePatientID(res.headers.get("Patient"));
      }
      this.matDialog.closeAll();
      this.router.navigate(['home']);
    },
    (res)=>{
      console.log("Login Failure");
      this.message = "Invalid login credentials submitted."
    });
  }

}
