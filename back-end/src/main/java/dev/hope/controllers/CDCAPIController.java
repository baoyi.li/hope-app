package dev.hope.controllers;



import dev.hope.externalapis.CDCAPI;
import dev.hope.models.CDCCovidData;
import dev.hope.models.CDCVaccinationData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
/**
 * registering this class as a spring bean
 * returns a response body from each method
 * @RestController is equivalent to both @Controller and @ResponseBody for each handler method
 */

@RequestMapping("/cdc-api")   // prepends this to each request URI
@CrossOrigin
public class CDCAPIController {

    @Autowired
    private CDCAPI cdcAPI;


    /**
     * ask karl or carolyn
     * @return
     */
    @GetMapping(value = "/covid-data")
    public List<CDCCovidData> getAllCDCCovidData() {
        return cdcAPI.getAllCovidData();
    }

    /**
     * works
     * @return
     */
    @GetMapping(value = "/vaccination-data")
    public List<CDCVaccinationData> getAllCDCVaccinationData() {
        return cdcAPI.getAllVaccinationData();
    }


}
