package dev.hope.controllers;

import dev.hope.models.ApptMapping;
import dev.hope.models.Provider;
import dev.hope.repositories.LocationRepository;
import dev.hope.services.AppointmentServiceImpl;
import dev.hope.services.AuthService;
import dev.hope.services.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/appointments")
@CrossOrigin
public class AppointmentController {
    private static final Logger logger = LogManager.getLogger(AppointmentController.class);

    @Autowired
    private AppointmentServiceImpl apptService;
    @Autowired
    private AuthService authService;
    @Autowired
    ProviderService providerService;
    @Autowired
    private LocationRepository locationRepository;

    @GetMapping
    public ResponseEntity<List<ApptMapping>> getAllAppt(@RequestHeader("Authorization")String token){
        logger.info("Getting all appointment");
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        if(claimsMap.get("providerId")==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        try{
            int providerID = Integer.parseInt((String)claimsMap.get("providerId"));
            return ResponseEntity.ok().body(apptService.listAllAppt(providerID));
        }catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @GetMapping("/my-appointment")
    public ResponseEntity<ApptMapping> getAppointmentByUserID(@RequestHeader("Authorization")String token){
        if(token==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        int userID;
        if(claimsMap.get("userId")==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        try {
            userID = Integer.parseInt((String) claimsMap.get("userId"));
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        ApptMapping appointment = apptService.getPatientAppointment(userID);
        System.out.println(appointment);
        return ResponseEntity.ok().body(apptService.getPatientAppointment(userID));
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<ApptMapping> createNewAppt(@RequestHeader("Authorization")String token, @RequestBody ApptMapping appt){
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        int providerID;
        if(claimsMap.get("providerId")==null) {
            System.out.println("providerId==null");
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        try {
            providerID = Integer.parseInt((String) claimsMap.get("providerId"));
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        Provider provider = providerService.getProviderById(providerID);
        appt.setProvider(provider);
        int locationID = appt.getLocation().getId();
        appt.setLocation(locationRepository.getOne(locationID));
        apptService.addAppt(appt);
        return ResponseEntity.ok().body(appt);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ApptMapping> deleteAppt(@RequestHeader("Authorization")String token, @PathVariable("id") int id){
        Map<String,Object> claimsMap = authService.validateAuthToken(token);
        int providerID;
        if(claimsMap.get("providerId")==null)
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        try {
            providerID = Integer.parseInt((String) claimsMap.get("providerId"));
        } catch (Exception e){
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        ApptMapping appt = apptService.getApptById(id);
        if(providerID!=appt.getProvider().getId())
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        boolean isRemoved = apptService.deleteAppt(id);
        if (!isRemoved)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{apptId}/{patId}/confirm")
    public ResponseEntity<String> confirmAppointment(@PathVariable("apptId") int apptID,@PathVariable("patId") int patId){
        // System.out.println("Appoint confirmation. ApptID: " + apptID + "\t\tpatID: " + patId);
        apptService.confirmAppointment(apptID,patId);
        return ResponseEntity.ok().body("Thank you, your appointment has been confirmed.\nYou can check your appointment status at any time through the patient portal portal at http://karlkanitschrevature.z13.web.core.windows.net/");
    }
}
