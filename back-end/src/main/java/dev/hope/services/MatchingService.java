package dev.hope.services;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.hope.models.*;
import dev.hope.repositories.PatientRepository;
import dev.hope.repositories.ZipCopeRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class MatchingService {

//    private List<ZipCode> zipCodes;
//    private Map<Integer, Integer> zipCodeIndeces = new HashMap<>();
    private List<Patient> waitlist;

    @Autowired
    PatientRepository patientRepository;

    @Autowired
    ZipCopeRepository zipCopeRepository;

    @Value("classpath:Data_Zipcodes.json")
    Resource resourceFile;

    private Logger logger = LogManager.getLogger(MatchingService.class);

    public void MatchingServiceInit() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            ZipCodeDataStream[] zipCodeData = objectMapper.readValue(resourceFile.getFile(), ZipCodeDataStream[].class);
            int i = 0;
            for(ZipCodeDataStream entry: zipCodeData){
                zipCopeRepository.save(entry.zipCode);
//                zipCodes.add(entry.zipCode);
//                zipCodeIndeces.put(entry.zipCode.getZipcode(), i);
//                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Patient findMatch(ApptMapping appointment){
        int lZip;
        int pZip;
        try {
            lZip = Integer.parseInt(appointment.getLocation().getPostalCode());
        } catch (NumberFormatException nfe){
            nfe.printStackTrace();
            return null;
        }
        updateWaitlist();
        for(Patient candidate: waitlist){
            int maxDistance = candidate.getMaxDistance();
            try {
                pZip = Integer.parseInt(candidate.getZipcode());
            } catch(NumberFormatException nfe){
                nfe.printStackTrace();
                continue;
            }
            if(calculateDistance(lZip,pZip) <= maxDistance){
//                System.out.println(candidate.toString());
                return candidate;
            }
        }
        return null;
    }

    private void updateWaitlist(){
        waitlist = patientRepository.findAllByOrderByIdAsc();
    }

    private int calculateDistance(int locationZipCode, int patientZipCode){
        ZipCode lZip;
        ZipCode pZip;
        try {
            Optional<ZipCode> oZip = zipCopeRepository.findById(locationZipCode);
            if(oZip.isPresent())
                lZip = oZip.get();
            else
                return 1000;
            oZip = zipCopeRepository.findById(patientZipCode);
            if(oZip.isPresent())
                pZip = oZip.get();
            else
                return 1000;
//            logger.info("lZip: " + locationZipCode + "\t\tpZip: " + patientZipCode + "\t\tdistance: " + Math.ceil(haversine(lZip.getLatitude(),lZip.getLongitude(),pZip.getLatitude(),pZip.getLongitude())));
            return (int) Math.ceil(haversine(lZip.getLatitude(),lZip.getLongitude(),pZip.getLatitude(),pZip.getLongitude()));
        } catch(NullPointerException e){
            return 1000;
        }
    }


    /**
     * Calculates the distance in km between two lat/long points
     * using the haversine formula
     */
    private static double haversine(
            double lat1, double lng1, double lat2, double lng2) {
        int r = 6371; // average radius of the earth in km
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                        * Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        // Adding a 0.6 factor to convert to miles
        double d = r * c * 0.6;
        return d;
    }
}
