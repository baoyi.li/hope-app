package dev.hope.controllers;

import dev.hope.models.Patient;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.sql.Date;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
@Disabled
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT) // this parameter is for web applications

public class PatientControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testGetOnePatient() {
        Patient expectedPatient = new Patient();
        Patient actual = this.restTemplate.getForObject("http://localhost/patients/0", Patient.class);
        assertEquals(expectedPatient, actual);
    }

    @Test
    public void testCreatePatient() {
//        Patient patient = new Patient("Sujit", "N", "206", "s@gmail.com", "98133", new LocalDate(System.currentTimeMillis()), false);
//        Patient actual = this.restTemplate.postForObject("http://localhost/patients", patient, Patient.class);
//        assertNotNull(actual);
    }

    @Test
    @Disabled
    public void testGetPatientByName() {
//        Patient patient = new Patient("Sujit", "N", "206", "s@gmail.com", "98133", new LocalDate(System.currentTimeMillis()), false);
//        Patient actual = this.restTemplate.postForObject("http://localhost/patients", patient, Patient.class);
//
//        Patient returnedPatient = this.restTemplate.getForObject("http://localhost/patients?name=Sujit", Patient.class);
//        // assertNotNull(actual);
    }
}
