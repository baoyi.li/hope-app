package dev.hope.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name="covid_data")
@JsonIgnoreProperties("hibernateLazyInitializer")
@Component
@Scope("prototype")
public class CDCCovidData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @JsonProperty("submission_date")
    private String submissionDate;
    @JsonProperty("state")
    private String state;
    @JsonProperty("tot_cases")
    private double totalCases;
    @JsonProperty("tot_death")
    private double totalDeaths;

    public CDCCovidData() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(String submissionDate) {
        this.submissionDate = submissionDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public double getTotalCases() {
        return totalCases;
    }

    public void setTotalCases(double totalCases) {
        this.totalCases = totalCases;
    }

    public double getTotalDeaths() {
        return totalDeaths;
    }

    public void setTotalDeaths(double totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CDCCovidData that = (CDCCovidData) o;
        return id == that.id && Double.compare(that.totalCases, totalCases) == 0 && Double.compare(that.totalDeaths, totalDeaths) == 0 && Objects.equals(submissionDate, that.submissionDate) && Objects.equals(state, that.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, submissionDate, state, totalCases, totalDeaths);
    }

    @Override
    public String toString() {
        return "CDCCovidData{" +
                "id=" + id +
                ", submissionDate='" + submissionDate + '\'' +
                ", state='" + state + '\'' +
                ", totalCases=" + totalCases +
                ", totalDeaths=" + totalDeaths +
                '}';
    }
}
