package dev.hope.repositories;

import dev.hope.models.Provider;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
abstract class ProviderData implements ProviderRepository{

// public class ProviderData implements ProviderRepository{

    private final List<Provider> providers = new ArrayList<>();

    /**
     * version 0:
     *
     // get methods
     @Override
     public List<Provider> getAllProviders() {
     return new ArrayList<>(providers);
     }

     @Override
     public Provider getProviderById(int id) {
     return providers.stream().filter(provider -> provider.getId()==id).findAny().orElse(null);
     }

     @Override
     public Provider getProviderByName(String name) {
     return providers.stream().filter(location -> location.getProviderName().equals(name)).findFirst().get();
     }

     // post method
     @Override
     public Provider addProvider(Provider provider) {
     providers.add(provider);
     return provider;
     }

     @Override
     public void updateProvider(int id, Provider provider) {
     for (int i = 0; i < providers.size(); i++) {
     Provider p = providers.get(i);
     if(p.getId() == id) {
     providers.set(i, provider);
     return;
     }
     }
     }


    // delete method
     @Override
     public void deleteProvider(int id) {
     providers.removeIf(provider -> provider.getId()==id);
     }

     *
     */




}
