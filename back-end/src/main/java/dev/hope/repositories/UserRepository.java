package dev.hope.repositories;

import dev.hope.models.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserAccount, Integer> {

    public UserAccount findUserAccountByEmail(String Email);
}
