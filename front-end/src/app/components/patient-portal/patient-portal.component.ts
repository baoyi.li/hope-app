import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.service';
import { CdcAPIService } from 'src/app/services/cdc-api.service';
import { PatientPortalService } from 'src/app/services/patient-portal.service';
import { AddToWaitlistComponent } from '../add-to-waitlist/add-to-waitlist.component';
import { AppointmentStatusComponent } from '../appointment-status/appointment-status.component';

@Component({
  selector: 'app-patient-portal',
  templateUrl: './patient-portal.component.html',
  styleUrls: ['./patient-portal.component.css']
})
export class PatientPortalComponent implements OnInit {

  hasAppointment = false;
  isPatient = false;

  totalVaccineDistributed = "";


  constructor(public addToWaitlistModal: MatDialog, public appointmentStatusModal: MatDialog, private patientPortalService: PatientPortalService, 
    private cdcAPIService: CdcAPIService, private authService: AuthService) { }

  ngOnInit(): void {
    this.patientPortalService.hasAppointmentObservable
    .subscribe((setHasAppointment)=>{this.hasAppointment=setHasAppointment});
    this.authService.isPatientObservable.subscribe((isPatent)=>this.isPatient=isPatent);
    this.cdcAPIService.getAllVaccinationData()
      .subscribe(data=> {
        this.totalVaccineDistributed = data[0].Doses_Distributed;
    });
  }

  openAddToWaitlistCard() {
    this.addToWaitlistModal.open(AddToWaitlistComponent, {panelClass: "mat-dialog-master"});
  }

  openAppointmentStatusCard() {
    this.appointmentStatusModal.open(AppointmentStatusComponent, {panelClass: "mat-dialog-master"});
  }
}
