package dev.hope.repositories;

import dev.hope.models.ZipCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZipCopeRepository extends JpaRepository<ZipCode, Integer> {
}
