package dev.hope.services;

import dev.hope.models.UserAccount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthService {

    private static final Key key = Keys.secretKeyFor(SignatureAlgorithm.HS512);
    private static final Logger logger = LogManager.getLogger(AuthService.class);

    public String generateToken(UserAccount userAccount){
        Map<String, String> claimsMap = new HashMap<>();
        claimsMap.put("userId", String.valueOf(userAccount.getId()));
        if(userAccount.getProvider()!=null)
            claimsMap.put("providerId", String.valueOf(userAccount.getProvider().getId()));
        if(userAccount.getPatient()!=null)
            claimsMap.put("patientId", String.valueOf(userAccount.getPatient().getId()));
        return Jwts.builder().setClaims(claimsMap).signWith(key).compact();
    }

    public String hashPassword(String password){
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.encode(password);
    }

    public boolean checkPassword(String submittedPW, String storedPW){
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(submittedPW,storedPW);
    }

    public Map<String, Object> validateAuthToken(String authToken) {
        logger.info(authToken);
        try {
            return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(authToken).getBody();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("SignatureException encountered while validating login token.");
        }
        return new HashMap<>();
    }
}
