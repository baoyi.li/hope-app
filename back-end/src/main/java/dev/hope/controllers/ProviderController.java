package dev.hope.controllers;

import dev.hope.models.Provider;
import dev.hope.services.ProviderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


    /*
    GET /providers
    POST /providers
    GET /providers/id
    PUT /providers/id
    DELETE /providers/id

    GET /providers?name=str
     */



import java.util.List;


@RestController
/**
 * registering this class as a spring bean
 * returns a response body from each method
 * @RestController is equivalent to both @Controller and @ResponseBody for each handler method
 */

@RequestMapping("/providers")   // prepends this to each request URI
@CrossOrigin    // prevents cors issues
public class ProviderController {

    private static Logger logger = LogManager.getLogger(ProviderController.class);

    @Autowired
    private ProviderService providerService;

    @GetMapping
    public List<Provider> getAllProviders() {
        return providerService.getAll();
    }

    @GetMapping(value = "/{id}",produces = "application/json")
    public Provider getProviderById(@PathVariable int id) {
        logger.info("Getting provider with id: {}", id);
        return providerService.getProviderById(id);
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<Provider> addProvider(@RequestBody Provider provider) {
        providerService.addProvider(provider);
        return new ResponseEntity<>(provider, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    public void updateProvider(@RequestBody Provider provider, @PathVariable("id") int id) {
        providerService.updateProvider(id, provider);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteProvider(@PathVariable("id") int id) {
        providerService.deleteProvider(id);
    }
}
