import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Appointment} from '../models/appointment.model';



@Injectable({
  providedIn: 'root'
})
export class AppointmentService {

  appointmentUrl: string = environment.baseUrl + "appointments";
  constructor(private http: HttpClient) { }

  getAllData(): Observable<Appointment[]> {
    let token: string = sessionStorage.getItem("Authorization") || "";
    let httpOptions = {
      headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
    }
    console.log(httpOptions);
    return this.http.get<Appointment[]>(this.appointmentUrl, httpOptions);
  }

  addAppointment(appointment: Appointment): Observable<Appointment>{
    let token: string = sessionStorage.getItem("Authorization") || "";
    let httpOptions = {
      headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
    }
    return this.http.post<Appointment>(this.appointmentUrl, appointment, httpOptions);
  }

  deleteAppt(id: number): Observable<{}>{
    let token: string = sessionStorage.getItem("Authorization") || "";
    let httpOptions = {
      headers: new HttpHeaders().set("Content-Type", "application/json").set("Authorization", token)
    }
    const url = `${this.appointmentUrl}/${id}`;
    return this.http.delete(url, httpOptions);
  }
}
